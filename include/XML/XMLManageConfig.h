/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef XML_MANAGE_CONFIG_H
#define XML_MANAGE_CONFIG_H

#include "XMLManager.h"

class XMLManageConfig: public XMLManager {
	public:
		XMLManageConfig();
		~XMLManageConfig();
		void loadXML(Configuration* configuration);
		void saveXML(Configuration* configuration);
	private:
		char* XML_PATH;
		const char* XML_PATHS_TAG;
		const char* PATH_TAG;
		const char* TYPE_ATTRIBUTE;
		const char* MUSIC_TAG;
		const char* FX_TAG;
		const char* LEVEL_TAG;
		const char* PLANET_TAG;
		const char* PLAYER_TAG;
		const char* ENEMY_TAG;
		const char* SHOOT_TAG;
		const char* SHIELD_TAG;
		//const char* MOVEMENT_TAG;
		const char* OPTIONS_TAG;
		const char* UI_TAG;
		const char* FX_ATTRIBUTE;
		const char* MUSIC_ATTRIBUTE;
		const char* LOAD_TAG;
		const char* SCORE_TAG;
};
#endif
