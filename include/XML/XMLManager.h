/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef XML_MANAGER_H
#define XML_MANAGER_H

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

#include "Configuration.h"

using namespace std;
using namespace xercesc;

class XMLManager {
	public:
		XMLManager();
		virtual ~XMLManager();
		char* getAttributeValue (DOMNode* node, const XMLCh* attributeName);
		char* getNodeValue (DOMNode* node);
		virtual void loadXML(Configuration* configuration) = 0;
		virtual void saveXML(Configuration* configuration) = 0;
	protected:
		void initParser(char* xmlPath);
		XercesDOMParser* _parser;
		DOMDocument* _xmlDoc;
		DOMElement* _elementRoot;
};

#endif
