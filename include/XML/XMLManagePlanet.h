/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef XML_MANAGE_PLANET_H
#define XML_MANAGE_PLANET_H

#include "XMLManager.h"

class XMLManagePlanet: public XMLManager {
	public:
		XMLManagePlanet();
		~XMLManagePlanet();
		void loadXML(Configuration* configuration);
		void saveXML(Configuration* configuration);
	private:
		const char* PLANET_TAG;
		const char* INDEX_ATTRIBUTE;
		const char* NAME_TAG;
		const char* MESH_TAG;
		const char* SPEED_TAG;
		const char* SIZE_TAG;
		const char* RADIO_TAG;
};

#endif
