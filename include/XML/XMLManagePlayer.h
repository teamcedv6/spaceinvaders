/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef XML_MANAGE_PLAYER_H
#define XML_MANAGE_PLAYER_H

#include "XMLManager.h"
#include "SoundManager.h"

class XMLManagePlayer: public XMLManager {
	public:
		XMLManagePlayer();
		~XMLManagePlayer();
		void loadXML(Configuration* configuration);
		void loadXML(Configuration* configuration, SoundManager* soundManager);
		void saveXML(Configuration* configuration);
	private:
		const char* PLAYER_TAG;
		const char* INDEX_ATTRIBUTE;
		const char* NAME_TAG;
		const char* MESH_TAG;
		const char* LIFE_TAG;
		const char* SHOOT_TAG;
		const char* SHIELD_TAG;
		const char* SIZE_TAG;
		const char* HEIGHT_TAG;
		const char* LENGTH_TAG;
		const char* SPEED_TAG;
		const char* IMPACT_FX_TAG;
		const char* DESTROY_FX_TAG;


};

#endif
