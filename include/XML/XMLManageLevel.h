/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef XML_MANAGE_LEVEL_H
#define XML_MANAGE_LEVEL_H

#include "XMLManager.h"

class XMLManageLevel: public XMLManager {
	public:
		XMLManageLevel();
		~XMLManageLevel();
		void loadXML(Configuration* configuration);
		void saveXML(Configuration* configuration);
	private:
		const char* INIT_TAG;
		const char* LEVEL_TAG;
		const char* MUSIC_TAG;
		const char* PLANET_TAG;
		const char* WAVES_TAG;
		const char* WAVE_TAG;
		const char* ENEMY_TAG;
		const char* INDEX_ATTRIBUTE;
		const char* LAST_ATTRIBUTE;
		const char* MOVE_ATTRIBUTE;
		const char* SPLIT_ATTRIBUTE;
		const char* NUMBER_ATTRIBUTE;
		const char* RADIO_ATTRIBUTE;
		const char* SPEED_ATTRIBUTE;
		const char* ANGLE_ATTRIBUTE;
		const char* HEIGHT_ATTRIBUTE;
};

#endif
