/*
 * Enemy.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef ENEMY_H_
#define ENEMY_H_

#include "Ship.h"

class Enemy : public Ship {
public:
	Enemy();
	Enemy(std::string name, int lifes, Shoot::ShootType shoot, int row, int column);
	Enemy(const Enemy & enemy);
	~Enemy();
	Enemy& operator = (const Enemy &Enemy);

	void shoot();
	void setPosition(float p);
	void move(float position, float height);
	virtual ActionResult update(Ogre::Real deltaTime);

	const int getRow() const;
	const int getColumn() const;

	void setUniqueParameters(std::string name, int row, int column);

private:
	float _minHeight;
	bool _hasMoved;
	float _dp;

	int _column;
	int _row;
};

#endif /* ENEMY_H_ */
