/*
 * WaveGenerator.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef WAVEGENERATOR_H_
#define WAVEGENERATOR_H_

#include <Ogre.h>
#include "Shoot.h"
#include "Wave.h"
#include "Planet.h"
#include "Wall.h"
#include "ActionResults.h"

class WaveGenerator {
public:

	WaveGenerator();
	WaveGenerator(const WaveGenerator & waveGenerator);
	virtual ~WaveGenerator();
	WaveGenerator& operator = (const WaveGenerator &waveGenerator);

	void addWave(Wave * wave);

	Wave * askForWave();
	void deleteCurrentWave(Wave * wave);

protected:

	std::vector<Wave*> _futureWaves;
	std::vector<Wave*> _currentWaves;
};



#endif /* WAVEGENERATOR_H_ */
