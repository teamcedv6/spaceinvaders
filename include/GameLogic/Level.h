/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef LEVEL_H_
#define LEVEL_H_

#include <Ogre.h>
#include <vector>
#include "Shoot.h"
#include "Wave.h"
#include "Planet.h"
#include "Wall.h"
#include "ActionResults.h"
#include "WaveGenerator.h"
#include "EnemyAttacker.h"

class Level {
public:

	Level();
	Level(std::string name, int radius, std::string music, Planet & planet, int last);
	Level(const Level & Level);
	virtual ~Level();
	Level& operator = (const Level &Level);

	std::string & getName();
	int getRadius();
	std::string & getMusic();

	ActionResult update(Ogre::Real deltaTime);
	ActionResult receiveImpact(std::string & name);
	void startLevel();
	int getLast();

	void addShoot(Shoot & shoot);
	void addWall(Wall wall);
	void addWaveGenerator(WaveGenerator * generator);
	void addAttacker(EnemyAttacker * enemyAttacker);

protected:
	std::string _name;
	std::string _music;
	int _r;
	int _last;

	std::vector<Shoot> _shoots;
	std::vector<Wave*> _waves;
	std::vector<Wall> _walls;
	Planet _planet;
	WaveGenerator * _waveGenerator;
	EnemyAttacker * _attacker;
};



#endif /* Level_H_ */
