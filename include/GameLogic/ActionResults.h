/*
 * ActionResults.h
 *
 *  Created on: Feb 28, 2017
 *      Author: root
 */

#ifndef ACTIONRESULTS_H_
#define ACTIONRESULTS_H_

enum ActionResult {
	RESULT_CONTINUE   = 0,
	RESULT_DEAD 	  = 1,
	RESULT_DESTROY    = 2,
	RESULT_GAME_WIN   = 3,
	RESULT_GAME_LOST  = 4
};


#endif /* ACTIONRESULTS_H_ */
