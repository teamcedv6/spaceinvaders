/*
 * EnemyAttacker.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef ENEMYATTACKER_H_
#define ENEMYATTACKER_H_

#include <Ogre.h>
#include "Wave.h"
#include "Player.h"
#include "Enemy.h"


class EnemyAttacker {
public:

	EnemyAttacker();
	EnemyAttacker(const EnemyAttacker & enemyAttacker);
	virtual ~EnemyAttacker();
	EnemyAttacker& operator = (const EnemyAttacker &enemyAttacker);

	void setPlayer(Player * player);
	void addWave(Wave * wave);
	void removeWave(Wave * wave);

	void tryAttack(Ogre::Real deltaTime);


protected:
	Player *  _player;
	std::vector<Wave*> _waves;
	Ogre::Real _delay;
	Ogre::Real _currentDelay;
};



#endif /* EnemyAttacker_H_ */

