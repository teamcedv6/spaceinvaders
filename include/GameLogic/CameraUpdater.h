/*
 * CameraUpdater.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef CAMERAUPDATER_H_
#define CAMERAUPDATER_H_

#include <Ogre.h>
#include "Player.h"

class CameraUpdater {
private:
	Ogre::Camera * _camera;
	Player * _player;
	int _distance;
	bool _inertia;

	/* Camera position, velocity and height */
	double _pc;
	double _wc;
	float _h;

public:
	CameraUpdater();
	CameraUpdater(Ogre::Camera * camera, Player * player, int distance, float _height);
	CameraUpdater(const CameraUpdater & CameraUpdater);
	~CameraUpdater();
	CameraUpdater& operator = (const CameraUpdater &CameraUpdater);

	void update(Ogre::Real deltaTime);
};


#endif /* CameraUpdater_H_ */
