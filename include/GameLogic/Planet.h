/*
 * Planet.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef PLANET_H_
#define PLANET_H_

#include "Ogre.h"
#include "ActionResults.h"
#include <string>
#include "Observable.h"

class Planet : public Observable {
public:

	Planet();
	Planet(std::string name, std::string mesh, float speed, float size, float radio);
	Planet(const Planet & planet);
	~Planet();
	Planet& operator = (const Planet &planet);

	void createNode();
	void showNode();
	void destroyNode();

	std::string & getName();

	ActionResult update(Ogre::Real deltaTime);

	void setInitialPosition(float x, float y, float z);

private:

	Ogre::SceneNode * _node;
	std::string _name;
	std::string _mesh;
	float _speed;
	float _size;
	float _radio;

};



#endif /* Planet_H_ */
