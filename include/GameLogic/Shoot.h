/*
 * Shoot.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef SHOOT_H_
#define SHOOT_H_

#include <Ogre.h>
#include <string>
#include "Observable.h"

class Shoot : public Observable {
public:
	enum ShootType {
		SHOOT_NORMAL
	};

	Shoot();
	Shoot(std::string name, int direction, ShootType type, float height);
	Shoot(const Shoot & shoot);
	~Shoot();
	Shoot& operator = (const Shoot &shoot);

	void createNode();
	void showNode();
	void destroyNode();

	std::string & getName();

	bool update(Ogre::Real deltaTime);

	void setInitialPosition(float x, float y, float z);

private:
	static constexpr  float _maxHeight = 40;
	static constexpr  float _minHeight = -5;

	//const static constexpr  float _maxHeight = 20;
	//const static constexpr  float _minHeight = -5;

	Ogre::SceneNode * _node;
	std::string _name;
	ShootType _type;
	int _direction;
	float _v;
	// Shoot height, so we can make it appear at the right position
	float _height;
};


#endif /* Shoot_H_ */
