/*
 * Player.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <Ogre.h>
#include "Ship.h"
#include "ActionResults.h"

class Player : public Ship{

friend class CameraUpdater;

private:
	int _shield;
	double _maxW;
	double _maxV;
	double _w;
	Ogre::Real _shootDelay;
	Ogre::Real _currentDelay;

public:
	Player();
	Player(std::string name, int lifes, int shield, Ogre::Real shootDelay);
	Player(const Player & player);
	~Player();
	Player& operator = (const Player &player);

	void setMaxVelocity(double maxV);
	void calculateAngularVelocity();
	int getShield();

	void shoot();
	ActionResult receiveImpact();
	ActionResult update(Ogre::Real deltaTime);;
	void move(Direction direction);
	void stop();
};


#endif /* Player_H_ */
