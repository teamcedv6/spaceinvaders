/*
 * Ship.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef SHIP_H_
#define SHIP_H_

#include <Ogre.h>
#include "Observable.h"
#include "Shoot.h"
#include "ActionResults.h"

class Ship : public Observable{
public:

	enum Direction {
		DIRECTION_RIGHT = -1,
		DIRECTION_LEFT =   1,
		DIRECTION_UP = 2,
		DIRECTION_DOWN = 3
	};

	Ship();
	Ship(std::string name, int lifes, Shoot::ShootType shoot);
	Ship(const Ship & ship);
	virtual ~Ship();
	Ship& operator = (const Ship &ship);

	void createNode();
	void showNode();
	void destroyNode();
	void setInitialPosition(float x, float y, float z);
	void setInitialPosition(float position, float height);
	void setRadius(int radius);

	std::string & getName();
	double getPosition();
	const int getLifes();

	virtual ActionResult update(Ogre::Real deltaTime) = 0;
	virtual ActionResult receiveImpact();
	virtual void shoot() = 0;

protected:
	std::string _name;
	int _lifes;
	Shoot::ShootType _shoot;
	Ogre::SceneNode * _node;

	double _p;
	float _h;
	int _r;


	void createShoot(Direction direction);
};



#endif /* SHIP_H_ */
