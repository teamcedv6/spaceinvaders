/*
 * Wall.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef WALL_H_
#define WALL_H_



class Wall {
public:

	Wall();
	Wall(std::string name, int radius);
	Wall(const Wall & wall);
	virtual ~Wall();
	Wall& operator = (const Wall &wall);

protected:
	int _r;
};



#endif /* Wall_H_ */
