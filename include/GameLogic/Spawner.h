/*
 * Spawner.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef SPAWNER_H_
#define SPAWNER_H_

#include "Enemy.h"
#include "Configuration.h"

class Spawner {
public:

	Spawner();
	Spawner(const Spawner & spawner);
	virtual ~Spawner();
	Spawner& operator = (const Spawner &spawner);

	Enemy cloneEnemy(std::string name);
	void addEnemyModel(Configuration::Enemy_t::enemies_t & enemyData);

protected:
	std::map<std::string, Enemy> _enemyModels;
};



#endif /* Spawner_H_ */

