/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef LEVEL_CREATOR_H
#define LEVEL_CREATOR_H

#include <vector>
#include <iostream>

#include "Configuration.h"
#include "LevelCreator.h"

#include "Level.h"
#include "Planet.h"
#include "Wave.h"
#include "Shoot.h"
#include "Enemy.h"
#include "PlayState.h"

using namespace std;

class LevelCreator {
	public:
		LevelCreator();
		~LevelCreator();
		void createLevel(Configuration* configuration);
};

#endif /* LEVEL_CREATOR_H */
