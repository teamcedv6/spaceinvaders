/*
 * Wave.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef WAVE_H_
#define WAVE_H_

#include <Ogre.h>
#include "Observable.h"
#include "Enemy.h"


class Wave : public Observable{
public:
	enum Movement{
		LINE = 1,
		ZIGZAG = 2
	};

	Wave();
	Wave(std::string name, std::vector<Enemy> * enemies, int columnsNumber, Movement movement);
	Wave(const Wave & wave);
	virtual ~Wave();
	Wave& operator = (const Wave &wave);



	void createNode();
	void showNode();
	void destroyNode();
	void setInitialPosition(float x, float y, float z);
	void setInitialPosition(float position, float height);
	void calculateEnemiesInitialPosition();
	void setMaxVelocity(double maxV);
	void setRadius(int radius);

	std::string & getName();
	std::vector<Enemy *> getPotencialAttackers();
	int getEnemyNumber();

	ActionResult update(Ogre::Real deltaTime);
	ActionResult receiveImpact(int row, int column);
	void shoot();

protected:
	std::string _name;
	std::vector<Enemy> * _enemiesColumns;
	int _columnsNumber;
	int _enemiesNumber;

	double _p;
	double _p0;
	double _w;
	double _v;
	int _r;
	float _h;

	Ship::Direction _movementDirection;

	float _currentWidthDistance;
	float _currentHeightDistance;
	float _widthDistance;
	float _heightDistance;
	float _widthSeparation;
	float _heightSeparation;
	Movement _movement;

	ActionResult positionEnemies(Ogre::Real deltaTime);
};



#endif /* Wave_H_ */
