/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <map>
#include <tuple>

using namespace std;

// Options for Game
class Configuration{
	public:
		Configuration();
		~Configuration();
		Configuration(const Configuration & configuration);
		Configuration& operator = (const Configuration &configuration);

		// Config Enemy
		typedef struct Enemy {
			char* _config_enemy_path;
			char* _resource_type;
			typedef struct enemies {
				char* _name;
				char* _mesh;
				float _life;
				float _size;
				float _height;
				float _length;
				float _points;
				string _impact_fx;
				string _destroy_fx;
				string _shoot;
				string _shield;
			} enemies_t;
			map<string,enemies_t> _enemies;
		} Enemy_t;

		// Config Fx
		typedef struct Fx {
			char* _config_fx_path;
			char* _resource_type;
			map<string,tuple<string,float,char*> > _fxs;
		} Fx_t;

		// Config Level
		typedef struct Level {
			char* _config_level_path;
			char* _resource_type;
			int _init_level;
			int _currentLevel;
			typedef struct levels {
				string _music;
				string _planet;
				int _last;
				double _radio;
				typedef struct waves
				{
					string _move;
					int _split;
					float _speed;
					float _angle;
					float _height;
					typedef struct enemies
					{
						string _enemy;
						int _number;
					} enemies_t;
					map<string,enemies_t> _enemies;
				} waves_t;
				map<int,waves_t> _waves;
			} levels_t;
			map<int,levels_t> _levels;
		} Level_t;

		/*// Config Movement
		typedef struct Movement {
			char* _config_movement_path;
			char* _resource_type;
			typedef struct movements {
				char* _parameters;
				float _speed;
			} movements_t;
			map<string,movements_t> _movements;
		} Movement_t;*/

		// Config Music
		typedef struct Music {
			char* _config_music_path;
			char* _resource_type;
			map<string,tuple<string,float,char*> >  _tracks;
		} Music_t;

		// Config Options
		typedef struct Options {
			char* _fx;
			char* _music;
			string _loadFx;
			string _loadMusic;
		} Options_t;

		// Config UI
		typedef struct Ui {
			string _fx;
			string _music;
		} Ui_t;

		// Config Planet
		typedef struct Planet {
			char* _config_planet_path;
			char* _resource_type;
			typedef struct planets {
				char* _name;
				char* _mesh;
				float _speed;
				float _size;
				double _radio;
			} planets_t;
			map<string,planets_t> _planets;
		} Planet_t;

		// Config Player
		typedef struct Player {
			char* _config_player_path;
			char* _resource_type;
			typedef struct players {
				char* _name;
				char* _mesh;
				float _life;
				char* _shoot;
				char* _shield;
				float _size;
				float _height;
				float _length;
				float _speed;
				string _impact_fx;
				string _destroy_fx;
			} players_t;
			map<string,players_t> _players;
		} Player_t;

		// Config Shield
		typedef struct Shield {
			char* _config_shield_path;
			char* _resource_type;
			typedef struct shields {
				char* _name;
				char* _mesh;
				float _life;
				float _size;
				string _impact_fx;
				string _destroy_fx;
			} shields_t;
			map<string,shields_t> _shields;
		} Shield_t;

		// Config Shoot
		typedef struct Shoot {
			char* _config_shoot_path;
			char* _resource_type;
			typedef struct shoots {
				char* _name;
				char* _mesh;
				float _size;
				char* _move;
				string _impact_fx;
				string _destroy_fx;
				float _speed;
			} shoots_t;
			map<string,shoots_t> _shoots;
		} Shoot_t;

		// Config Score
		typedef struct Score {
			char* _config_score_path;
			char* _resource_type;
		} Score_t;

		// Setters Options
		void setConfigOptionMusic(char* path);
		void setConfigOptionFx(char* path);
		void setConfigOptionLoadMusic(string path);
		void setConfigOptionLoadFx(string path);

		// Setters UI
		void setConfigUiMusic(string path);
		void setConfigUiFx(string path);

		// Setters Enemy
		void setConfigEnemyPath(char* path);
		void setConfigEnemyType(char* path);
		void addEnemy(string index,Enemy::enemies_t enemy);

		// Setters Fx
		void setConfigFxPath(char* path);
		void setConfigFxType(char* path);
		void addFx(string index,char* fx, float volume);

		// Setters Levels
		void setConfigLevelPath(char* path);
		void setConfigLevelType(char* path);
		void setConfigLevelInit(int initLevel);
		void addLevel(int index,Level::levels_t level);

		/*// Setters Movement
		void setConfigMovementPath(char* path);
		void setConfigMovementType(char* path);
		void addMovement(string index,Movement::movements_t movement);*/

		// Setters music
		void setConfigMusicPath(char* path);
		void setConfigMusicType(char* path);
		void addMusic(string index,char* track, float volume);

		// Setters Planet
		void setConfigPlanetPath(char* path);
		void setConfigPlanetType(char* path);
		void addPlanet(string index,Planet::planets_t planet);

		// Setters Player
		void setConfigPlayerPath(char* path);
		void setConfigPlayerType(char* path);
		void addPlayer(string index,Player::players_t player);

		// Setters Shield
		void setConfigShieldPath(char* path);
		void setConfigShieldType(char* path);
		void addShield(string index,Shield::shields_t shield);

		// Setters Shoot
		void setConfigShootPath(char* path);
		void setConfigShootType(char* path);
		void addShoot(string index,Shoot::shoots_t shoot);

		// Setters Score
		void setConfigScorePath(char* path);
		void setConfigScoreType(char* path);

		// Getters
		Options_t* getOptionConfig();
		Ui_t* getUiConfig();
		Enemy_t* getEnemyConfig();
		Fx_t* getFxConfig();
		Level_t* getLevelConfig();
		//Movement_t* getMovementConfig();
		Music_t* getMusicConfig();
		Planet_t* getPlanetConfig();
		Player_t* getPlayerConfig();
		Shield_t* getShieldConfig();
		Shoot_t* getShootConfig();
		Score_t* getScoreConfig();

	private:

		Options_t* _optionsConfig;
		Ui_t* _uiConfig;
		Enemy_t* _enemyConfig;
		Fx_t* _fxConfig;
		Level_t* _levelConfig;
		//Movement_t* _movementConfig;
		Music_t* _musicConfig;
		Planet_t* _planetConfig;
		Player_t* _playerConfig;
		Shield_t* _shieldConfig;
		Shoot_t* _shootConfig;
		Score_t* _scoreConfig;

};
#endif
