/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef SOUND_SYSTEM_H
#define SOUND_SYSTEM_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include <map>
#include <tuple>
#include <string>

#include "TrackManager.h"
#include "SoundFXManager.h"
#include "XMLManageSound.h"

#include "Observer.h"
#include "Event.h"

using namespace std;

class SoundManager : public Observer{
	public:
		SoundManager();
		~SoundManager();
		SoundManager(const SoundManager & soundManager);
		SoundManager& operator = (const SoundManager &soundManager);
		void selectResource(string type, string key,string action);
		void start(Configuration* configuration);
		void setSelectFx(string element, string hit, string dead, string shoot);

	private:

		// Init SDL
		bool _initSDL();

		void process(Event& _event);

		// Load resources
		void loadMusic(Configuration* configuration);
		void loadFx(Configuration* configuration);

		// Sounds Manager
		TrackManager* _pTrackManager;
		SoundFXManager* _pSoundFXManager;

		// Sounds & Fx
		map<string, TrackPtr>* _musicList;
		map<string, SoundFXPtr>* _fxList;
		map<string,tuple<string,string,string> >* _selectFx;
};

#endif
