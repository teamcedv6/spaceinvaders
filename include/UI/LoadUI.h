/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef LOAD_UI_H
#define LOAD_UI_H

#include "UserInterface.h"
#include <string>
#include <iostream>
#include "GameManager.h"
#include "MainMenuState.h"
#include "PlayState.h"


class LoadUI : public UserInterface {
private:
	LoadUI (const LoadUI &);
  CEGUI::Window *sheet;
  CEGUI::Window *lay;
  CEGUI::Window *progressBarWindow;
  CEGUI::ProgressBar *progressBar;

public:
  LoadUI();
  ~LoadUI();
  void updateProgressBar();
  void createMenu();
  void destroyMenu();
  void setLevelNumber();
};
#endif /* LOAD_UI_H  */
