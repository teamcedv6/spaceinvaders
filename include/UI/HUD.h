/*
 * HUD.h
 *
 *  Created on: 28 feb. 2017
 *      Author: joe
 */

#ifndef HUD_H
#define HUD_H
#include "UserInterface.h"
#include "vector"

class HUD : public UserInterface {
  private:
    int _lifes;
    int _shield;
    int _score;
    float _lifebar;
    HUD(const HUD &);
    CEGUI::Window *sheet;
    CEGUI::Window *lay;
    CEGUI::Window *pbar;
    std::vector <CEGUI::Window *> plife;
    std::vector <CEGUI::Window *> pshield;
    CEGUI::Window *pscore;
    std::stringstream ss;
    std::string str;

  public:
    HUD();
    ~HUD();
    void setLifes(int n);
    void setShield(int n);
    void substractLife();
    void substractShield();
    void addShield();
    void addLife();
    void addScore(int score);
    int getLifes() const;
    int getShield() const;
    int getScore() const;
    void changeLbar(float f);
    void intostring();
    void createHUD(int life,int shield);
    void addLifeGUI();
    void removeLifeGUI();
    void addShieldGUI();
    void removeShieldGUI();
    /*void dynamicHUD();*/
    void destroyHUD();
    void setLevelLabel(std::string text);
    void setLevelLabelAlpha(float alpha);
};
#endif /* INCLUDE_STATES_HUD_H_ */
