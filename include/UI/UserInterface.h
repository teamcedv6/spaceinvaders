/*
 * UserInterface.h
 *
 *  Created on: 22 feb. 2017
 *      Author: joe
 */

#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>

class UserInterface {
  protected:
    CEGUI::OgreRenderer* renderer;
    UserInterface (const UserInterface &);
  public:
    UserInterface();
    ~UserInterface();
    void createInterface();
    void destroyInterface();
};
#endif /* USERINTERFACE_H */
