/*
 * MenuUI.h
 *
 *  Created on: 22 feb. 2017
 *      Author: joe
 */

#ifndef MENUUI_H
#define MENUUI_H

#include "UserInterface.h"


class MenuUI : public UserInterface {
private:
  MenuUI (const MenuUI &);
  CEGUI::Window *sheet;
  CEGUI::Window *lay;
  CEGUI::Window *button;
  CEGUI::Window *splash;
  int mask;


public:
  MenuUI();
  ~MenuUI();
  void createMenu(const int &n);
  void destroyMenu();
  bool cscallback(const CEGUI::EventArgs &e);
  void ShowScores();
};
#endif /* MENUUI_H  */
