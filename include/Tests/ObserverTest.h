/*
 * ObserverTest.h
 *
 *  Created on: Feb 4, 2017
 *      Author: root
 */

#ifndef INCLUDE_TESTS_OBSERVERTEST_H_
#define INCLUDE_TESTS_OBSERVERTEST_H_

#include "Observer.h"
#include "Observable.h"
#include "Event.h"
#include <iostream>
#include <assert.h>


class MyObserver : public Observer {
private:
	int n = 0;
protected:
	void receive(Event &event) {
		std::cout << "Evento recibido\n";
		n++;

		assert(n <= 1);
	}
};

class MyObservable : public Observable {
public:
	void sendTest() {
		this->send(_event);
	}

};

int observerTest();

#endif /* INCLUDE_TESTS_OBSERVERTEST_H_ */
