/*
 * ScoreBoard.h
 *
 *  Created on: 10 dic. 2016
 *      Author: joe
 */

#ifndef INCLUDE_SCOREBOARDSTATE_H_
#define INCLUDE_SCOREBOARDSTATE_H_

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"


class ScoreBoardState : public Ogre::Singleton<ScoreBoardState>, public GameState
{
public:
	ScoreBoardState() {}

	enum ScoreType {
		  READ=0,
		  WRITE=1
	  };
	void setType(ScoreType type);
	// Gestion de estados
	void enter();
	void exit();
	void pause();
	void resume();

	// Gestion de eventos de teclado y ratón
	void keyPressed (const OIS::KeyEvent &e);
	void keyReleased (const OIS::KeyEvent &e);

	void mouseMoved (const OIS::MouseEvent &e);
	void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
	void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

	// Gestion de eventos antes y después de renderizar fames
	bool frameStarted (const Ogre::FrameEvent& evt );
	bool frameEnded (const Ogre::FrameEvent& evt);

	// Camino de instaciar objetos heredado de OGRE::Singleton
	static ScoreBoardState& getSingleton();
	static ScoreBoardState* getSingletonPtr();

protected:
	Ogre::Root* _root;
	Ogre::SceneManager* _sceneMgr;
	Ogre::Viewport* viewport;
	Ogre::Camera* _camera;

	// Menú
	MenuUI* _menu;

	ScoreType _type;

	// Variables para CEGUI
  float _timeSinceLastFrame;
  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

	bool _exitGame;

};


#endif /* INCLUDE_SCOREBOARDSTATE_H_ */
