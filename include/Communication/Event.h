/*
 * Observable.h
 *
 *  Created on: Feb 1, 2017
 *      Author: root
 */

#ifndef EVENT_H_
#define EVENT_H_

#include <Ogre.h>
#include <OgreNode.h>
#include <string>
#include <iostream>
#include <cassert>

// At least up to 32 bits (4 bytes) of different event types
typedef unsigned int EventType_t;

//friend class Observer;
//friend class Observable;

class Event {


public:
	Event();
	Event(const Event & event);
	virtual ~Event();
	Event& operator = (const Event &t);

	struct NodeAndString{
		Ogre::SceneNode** node; std::string * name;
	};

	struct ArrayAndString{
		std::string * parent; std::string ** names; int n;
	};

	struct DoubleString {
		std::string * name1; std::string * name2;
	};

	/*struct StringAndPosition {
		std::string * name; float p; float h;
	};*/

	/**	create 		-> entity created but not visible in game
	 * 	ingame 		-> entity becomes visible
	 * 	move   		-> entity performs a physical movement
	 * 	shoot  		-> entity shoots
	 * 	destroy		-> entity is destroyed (i.e. loses all lifes)
	 * 	hit			-> entity gets hit (i.e. loses a life)
	 * 	dead		-> entity deads (it has no more lifes)
	 * 	collision	-> collision detectd between two entities
	 * 	addpoints	-> point count has been increased
	 * 	itemchange	-> menu selected item has changed
	 * 	itempush	-> menu item has been pushed
	 */

	// General entities actions
	const static EventType_t TYPE_CREATE	 	= 1 << 0;
	const static EventType_t TYPE_INGAME	 	= 1 << 1;
	const static EventType_t TYPE_MOVE 			= 1 << 2;
	const static EventType_t TYPE_SHOOT 	 	= 1 << 3;
	const static EventType_t TYPE_DEAD			= 1 << 4;
	const static EventType_t TYPE_DESTROY		= 1 << 5;
	const static EventType_t TYPE_HIT 	   		= 1 << 6;
	const static EventType_t TYPE_COLLISION 	= 1 << 7;

	// Score system
	const static EventType_t TYPE_ADDPOINTS  	= 1 << 8;
	// Menu actions
	const static EventType_t TYPE_ITEMCHANGE 	= 1 << 9;
	const static EventType_t TYPE_ITEMPUSH   	= 1 << 10;

	const EventType_t getType() const;
	const std::string & getName() const;

	void setType(unsigned int type);

	void setParameters(int n);
	void setParameters(std::string * n);
	void setParameters(std::string * parent, std::string * names[], int n);
	void setParameters(Ogre::SceneNode** node, std::string * name);
	void setParameters(std::string * name1, std::string * name2);
	//void setParameters(std::string * name, float p, float h);

	int getParametersInt();
	std::string& getParametersString();
	ArrayAndString& getParametersArrayAndString();
	NodeAndString& getParametersNodeAndString();
	DoubleString& getParametersDoubleString();

private:
	EventType_t _type;
	std::string _name;
	int _argType;

	union {
		int onlyInteger;
		std::string * onlyString;
		ArrayAndString arrayAndString;
		NodeAndString nodeAndString;
		DoubleString doubleString;
		//StringAndPosition stringAndPosition;
	} _parameters;
};

#endif /* EVENT_H_ */
