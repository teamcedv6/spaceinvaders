/*
 * Observable.h
 *
 *  Created on: Feb 1, 2017
 *      Author: root
 */

#ifndef OBSERVABLE_H_
#define OBSERVABLE_H_

#include <vector>
#include "Observer.h"
#include "Event.h"

class Observable {
private:
	std::vector<Observer *> _observers;

protected:
	Event _event;
	EventType_t _type;

	Observable();
	/* Usar este constructor si sabemos cuantos observadores va a tener
	 * para evitar tener que ampliar la capacidad del vector.
	 */
	Observable(int expectedObservers, EventType_t type);
	Observable(const Observable & observable);
	virtual ~Observable();
	Observable& operator = (const Observable &observable);

public:
	void add(Observer * observer);
	void remove(Observer * observer);
	void send(Event &event);

	EventType_t getType();

	Event& getEvent();
};

#endif /* OBSERVABLE_H_ */
