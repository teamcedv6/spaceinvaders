/*
 * Observer.h
 *
 *  Created on: Feb 1, 2017
 *      Author: root
 */

#ifndef OBSERVER_H_
#define OBSERVER_H_

#include "Event.h"

class Observer {
protected:
	EventType_t _filter;

	Observer();
	Observer(EventType_t filter);
	Observer(const Observer& observer);
	virtual ~Observer();
	Observer& operator = (const Observer &observer);

	virtual void process(Event &event) = 0;

public:
	void receive(Event &event);
	const EventType_t getFilter() const;

};

#endif /* OBSERVER_H_ */
