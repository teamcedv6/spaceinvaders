/*
 * GraphicsSystem.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef GRAPHICSSYSTEM_H_
#define GRAPHICSSYSTEM_H_

#include <Ogre.h>
#include <string>
#include "Observer.h"
#include "Event.h"

class GraphicsSystem : public Observer{
private:
	Ogre::SceneManager * _sceneManager;

	void createNode(Ogre::SceneNode ** nodePtr, std::string * nodeName,std::string * mesh);
	void attachNode(std::string * parentName, std::string * nodeName);
	void dettachNode(const std::string & nodeName);
	void destroyNode(const std::string & nodeName);
	void process(Event& _event);

	GraphicsSystem(const GraphicsSystem & graphicsSystem);
	GraphicsSystem& operator = (const GraphicsSystem &graphicsSystem);

public:
	GraphicsSystem();
	GraphicsSystem(Ogre::SceneManager * sceneManager);
	~GraphicsSystem();


};


#endif /* GraphicsSystem_H_ */

