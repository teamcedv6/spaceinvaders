/*
 * AnimationSystem.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef ANIMATIONSYSTEM_H_
#define ANIMATIONSYSTEM_H_

#include <Ogre.h>
#include <vector>
#include "Observer.h"
#include "Event.h"

class AnimationSystem : public Observer {
private:
	int _deadNumber;

	std::vector<Ogre::AnimationState *> _animations;
	std::vector<Ogre::AnimationState *> _deadAnimations;
	std::vector<Ogre::SceneNode *> _deadNodes;

	Ogre::SceneManager * _sceneManager;

	void process(Event& _event);

	AnimationSystem(const AnimationSystem & animationSystem);
	AnimationSystem& operator = (const AnimationSystem & animationSystem);

public:
	AnimationSystem();
	AnimationSystem(Ogre::SceneManager * sceneManager);
	~AnimationSystem();

	void update(Ogre::Real deltaTime);
	void clear();
};


#endif /* ANIMATIONSYSTEM_H_ */

