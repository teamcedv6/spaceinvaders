/*
 * scoresystem.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef SCORESYSTEM_H_
#define SCORESYSTEM_H_

#include <map>
#include <string>

#include "HUD.h"
#include "Observer.h"
#include "Observable.h"
#include "Event.h"

using namespace std;

class ScoreSystem : public Observer {
private:

	std::string _playerName;
	std::map<std::string, int> _enemiesScores;
	int _currentScore;
	HUD * _hud;

	map<int,string> _xmlScores;

	void process(Event& _event);


	ScoreSystem(const ScoreSystem & scoreSystem);
	ScoreSystem& operator = (const ScoreSystem & scoreSystem);

public:
	ScoreSystem();
	~ScoreSystem();

	void addEnemyScore(std::string name, int score);
	void setPlayerName(std::string playerName);
	void setHUD(HUD * hud);

	// Add to current score and update HUD
	void addScore(int score);
	void addScore(std::string name);
	void setXmlScores(map<int,string> xmlScores);
	int getCurrentScore();
	map<int,string> getXmlScores();
	void saveScore();
	void clear();
};


#endif /* SCORESYSTEM_H_ */

