/*
 * CollisionSystem.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef COLLISIONSYSTEM_H_
#define COLLISIONSYSTEM_H_

#include <Ogre.h>
#include <vector>
#include "Observer.h"
#include "Observable.h"
#include "Event.h"

class CollisionSystem : public Observer, public Observable{
private:
	struct BoundingRectangle {
		std::string name;
		float x;
		float y;

		float xMin;
		float xMax;

		float yMin;
		float yMax;

		float width;
		float height;
	};

	int _r;
	int _L;

	std::map<std::string, std::pair<float, float> > _sizes;

	std::vector<BoundingRectangle> _collidable;
	std::vector<BoundingRectangle> _colliders;

	Ogre::SceneManager * _sceneManager;

	void initBoundingParameters(std::string * name, BoundingRectangle & br);
	void updateBoundingParameters(BoundingRectangle & br, Ogre::SceneNode * node);

	void process(Event& _event);
	bool collide(float x1Min, float x1Max, float y1Min, float y1Max,
			  float x2Min, float x2Max, float y2Min, float y2Max);

	CollisionSystem(const CollisionSystem & CollisionSystem);
	CollisionSystem& operator = (const CollisionSystem &CollisionSystem);

public:
	CollisionSystem();
	CollisionSystem(Ogre::SceneManager * sceneManager);
	~CollisionSystem();

	void setRadius(int radius);

	void registerCollisionReceiver(std::string * name);
	void registerCollisionProvider(std::string * name);

	void addCollisionShape(std::string name, float width, float height);

	void update();
	void clear();
};


#endif /* CollisionSystem_H_ */

