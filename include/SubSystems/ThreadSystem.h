/*********************************************************************
 * Minijuego 2 - Space Invaders - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef THREAD_SYSTEM_H
#define THREAD_SYSTEM_H

#include <IceUtil/IceUtil.h>
#include "GameManager.h"
#include "Configuration.h"
#include "SoundManager.h"

#include "LevelCreator.h"

#include "XMLManageConfig.h"
#include "XMLManageSound.h"
#include "XMLManageEnemy.h"
#include "XMLManagePlanet.h"
#include "XMLManageLevel.h"
#include "XMLManagePlayer.h"
#include "XMLManageShoot.h"
#include "XMLManageShield.h"
#include "XMLManageScore.h"
//#include "XMLManageMovement.h"

using namespace std;

class ThreadSystem : public IceUtil::Thread {

	public:

		enum type {
			CONFIG 	= 0,
			LOAD 	= 1,
			DELAY 	= 2,
			TEST 	= 3
		};

		ThreadSystem (int type);
		ThreadSystem (int type,const IceUtil::Time& delay);
		virtual void run ();

	private:
		void basicConfigLoad();
		void loadLevel();

		IceUtil::Time _delay;
		int _type;

		GameManager * _gameManager;
};

typedef IceUtil::Handle<ThreadSystem> ThreadPtr; // Smart pointer.

#endif
