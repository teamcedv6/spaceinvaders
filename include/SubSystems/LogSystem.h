/*
 * LogSystem.h
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#ifndef LOGSYSTEM_H_
#define LOGSYSTEM_H_

#include <string>
#include "Observer.h"
#include "Event.h"

class LogSystem : public Observer{
private:
	void writeLog(const std::string & str);

	LogSystem(const LogSystem & LogSystem);
	LogSystem& operator = (const LogSystem &LogSystem);

	/**
	 * 	0 -> no log
	 * 	1 -> error
	 * 	2 -> warning
	 * 	3 -> info
	 * 	4 -> debug
	 * 	5 -> all
	 */
	int _logLevel;
	void process(Event& _event);

public:
	LogSystem();
	LogSystem(int logLevel);
	~LogSystem();


};


#endif /* LOGSYSTEM_H_ */

