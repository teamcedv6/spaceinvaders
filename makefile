# --------------------------------------------------------------------
# Makefile Mastermind :: Minijuego 1. Curso Experto Desarrollo de Videojuegos 
# Basado en Makefile Genérico :: Módulo 2. CEDV por Carlos González Morcillo ESI UCLM
# Modificado or Gonzalo Trigueros Manzanas para permitir separación de .cpp y .h en 
# varios subdirectorios
# --------------------------------------------------------------------
EXEC := spaceinvaders
MODULES := Sound/ States/ UserInterface/ Communication/ GameLogic/ XML/ Configuration/ SubSystems/ UI/

# Compilar test para ejecutar al arranque  
ifeq ($(tests), only)	
	MODULES := $(MODULES) Tests/	
	DEFINETAGS := -D_TESTONLY
else
	ifeq ($(tests), continue)
		MODULES := $(MODULES) Tests/
		DEFINETAGS := -D_TESTCONTINUE	
	else
		tests := off
	endif
endif


DIRSRC := src/ $(addprefix src/,$(MODULES))
DIROBJ := obj/ $(addprefix obj/,$(MODULES))
DIRHEA := include/ $(addprefix include/,$(MODULES))



CXX := g++

# Flags de compilación -----------------------------------------------
CXXFLAGS := $(DEFINETAGS) $(addprefix -I,$(DIRHEA)) -Wall -std=c++11 `pkg-config --cflags OIS OGRE OGRE-Overlay CEGUI-0 CEGUI-0-OGRE`  

# Flags del linker ---------------------------------------------------
LDFLAGS := `pkg-config --libs-only-L OGRE CEGUI-0 CEGUI-0-OGRE`
LDLIBS := `pkg-config --libs-only-l gl OIS OGRE OGRE-Overlay CEGUI-0 CEGUI-0-OGRE SDL2_mixer xerces-c` -lstdc++ -lboost_system -lIceUtil -lpthread

# Modo de compilación (-mode=release -mode=debug) --------------------
ifeq ($(mode), release) 
	CXXFLAGS += -O2 -D_RELEASE
else 
	ifeq ( $(mode), debugconsole)
		CXXFLAGS += -D_CONSOLE	
	endif
	CXXFLAGS += -g -D_DEBUG
	mode := debug
endif

# Obtención automática de la lista de objetos a compilar -------------
OBJS := $(foreach dir,$(DIRSRC), \
	$(subst src/, obj/, \
	$(patsubst %.cpp, %.o, $(wildcard $(dir)*.cpp))))

# Función para compilar en rutas genéricas ---------------------------
define make-target
obj/%.o : $(subst obj/, src/, $(1))%.cpp	
	$$(CXX) $$(CXXFLAGS) -c $$< -o $$@ 
endef

.PHONY: all clean

all: info dirs $(EXEC)

info:
	@echo '------------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '    (Please, call "make" with [mode=debug|release])  '
	@echo '------------------------------------------------------'

dirs:
	mkdir -p $(DIROBJ)

# Enlazado -----------------------------------------------------------
$(EXEC): $(OBJS)
	@echo $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# Crear targets para los distintos .obj gracias a la función ---------
# make-target previamente definida -----------------------------------
$(foreach dir,$(DIROBJ), $(eval $(call make-target,$(dir))))

# Limpieza de temporales ---------------------------------------------
clean:
	rm -f *.log $(EXEC) *~ $(addsuffix *~,$(DIRSRC)) $(addsuffix *~, $(DIRHEA))
	rm -rf $(DIROBJ)
