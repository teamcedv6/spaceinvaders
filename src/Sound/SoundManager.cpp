
#include "SoundManager.h"

SoundManager::SoundManager() : Observer(Event::TYPE_SHOOT | Event::TYPE_HIT | Event::TYPE_DEAD) {
	// Init SDL
	_initSDL();

	// Sounds Managers
	_pTrackManager = new TrackManager;
	_pSoundFXManager = new SoundFXManager;

	// Resources
	_musicList = new map<string, TrackPtr>;
	_fxList = new map<string, SoundFXPtr>;
	_selectFx = new map<string,tuple<string,string,string> >;

}

SoundManager::~SoundManager () {
	delete _pTrackManager;
	delete _pSoundFXManager;
	delete _musicList;
	delete _fxList;
	delete _selectFx;
}

SoundManager::SoundManager(const SoundManager & soundManager) {

	_pTrackManager = soundManager._pTrackManager;
	_pSoundFXManager = soundManager._pSoundFXManager;
	_musicList = soundManager._musicList;
	_fxList = soundManager._fxList;
	_selectFx = soundManager._selectFx;

}


SoundManager& SoundManager::operator = (const SoundManager &soundManager){

	_pTrackManager = soundManager._pTrackManager;
	_pSoundFXManager = soundManager._pSoundFXManager;
	_musicList = soundManager._musicList;
	_fxList = soundManager._fxList;
	_selectFx = soundManager._selectFx;

	return *this;

}

void SoundManager::start (Configuration* configuration)
{
	loadMusic(configuration);
	loadFx(configuration);
}

void SoundManager::selectResource(string type, string key,string action) {

	if(type =="music"){
		if(action == "play"){
				(*_musicList)[key]->play();
			}else if(action == "stop"){
				(*_musicList)[key]->stop();
			}

	} else if(type =="fx"){
		(*_fxList)[key]->play();
	}
}

void SoundManager::loadMusic(Configuration* configuration) {
	 for (map<string,tuple<string,float,char*> >::iterator it = configuration->getMusicConfig()->_tracks.begin(); it != configuration->getMusicConfig()->_tracks.end(); ++it){
		 TrackPtr myMusic = _pTrackManager->load(get<2>(it->second),configuration->getMusicConfig()->_resource_type);
		 (*_musicList)[get<0>(it->second)] = myMusic;
	 }
}

void SoundManager::loadFx(Configuration* configuration) {
	for (map<string,tuple<string,float,char*> >::iterator it = configuration->getFxConfig()->_fxs.begin() ; it != configuration->getFxConfig()->_fxs.end(); ++it){
		 SoundFXPtr myFx = _pSoundFXManager->load(get<2>(it->second),configuration->getFxConfig()->_resource_type);
		 (*_fxList)[get<0>(it->second)] = myFx;
	 }
}

void SoundManager::process(Event& _event){
	if(_event.getType() == Event::TYPE_SHOOT){
		Event::ArrayAndString& params = _event.getParametersArrayAndString();
		std::string str2 = (params.parent)->substr (2,5);
		(*_fxList)[get<2>((*_selectFx)[str2])]->play();
		//(*_fxList)[key]->play();
	}
	if(_event.getType() == Event::TYPE_DEAD){
		auto name= _event.getParametersString();
		std::string str2 = name.substr (2,5);
		(*_fxList)[get<1>((*_selectFx)[str2])]->play();
	}
	if(_event.getType() == Event::TYPE_HIT){
		auto name= _event.getParametersString();
		std::string str2 = name.substr (2,5);
		(*_fxList)[get<0>((*_selectFx)[str2])]->play();
	}
}

void SoundManager::setSelectFx(string element, string hit, string dead, string shoot) {
	(*_selectFx)[element] = make_tuple(hit,dead,shoot);
}

bool SoundManager::_initSDL() {
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    return false;
  }
  // Llamar a  SDL_Quit al terminar.
  atexit(SDL_Quit);

  // Inicializando SDL mixer...
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0) {
    return false;
  }

  // Llamar a Mix_CloseAudio al terminar.
  atexit(Mix_CloseAudio);

  return true;
}
