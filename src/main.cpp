/*********************************************************************
 * Minijuego 1 - Mastermind - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 *
 * Para estructurar el juego se utilizará un esquema basado en estados
 * proporcionado por David Vallejo Fernández en M1.13 El bucle de juego
 *
 * GameStates
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * M1.13 El bucle de juego
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "PlayState.h"
#include "PauseState.h"
#include "AuthorState.h"
#include "ScoreBoardState.h"
#include "ConfigurationState.h"
#include "MainMenuState.h"
#include <States/LoadState.h>

#if defined _TESTONLY || _TESTCONTINUE
	#include "Test.h"
	#include "TestState.h"
#endif

#if defined _TESTONLY || _TESTCONTINUE
	#include "Test.h"
	#include "TestState.h"
#endif

#include <iostream>

using namespace std;

int main () {

  GameManager* game = new GameManager();
  PlayState* playState = new PlayState();
  PauseState* pauseState = new PauseState();
  AuthorState* authorState = new AuthorState();
  ScoreBoardState* scoreBoardstate= new ScoreBoardState();
  MainMenuState* mainMenuState = new MainMenuState();
  LoadState* loadState=new LoadState();
  ConfigurationState* configurationState=new ConfigurationState();

  UNUSED_VARIABLE(playState);
  UNUSED_VARIABLE(pauseState);
  UNUSED_VARIABLE(authorState);
  UNUSED_VARIABLE(scoreBoardstate);
  UNUSED_VARIABLE(mainMenuState);
  UNUSED_VARIABLE(loadState);
  UNUSED_VARIABLE(configurationState);

  try
    {
	  #if defined _TESTONLY || _TESTCONTINUE
	  	  TestState* testState = new TestState();
	  	  initTests();
	  	  game->start(TestState::getSingletonPtr());
	  #else
	  	// Inicializa el juego y transición al primer estado.
	  	//game->start(PlayState::getSingletonPtr());
	  	LoadState::getSingletonPtr()->setType(LoadState::INIT);
	  	game->start(LoadState::getSingletonPtr());

	  #endif
    }
  catch (Ogre::Exception& e)
    {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }
  
  delete game;
  
  return 0;
}
