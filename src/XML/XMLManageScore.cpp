#include "XMLManageScore.h"


#include <iostream>
#include <string>
#include <sstream>
//Mandatory for using any feature of Xerces.
#include <xercesc/util/PlatformUtils.hpp>
//Use the Document Object Model (DOM) API
#include <xercesc/dom/DOM.hpp>
//Required for outputing a Xerces DOMDocument to a standard output stream (Also see: XMLFormatTarget)
#include <xercesc/framework/StdOutFormatTarget.hpp>
//Required for outputing a Xerces DOMDocument to the file system (Also see: XMLFormatTarget)
#include <xercesc/framework/LocalFileFormatTarget.hpp>



using namespace std;
XERCES_CPP_NAMESPACE_USE


void DoOutput2File(DOMDocument* pmyDOMDocument, XMLCh * FullFilePath );




XMLManageScore::XMLManageScore ():XMLManager(){
	SCORE_TAG = "score";
	NAME_TAG = "name";
	VALUE_TAG = "value";
	INDEX_ATTRIBUTE = "index";

}

XMLManageScore::~XMLManageScore (){
	/*delete SCORE_TAG;
	delete NAME_TAG;
	delete VALUE_TAG;
	delete INDEX_ATTRIBUTE;*/
}
void XMLManageScore::loadXML(Configuration* configuration) {

}
void XMLManageScore::loadXML(Configuration* configuration,ScoreSystem* scoreSystem) {
	// Init parser with shoot.xml
	initParser(configuration->getScoreConfig()->_config_score_path);
	// Convert string tags in int
	XMLCh* scoreTag = XMLString::transcode(SCORE_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* nameTag = XMLString::transcode(NAME_TAG);
	XMLCh* valueTag = XMLString::transcode(VALUE_TAG);
	map<int,string> _scores;
	// Proccess root node <shoot>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* scoreNode = _elementRoot->getChildNodes()->item(i);
		// If <shoot>??
		if (scoreNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(scoreNode->getNodeName(), scoreTag)) {
			// Prepare shoot
			string index = XMLManager::getAttributeValue(scoreNode,indexAttribute);
			string name;
			int value;

			// Proccess root node <shoot>
			for (XMLSize_t i = 0; i < scoreNode->getChildNodes()->getLength(); ++i ) {
				DOMNode* scoreChildrenNode = scoreNode->getChildNodes()->item(i);
				// Name
				if (scoreChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(scoreChildrenNode->getNodeName(), nameTag)) {
					name = XMLManager::getNodeValue(scoreChildrenNode);
				// Life
				}else if(scoreChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(scoreChildrenNode->getNodeName(), valueTag)){
					value = atoi(XMLManager::getNodeValue(scoreChildrenNode));
				// Mesh
				}
			}
			_scores[value] = name;
		}
	}
	scoreSystem->setXmlScores(_scores);
}
void XMLManageScore::saveXML(Configuration* configuration){

}


void XMLManageScore::saveXML(Configuration* configuration,ScoreSystem* scoreSystem,string name){
	 // Initilize Xerces.

	  initParser(configuration->getScoreConfig()->_config_score_path);


	  // Pointer to our DOMDocument.
	  DOMDocument*        p_DOMDocument = _xmlDoc;

	  XMLCh tempAttribute[100];
	  XMLString::transcode("Hello_World", tempAttribute, 99);


	  //p_DOMDocument = p_DOMImplementation->createDocument(0, XMLString::transcode("hello"), 0);

	  DOMElement * p_RootElement = NULL;
	  p_RootElement = p_DOMDocument->getDocumentElement();

	  /*Fill in the DOM document - different parts
	  //Create a Comment node, and then append this to the root element.
	  DOMComment * p_DOMComment = NULL;
	  p_DOMComment = p_DOMDocument->createComment(XMLString::transcode("comment"));
	  p_RootElement->appendChild(p_DOMComment);*/

	  //Create an Element node, then fill in some attributes, and then append this to the root element.
	  DOMElement * p_DataElement = NULL;
	  p_DataElement = p_DOMDocument->createElement(XMLString::transcode("score"));

	  //Copy the current system date to a buffer, then set/create the attribute.
	  std::string s = std::to_string(scoreSystem->getCurrentScore());
	  char const *pchar = s.c_str();  //use char const* as target type
	  p_DataElement->setAttribute(XMLString::transcode("index"),XMLString::transcode(pchar));

	  p_RootElement->appendChild(p_DataElement);



	  // Create an Element node, then fill in some attributes, add some text,
	  // then append this to the 'pDataElement' element.
	  DOMElement * p_Row = NULL;
	  p_Row = p_DOMDocument->createElement(XMLString::transcode("name"));

	  DOMText* p_TextNode = NULL;


	  name.erase( std::remove(name.begin(), name.end(), '\r'), name.end() );
	  const char* chr = strdup(name.c_str());
	  p_TextNode = p_DOMDocument->createTextNode(XMLString::transcode(chr));
	  p_Row->appendChild(p_TextNode);

	  p_DataElement->appendChild(p_Row);


	  // Create an Element node, then fill in some attributes, add some text,
	  	  // then append this to the 'pDataElement' element.
	  	  DOMElement * p_Row2 = NULL;
	  	  p_Row2 = p_DOMDocument->createElement(XMLString::transcode("value"));

	  	  DOMText* p_TextNode2 = NULL;
	  	  p_TextNode2 = p_DOMDocument->createTextNode(XMLString::transcode(pchar));
	  	  p_Row2->appendChild(p_TextNode2);

	  	  p_DataElement->appendChild(p_Row2);

	  //Output to a file
	  DoOutput2File(p_DOMDocument, XMLString::transcode("./data/score.xml"));
}



void DoOutput2File(DOMDocument* pmyDOMDocument, XMLCh*  FullFilePath )
{

  DOMImplementation    *pImplement     = NULL;
  DOMLSSerializer      *pSerializer    = NULL;
  XMLFormatTarget      *pTarget        = NULL;

  /*
  Return the first registered implementation that
  has the desired features. In this case, we are after
  a DOM implementation that has the LS feature... or Load/Save.
  */
  pImplement = DOMImplementationRegistry::getDOMImplementation(XMLString::transcode("LS"));

  /*
  From the DOMImplementation, create a DOMWriter.
  DOMWriters are used to serialize a DOM tree [back] into an XML document.
  */
  pSerializer = ((DOMImplementationLS*)pImplement)->createLSSerializer();


  /*
  This line is optional. It just sets a feature
  of the Serializer to make the output
  more human-readable by inserting line-feeds,
  without actually inserting any new elements/nodes
  into the DOM tree. (There are many different features to set.)
  Comment it out and see the difference.
  */
  DOMConfiguration* pDomConfiguration = pSerializer->getDomConfig();
  pDomConfiguration->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);


  /*
  Choose a location for the serialized output. The 3 options are:
      1) StdOutFormatTarget     (std output stream -  good for debugging)
      2) MemBufFormatTarget     (to Memory)
      3) LocalFileFormatTarget  (save to file)
      (Note: You'll need a different header file for each one)
      Don't forget to escape file-paths with a backslash character, or
      just specify a file to be saved in the exe directory.

        eg. c:\\example\\subfolder\\pfile.xml

  */
  pTarget = new LocalFileFormatTarget(FullFilePath);
  // Write the serialized output to the target.
  DOMLSOutput* pDomLsOutput = ((DOMImplementationLS*)pImplement)->createLSOutput();
  pDomLsOutput->setByteStream(pTarget);

  pSerializer->write(pmyDOMDocument, pDomLsOutput);

}
