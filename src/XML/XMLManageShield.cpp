#include "XMLManageShield.h"
XMLManageShield::XMLManageShield ():XMLManager(){
	SHIELD_TAG = "shield";
	INDEX_ATTRIBUTE = "index";
	NAME_TAG = "name";
	MESH_TAG = "mesh";
	LIFE_TAG = "life";
	SIZE_TAG = "size";
	IMPACT_FX_TAG = "impact_fx";
	DESETROY_FX_TAG = "destroy_fx";
}

XMLManageShield::~XMLManageShield (){
	/*delete SHIELD_TAG = "shield";
	delete INDEX_ATTRIBUTE = "index";
	delete NAME_TAG = "name";
	delete MESH_TAG = "mesh";
	delete LIFE_TAG = "life";
	delete SIZE_TAG = "size";
	delete IMPACT_FX_TAG = "impact_fx";
	delete DESETROY_FX_TAG = "destroy_fx";*/
}
void XMLManageShield::loadXML(Configuration* configuration) {
	// Init parser with shield.xml
	initParser(configuration->getShieldConfig()->_config_shield_path);
	// Convert string tags in int
	XMLCh* shieldTag = XMLString::transcode(SHIELD_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* nameTag = XMLString::transcode(NAME_TAG);
	XMLCh* meshTag = XMLString::transcode(MESH_TAG);
	XMLCh* lifeTag = XMLString::transcode(LIFE_TAG);
	XMLCh* sizeTag = XMLString::transcode(SIZE_TAG);
	XMLCh* impactFxTag = XMLString::transcode(IMPACT_FX_TAG);
	XMLCh* destroyFxTag = XMLString::transcode(DESETROY_FX_TAG);
	// Proccess root node <shield>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* shieldNode = _elementRoot->getChildNodes()->item(i);
		if (shieldNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shieldNode->getNodeName(), shieldTag)) {
			// If <shield>??
			string index = XMLManager::getAttributeValue(shieldNode,indexAttribute);
			char* name = NULL;
			char* mesh = NULL;
			float life = 0;
			float size = 0;
			char* impact_fx = NULL;
			char* destroy_fx = NULL;
			// for each children <shield>
			for (XMLSize_t i = 0; i < shieldNode->getChildNodes()->getLength(); ++i ) {
				// Children
				DOMNode* shieldChildrenNode = shieldNode->getChildNodes()->item(i);
				// name
				if (shieldChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shieldChildrenNode->getNodeName(), nameTag)) {
					name = XMLManager::getNodeValue(shieldChildrenNode);
				// mesh
				} else if(shieldChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shieldChildrenNode->getNodeName(), meshTag)){
					mesh = XMLManager::getNodeValue(shieldChildrenNode);
				// life
				}else if(shieldChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shieldChildrenNode->getNodeName(), lifeTag)){
					life = atof(XMLManager::getNodeValue(shieldChildrenNode));
				//size
				}else if(shieldChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shieldChildrenNode->getNodeName(), sizeTag)){
					size = atof(XMLManager::getNodeValue(shieldChildrenNode));
				//impact_fx
				}else if(shieldChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shieldChildrenNode->getNodeName(), impactFxTag)){
					impact_fx = XMLManager::getNodeValue(shieldChildrenNode);
				//destroy_fx
				}else if(shieldChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shieldChildrenNode->getNodeName(), destroyFxTag)){
					destroy_fx = XMLManager::getNodeValue(shieldChildrenNode);
				}


			}

			// Prepare shield
			Configuration::Shield_t::shields shield;
			shield._name = name;
			shield._mesh = mesh;
			shield._life = life;
			shield._size = size;
			shield._impact_fx = impact_fx;
			shield._destroy_fx = destroy_fx;

			configuration->addShield(index,shield);
		}

	}

	// Free resources
	XMLString::release(&shieldTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&nameTag);
	XMLString::release(&meshTag);
	XMLString::release(&lifeTag);
	XMLString::release(&sizeTag);
	XMLString::release(&impactFxTag);
	XMLString::release(&destroyFxTag);
}


void XMLManageShield::saveXML(Configuration* configuration){

}
