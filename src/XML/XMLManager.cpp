#include "XMLManager.h"
#include <stdexcept>

XMLManager::XMLManager (){
}

XMLManager::~XMLManager (){

	_xmlDoc->release();
	XMLPlatformUtils::Terminate();
}

void XMLManager::initParser(char* xmlPath) {

	// Inicialización.
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Error durante la inicialización! :\n"
		<< message << "\n";
		XMLString::release(&message);
		return;
	}

	_parser = new XercesDOMParser;

	_parser->setValidationScheme(XercesDOMParser::Val_Always);

	// 'Parseando' el fichero xml...
	try {
		_parser->parse(xmlPath);
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Excepción capturada: \n"
		<< message << "\n";
		XMLString::release(&message);
	}
	catch (const DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		cout << "Excepción capturada: \n"
		<< message << "\n";
		XMLString::release(&message);
	}
	catch (...) {
		cout << "Excepción no esperada.\n" ;
		return;
	}

	try {
		// Obtener el elemento raíz del documento.
		// Nodo <config>


		_xmlDoc = _parser->getDocument();
		_elementRoot = _xmlDoc->getDocumentElement();

		if(!_elementRoot){
			throw(std::runtime_error("Documento XML vacío."));
		}

	}
	catch (xercesc::XMLException& e ) {
		char* message = xercesc::XMLString::transcode( e.getMessage() );
		ostringstream errBuf;
		errBuf << "Error 'parseando': " << message << flush;
		XMLString::release( &message );
		return;
	}
}

char* XMLManager::getAttributeValue (DOMNode* node, const XMLCh* attributeTag) {
	// Atributos de la cámara.
	DOMNamedNodeMap* attributes = node->getAttributes();
	DOMNode* attributeNameNode = attributes->getNamedItem(attributeTag);
	char* attributeValue = NULL;
	attributeValue = XMLString::transcode(attributeNameNode->getNodeValue());
	return attributeValue;
}

char* XMLManager::getNodeValue (DOMNode* node) {
	char* nodeValue = NULL;
	nodeValue = XMLString::transcode(node->getFirstChild()->getNodeValue());
	return nodeValue;
}
