#include "XMLManageSound.h"
XMLManageSound::XMLManageSound ():XMLManager(){
	TRACK_TAG = "track";
	FX_TAG = "fx";
	INDEX_ATTRIBUTE = "index";
	VOLUME_ATTRIBUTE = "volume";
}
XMLManageSound::~XMLManageSound (){
	/*delete TRACK_TAG;
	delete FX_TAG;
	delete INDEX_ATTRIBUTE;
	delete VOLUME_ATTRIBUTE*/;
}
void XMLManageSound::loadXML(Configuration* configuration) {
	loadMusic(configuration);
	loadFx(configuration);
}
void XMLManageSound::loadMusic(Configuration* configuration) {
	// Init parser with music.xml
	initParser(configuration->getMusicConfig()->_config_music_path);
	// Convert string tags in int
	XMLCh* trackTag = XMLString::transcode(TRACK_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* volumeAttribute = XMLString::transcode(VOLUME_ATTRIBUTE);
	// Proccess root node <tracks>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* trackNode = _elementRoot->getChildNodes()->item(i);
		// If <track>??
		if (trackNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(trackNode->getNodeName(), trackTag)) {
			string index = XMLManager::getAttributeValue(trackNode,indexAttribute);
			float volume = atof(XMLManager::getAttributeValue(trackNode,volumeAttribute));
			char* value = XMLManager::getNodeValue(trackNode);
			configuration->addMusic(index,value,volume);
		}
	}
	// Free resources
	XMLString::release(&trackTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&volumeAttribute);
}

void XMLManageSound::loadFx(Configuration* configuration) {
	// Init parser with fx.xml
	initParser(configuration->getFxConfig()->_config_fx_path);
	// Convert string tags in int
	XMLCh* fxTag = XMLString::transcode(FX_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* volumeAttribute = XMLString::transcode(VOLUME_ATTRIBUTE);
	// Proccess root node <fxs>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* fxNode = _elementRoot->getChildNodes()->item(i);
		// If <fx>??
		if (fxNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(fxNode->getNodeName(), fxTag)) {
			string index = XMLManager::getAttributeValue(fxNode,indexAttribute);
			float volume = atof(XMLManager::getAttributeValue(fxNode,volumeAttribute));
			char* value = XMLManager::getNodeValue(fxNode);
			configuration->addFx(index,value,volume);
		}
	}
	// Liberar recursos.
	XMLString::release(&fxTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&volumeAttribute);
}

void XMLManageSound::saveXML(Configuration* configuration){

}
