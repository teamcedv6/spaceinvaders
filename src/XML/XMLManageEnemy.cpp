#include "XMLManageEnemy.h"

XMLManageEnemy::XMLManageEnemy ():XMLManager(){
	ENEMY_TAG = "enemy";
	INDEX_ATTRIBUTE = "index";
	NAME_TAG = "name";
	MESH_TAG = "mesh";
	LIFE_TAG = "life";
	SIZE_TAG = "size";
	HEIGHT_TAG = "height";
	LENGTH_TAG = "length";
	POINTS_TAG = "points";
	IMPACT_FX_TAG = "impact_fx";
	DESTROY_FX_TAG = "destroy_fx";
	SHOOT_TAG = "shoot";
	SHIELD_TAG = "shield";
}

XMLManageEnemy::~XMLManageEnemy (){

	/*delete ENEMY_TAG;
	delete INDEX_ATTRIBUTE;
	delete NAME_TAG;
	delete MESH_TAG;
	delete LIFE_TAG;
	delete SIZE_TAG;
	delete HEIGHT_TAG;
	delete LENGTH_TAG;
	delete POINTS_TAG;
	delete IMPACT_FX_TAG;
	delete DESTROY_FX_TAG;
	delete SHOOT_TAG;
	delete SHIELD_TAG;*/

}
void XMLManageEnemy::loadXML(Configuration* configuration) {

}
void XMLManageEnemy::loadXML(Configuration* configuration,SoundManager* soundManager) {
	// Init parser with enemy.xml
	initParser(configuration->getEnemyConfig()->_config_enemy_path);

	// Convert string tags in int
	XMLCh* enemyTag = XMLString::transcode(ENEMY_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* nameTag = XMLString::transcode(NAME_TAG);
	XMLCh* meshTag = XMLString::transcode(MESH_TAG);
	XMLCh* lifeTag = XMLString::transcode(LIFE_TAG);
	XMLCh* sizeTag = XMLString::transcode(SIZE_TAG);
	XMLCh* heightTag = XMLString::transcode(HEIGHT_TAG);
	XMLCh* lengthTag = XMLString::transcode(LENGTH_TAG);
	XMLCh* pointsTag = XMLString::transcode(POINTS_TAG);
	XMLCh* impactFxTag = XMLString::transcode(IMPACT_FX_TAG);
	XMLCh* destroyFxTag = XMLString::transcode(DESTROY_FX_TAG);
	XMLCh* shootTag = XMLString::transcode(SHOOT_TAG);
	XMLCh* shieldTag = XMLString::transcode(SHIELD_TAG);

	// Proccess root node <enemies>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* enemyNode = _elementRoot->getChildNodes()->item(i);
		// If <enemy>??
		if (enemyNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyNode->getNodeName(), enemyTag)) {
			// Prepare enemy
			string index = XMLManager::getAttributeValue(enemyNode,indexAttribute);
			char* name 			= NULL;
			char* mesh 			= NULL;
			float life 			= 0;
			float size 			= 0;
			float height 		= 0;
			float length 		= 0;
			float points 		= 0;
			char* impact_fx 	= NULL;
			char* destroy_fx 	= NULL;
			char* shoot 		= NULL;
			char* shield 		= NULL;



			// Proccess root node <enemy>
			for (XMLSize_t i = 0; i < enemyNode->getChildNodes()->getLength(); ++i ) {
				DOMNode* enemyChildrenNode = enemyNode->getChildNodes()->item(i);
				//
				if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), nameTag)) {
					name = XMLManager::getNodeValue(enemyChildrenNode);
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), meshTag)) {
					mesh = XMLManager::getNodeValue(enemyChildrenNode);
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), lifeTag)) {
					life = atof(XMLManager::getNodeValue(enemyChildrenNode));
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), sizeTag)) {
					size = atof(XMLManager::getNodeValue(enemyChildrenNode));
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), heightTag)) {
					height = atof(XMLManager::getNodeValue(enemyChildrenNode));
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), lengthTag)) {
					length = atof(XMLManager::getNodeValue(enemyChildrenNode));
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), pointsTag)) {
					points = atof(XMLManager::getNodeValue(enemyChildrenNode));
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), impactFxTag)) {
					impact_fx = XMLManager::getNodeValue(enemyChildrenNode);
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), destroyFxTag)) {
					destroy_fx = XMLManager::getNodeValue(enemyChildrenNode);
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), shootTag)) {
					shoot = XMLManager::getNodeValue(enemyChildrenNode);
				}else if (enemyChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyChildrenNode->getNodeName(), shieldTag)) {
					shield = XMLManager::getNodeValue(enemyChildrenNode);
				}
			}

			Configuration::Enemy_t::enemies enemy;
			enemy._name = name;
			enemy._mesh = mesh;
			enemy._life = life;
			enemy._size = size;
			enemy._height = height;
			enemy._length = length;
			enemy._points = points;
			enemy._destroy_fx = impact_fx;
			enemy._impact_fx = destroy_fx;
			enemy._shoot = shoot;
			enemy._shield = shield;

			soundManager->setSelectFx(enemy._name,enemy._impact_fx,enemy._destroy_fx,enemy._shoot);

			configuration->addEnemy(index,enemy);
		}
	}
	// Free resources
	XMLString::release(&enemyTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&nameTag);
	XMLString::release(&meshTag);
	XMLString::release(&lifeTag);
	XMLString::release(&sizeTag);
	XMLString::release(&heightTag);
	XMLString::release(&lengthTag);
	XMLString::release(&pointsTag);
	XMLString::release(&impactFxTag);
	XMLString::release(&destroyFxTag);
	XMLString::release(&shootTag);
	XMLString::release(&shieldTag);
}


void XMLManageEnemy::saveXML(Configuration* configuration){

}
