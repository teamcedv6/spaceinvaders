#include "XMLManageConfig.h"

XMLManageConfig::XMLManageConfig():XMLManager(){
	XML_PATH  = "./data/config.xml";
	XML_PATHS_TAG  = "xml_paths";
	PATH_TAG  = "path";
	TYPE_ATTRIBUTE  = "type";
	MUSIC_TAG  = "Music";
	FX_TAG  = "Fx";
	LEVEL_TAG  = "Level";
	PLANET_TAG  = "Planet";
	PLAYER_TAG = "Player";
	ENEMY_TAG  = "Enemy";
	SHOOT_TAG = "Shoot";
	SHIELD_TAG = "Shield";
	//MOVEMENT_TAG = "Movement";
	OPTIONS_TAG = "options";
	UI_TAG = "ui";
	FX_ATTRIBUTE = "fx";
	MUSIC_ATTRIBUTE = "music";
	LOAD_TAG = "load";
	SCORE_TAG = "Score";
}

XMLManageConfig::~XMLManageConfig (){
	/*delete XML_PATH;
	delete XML_PATHS_TAG;
	delete PATH_TAG;
	delete TYPE_ATTRIBUTE;
	delete MUSIC_TAG;
	delete FX_TAG;
	delete LEVEL_TAG;
	delete PLANET_TAG;
	delete PLAYER_TAG;
	delete ENEMY_TAG;
	delete SHOOT_TAG;
	delete SHIELD_TAG;
	delete MOVEMENT_TAG;
	delete OPTIONS_TAG;
	delete UI_TAG;
	delete FX_ATTRIBUTE;
	delete MUSIC_ATTRIBUTE;
	delete LOAD_TAG;
	delete SCORE_TAG;*/
}

void XMLManageConfig::loadXML(Configuration* configuration){

	// Init parser with level.xml
	initParser(XML_PATH);

	// Convert string tags in int
	XMLCh* xmlpathsTag = XMLString::transcode(XML_PATHS_TAG);
	XMLCh* pathTag = XMLString::transcode(PATH_TAG);
	XMLCh* typeAttribute = XMLString::transcode(TYPE_ATTRIBUTE);
	XMLCh* musicTag = XMLString::transcode(MUSIC_TAG);
	XMLCh* fxTag = XMLString::transcode(FX_TAG);
	XMLCh* levelTag = XMLString::transcode(LEVEL_TAG);
	XMLCh* planetTag = XMLString::transcode(PLANET_TAG);
	XMLCh* playerTag = XMLString::transcode(PLAYER_TAG);
	XMLCh* enemyTag = XMLString::transcode(ENEMY_TAG);
	XMLCh* shootTag = XMLString::transcode(SHOOT_TAG);
	XMLCh* shieldTag = XMLString::transcode(SHIELD_TAG);
	//XMLCh* movementTag = XMLString::transcode(MOVEMENT_TAG);
	XMLCh* optionsTag = XMLString::transcode(OPTIONS_TAG);
	XMLCh* uiTag = XMLString::transcode(UI_TAG);
	XMLCh* fxAttribute = XMLString::transcode(FX_ATTRIBUTE);
	XMLCh* musicAttribute = XMLString::transcode(MUSIC_ATTRIBUTE);
	XMLCh* loadTag = XMLString::transcode(LOAD_TAG);
	XMLCh* scoreTag = XMLString::transcode(SCORE_TAG);

	XMLCh* typeCompareTag = 0;

	// Proccess root node <xml_paths>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* configChildrenNodes = _elementRoot->getChildNodes()->item(i);
		// If <xml_path>??
		if (configChildrenNodes->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(configChildrenNodes->getNodeName(), xmlpathsTag)) {
			// for each children <xml_path>
			for (XMLSize_t i = 0; i < configChildrenNodes->getChildNodes()->getLength(); ++i ) {
				// Children
				DOMNode* pathNode = configChildrenNodes->getChildNodes()->item(i);
				if (pathNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(pathNode->getNodeName(), pathTag)) {
					// Get type
					char* typePath = XMLManager::getAttributeValue(pathNode,typeAttribute);
					typeCompareTag = XMLString::transcode(typePath);

					// Music
					if(XMLString::equals(typeCompareTag, musicTag)){
						configuration->setConfigMusicPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigMusicType(typePath);
					// Fx
					}else if(XMLString::equals(typeCompareTag, fxTag)){
						configuration->setConfigFxPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigFxType(typePath);
					// Level
					}else if(XMLString::equals(typeCompareTag, levelTag)){
						configuration->setConfigLevelPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigLevelType(typePath);
					// Planet
					}else if(XMLString::equals(typeCompareTag, planetTag)){
						configuration->setConfigPlanetPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigPlanetType(typePath);
					// Player
					}else if(XMLString::equals(typeCompareTag, playerTag)){
						configuration->setConfigPlayerPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigPlayerType(typePath);
					// Enemy
					}else if(XMLString::equals(typeCompareTag, enemyTag)){
						configuration->setConfigEnemyPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigEnemyType(typePath);
					// Shoot
					} else if(XMLString::equals(typeCompareTag, shootTag)){
						configuration->setConfigShootPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigShootType(typePath);
					// Shield
					} else if(XMLString::equals(typeCompareTag, shieldTag)){
						configuration->setConfigShieldPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigShieldType(typePath);
					// Score
					}else if(XMLString::equals(typeCompareTag, scoreTag)){
						configuration->setConfigScorePath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigScoreType(typePath);
					}/*else if(XMLString::equals(typeCompareTag, movementTag)){
						configuration->setConfigMovementPath(XMLManager::getNodeValue(pathNode));
						configuration->setConfigMovementType(typePath);
					}*/
				}
			}
		// If <options>??
		}else if(configChildrenNodes->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(configChildrenNodes->getNodeName(), optionsTag)) {
			configuration->setConfigOptionMusic(XMLManager::getAttributeValue(configChildrenNodes,musicAttribute));
			configuration->setConfigOptionFx(XMLManager::getAttributeValue(configChildrenNodes,fxAttribute));
		// If <ui>??
		}else if(configChildrenNodes->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(configChildrenNodes->getNodeName(), uiTag)) {
			configuration->setConfigUiMusic((string)XMLManager::getAttributeValue(configChildrenNodes,musicAttribute));
			configuration->setConfigUiFx((string)XMLManager::getAttributeValue(configChildrenNodes,fxAttribute));
			// If <load>??
		}else if(configChildrenNodes->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(configChildrenNodes->getNodeName(), loadTag)) {
			configuration->setConfigOptionLoadMusic(XMLManager::getAttributeValue(configChildrenNodes,musicAttribute));
			configuration->setConfigOptionLoadFx(XMLManager::getAttributeValue(configChildrenNodes,fxAttribute));
		}
	}

	// Free resources
	XMLString::release(&xmlpathsTag);
	XMLString::release(&pathTag);
	XMLString::release(&typeAttribute);
	XMLString::release(&musicTag);
	XMLString::release(&fxTag);
	XMLString::release(&levelTag);
	XMLString::release(&planetTag);
	XMLString::release(&playerTag);
	XMLString::release(&enemyTag);
	XMLString::release(&shootTag);
	XMLString::release(&shieldTag);
	//XMLString::release(&movementTag);
	XMLString::release(&optionsTag);
	XMLString::release(&uiTag);
	XMLString::release(&fxAttribute);
	XMLString::release(&musicAttribute);
	XMLString::release(&scoreTag);
}

void XMLManageConfig::saveXML(Configuration* configuration){

}

