/*#include "XMLManageMovement.h"
XMLManageMovement::XMLManageMovement ():XMLManager(){
	MOVEMENT_TAG = "movement";
	INDEX_ATTRIBUTE = "index";
	PARAMETERS_TAG = "parameters";
	SPEED_TAG = "speed";
}

XMLManageMovement::~XMLManageMovement (){
	delete MOVEMENT_TAG;
	delete INDEX_ATTRIBUTE;
	delete PARAMETERS_TAG;
	delete SPEED_TAG;
}
void XMLManageMovement::loadXML(Configuration* configuration) {
	// Init parser with shield.xml
	//initParser(configuration->getMovementConfig()->_config_movement_path);
	// Convert string tags in int
	XMLCh* movementTag = XMLString::transcode(MOVEMENT_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* parametersTag = XMLString::transcode(PARAMETERS_TAG);
	XMLCh* speedTag = XMLString::transcode(SPEED_TAG);
	// Proccess root node <movement>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* movementNode = _elementRoot->getChildNodes()->item(i);
		if (movementNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(movementNode->getNodeName(), movementTag)) {
			// If <movement>??
			string index = XMLManager::getAttributeValue(movementNode,indexAttribute);
			char* parameters = NULL;
			float speed = 0;
			// for each children <movement>
			for (XMLSize_t i = 0; i < movementNode->getChildNodes()->getLength(); ++i ) {
				// Children
				DOMNode* movementChildrenNode = movementNode->getChildNodes()->item(i);
				// name
				if (movementChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(movementChildrenNode->getNodeName(), parametersTag)) {
					parameters = XMLManager::getNodeValue(movementChildrenNode);
				// speed
				} else if(movementChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(movementChildrenNode->getNodeName(), speedTag)){
					speed = atof(XMLManager::getNodeValue(movementChildrenNode));
				}
			}

			// Prepare shield
			Configuration::Movement_t::movements movement;
			movement._parameters = parameters;
			movement._speed = speed;
			configuration->addMovement(index,movement);
		}

	}

	// Free resources
	XMLString::release(&movementTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&parametersTag);
	XMLString::release(&speedTag);
}


void XMLManageMovement::saveXML(Configuration* configuration){

}
*/
