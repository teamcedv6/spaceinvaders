#include "XMLManagePlayer.h"

XMLManagePlayer::XMLManagePlayer ():XMLManager(){
	PLAYER_TAG = "player";
	INDEX_ATTRIBUTE = "index";
	NAME_TAG = "name";
	MESH_TAG = "mesh";
	LIFE_TAG = "life";
	SHOOT_TAG = "shoot";
	SHIELD_TAG = "shield";
	SIZE_TAG = "size";
	HEIGHT_TAG = "height";
	LENGTH_TAG = "length";
	SPEED_TAG = "speed";
	IMPACT_FX_TAG = "impact_fx";
	DESTROY_FX_TAG = "destroy_fx";
}

XMLManagePlayer::~XMLManagePlayer (){
	/*delete PLAYER_TAG;
	delete INDEX_ATTRIBUTE;
	delete NAME_TAG;
	delete MESH_TAG;
	delete LIFE_TAG;
	delete SHOOT_TAG;
	delete SHIELD_TAG;
	delete SIZE_TAG;
	delete HEIGHT_TAG;
	delete LENGTH_TAG;
	delete SPEED_TAG;
	delete IMPACT_FX_TAG;
	delete DESTROY_FX_TAG;*/
}
void XMLManagePlayer::loadXML(Configuration* configuration) {

}
void XMLManagePlayer::loadXML(Configuration* configuration, SoundManager* soundManager) {
	// Init parser with player.xml
	initParser(configuration->getPlayerConfig()->_config_player_path);
	// Convert string tags in int
	XMLCh* playerTag = XMLString::transcode(PLAYER_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* nameTag = XMLString::transcode(NAME_TAG);
	XMLCh* meshTag = XMLString::transcode(MESH_TAG);
	XMLCh* lifeTag = XMLString::transcode(LIFE_TAG);
	XMLCh* shootTag = XMLString::transcode(SHOOT_TAG);
	XMLCh* shieldTag = XMLString::transcode(SHIELD_TAG);
	XMLCh* sizeTag = XMLString::transcode(SIZE_TAG);
	XMLCh* heightTag = XMLString::transcode(HEIGHT_TAG);
	XMLCh* lengthTag = XMLString::transcode(LENGTH_TAG);
	XMLCh* speedTag = XMLString::transcode(SPEED_TAG);
	XMLCh* impactFxTag = XMLString::transcode(IMPACT_FX_TAG);
	XMLCh* destroyFxTag = XMLString::transcode(DESTROY_FX_TAG);
	// Proccess root node <enemies>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* playerNode = _elementRoot->getChildNodes()->item(i);
		// If <player>??
		if (playerNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerNode->getNodeName(), playerTag)) {
			// Prepare player
			string index = XMLManager::getAttributeValue(playerNode,indexAttribute);
			char* name = NULL;
			char* mesh = NULL;
			float life = 0;
			char* shoot = NULL;
			char* shield = NULL;
			float size = 0;
			float height 		= 0;
			float length 		= 0;
			float speed = 0;
			char* impact_fx = NULL;
			char* destroy_fx = NULL;
			// Proccess root node <player>
			for (XMLSize_t i = 0; i < playerNode->getChildNodes()->getLength(); ++i ) {
				DOMNode* playerChildrenNode = playerNode->getChildNodes()->item(i);
				// name
				if (playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), nameTag)) {
					name = XMLManager::getNodeValue(playerChildrenNode);
				// mesh
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), meshTag)){
					mesh = XMLManager::getNodeValue(playerChildrenNode);
				// life
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), lifeTag)){
					life = atof(XMLManager::getNodeValue(playerChildrenNode));
				// shoot
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), shootTag)){
					shoot = XMLManager::getNodeValue(playerChildrenNode);
				//shield
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), shieldTag)){
					shield = XMLManager::getNodeValue(playerChildrenNode);
				// size
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), sizeTag)){
					size = atof(XMLManager::getNodeValue(playerChildrenNode));
				// speed
				}else if (playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), heightTag)) {
					height = atof(XMLManager::getNodeValue(playerChildrenNode));
				}else if (playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), lengthTag)) {
					length = atof(XMLManager::getNodeValue(playerChildrenNode));
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), speedTag)){
					speed = atof(XMLManager::getNodeValue(playerChildrenNode));
				// impact_fx
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), impactFxTag)){
					impact_fx = XMLManager::getNodeValue(playerChildrenNode);
				// destroy_fx
				}else if(playerChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(playerChildrenNode->getNodeName(), destroyFxTag)){
					destroy_fx = XMLManager::getNodeValue(playerChildrenNode);
				}
			}

			Configuration::Player_t::players player;
			player._name = name;
			player._mesh = mesh;
			player._life = life;
			player._shoot = shoot;
			player._shield = shield;
			player._size = size;
			player._height = height;
			player._length = length;
			player._speed = speed;
			player._impact_fx = impact_fx;
			player._destroy_fx = destroy_fx;

			soundManager->setSelectFx(player._name,player._impact_fx,player._destroy_fx,player._shoot);

			configuration->addPlayer(index,player);

		}
	}
	// Free resources
	XMLString::release(&playerTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&nameTag);
	XMLString::release(&meshTag);
	XMLString::release(&lifeTag);
	XMLString::release(&shootTag);
	XMLString::release(&shieldTag);
	XMLString::release(&sizeTag);
	XMLString::release(&heightTag);
	XMLString::release(&lengthTag);
	XMLString::release(&speedTag);
	XMLString::release(&impactFxTag);
	XMLString::release(&destroyFxTag);
}


void XMLManagePlayer::saveXML(Configuration* configuration){

}
