#include "XMLManagePlanet.h"
XMLManagePlanet::XMLManagePlanet ():XMLManager(){
	PLANET_TAG = "planet";
	INDEX_ATTRIBUTE = "index";
	NAME_TAG = "name";
	MESH_TAG = "mesh";
	SPEED_TAG = "speed";
	SIZE_TAG = "size";
	RADIO_TAG = "radio";
}

XMLManagePlanet::~XMLManagePlanet (){
	/*delete PLANET_TAG;
	delete INDEX_ATTRIBUTE;
	delete NAME_TAG;
	delete MESH_TAG;
	delete SPEED_TAG;
	delete SIZE_TAG;
	delete RADIO_TAG;*/
}
void XMLManagePlanet::loadXML(Configuration* configuration) {
	// Init parser with level.xml
	initParser(configuration->getPlanetConfig()->_config_planet_path);
	// Convert string tags in int
	XMLCh* planetTag = XMLString::transcode(PLANET_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* nameTag = XMLString::transcode(NAME_TAG);
	XMLCh* meshTag = XMLString::transcode(MESH_TAG);
	XMLCh* speedTag = XMLString::transcode(SPEED_TAG);
	XMLCh* sizeTag = XMLString::transcode(SIZE_TAG);
	XMLCh* radioTag = XMLString::transcode(RADIO_TAG);
	// Proccess root node <planets>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* planetNode = _elementRoot->getChildNodes()->item(i);
		if (planetNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(planetNode->getNodeName(), planetTag)) {
			// If <planet>??
			string index = XMLManager::getAttributeValue(planetNode,indexAttribute);
			char* name = NULL;
			char* mesh = NULL;
			float speed = 0;
			float size = 0;
			float radio = 0;
			// for each children <planet>
			for (XMLSize_t i = 0; i < planetNode->getChildNodes()->getLength(); ++i ) {
				// Children
				DOMNode* planetChildrenNode = planetNode->getChildNodes()->item(i);
				// name
				if (planetChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(planetChildrenNode->getNodeName(), nameTag)) {
					name = XMLManager::getNodeValue(planetChildrenNode);
				// mesh
				}else if(planetChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(planetChildrenNode->getNodeName(), meshTag)){
					mesh = XMLManager::getNodeValue(planetChildrenNode);
				// speed
				} else if(planetChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(planetChildrenNode->getNodeName(), speedTag)){
					speed = atof(XMLManager::getNodeValue(planetChildrenNode));
				// size
				}else if(planetChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(planetChildrenNode->getNodeName(), sizeTag)){
					size = atof(XMLManager::getNodeValue(planetChildrenNode));
				// radio
				}else if(planetChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(planetChildrenNode->getNodeName(), radioTag)){
					radio = atof(XMLManager::getNodeValue(planetChildrenNode));
				}
			}

			// Prepare planet
			Configuration::Planet_t::planets planet;
			planet._name = name;
			planet._mesh = mesh;
			planet._speed = speed;
			planet._size = size;
			planet._radio = radio;

			configuration->addPlanet(index,planet);
		}

	}

	// Free resources
	XMLString::release(&planetTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&nameTag);
	XMLString::release(&meshTag);
	XMLString::release(&speedTag);
	XMLString::release(&sizeTag);
	XMLString::release(&radioTag);

}


void XMLManagePlanet::saveXML(Configuration* configuration){

}
