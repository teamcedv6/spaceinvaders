#include "XMLManageLevel.h"

XMLManageLevel::XMLManageLevel ():XMLManager(){
	INIT_TAG = "init";
	LEVEL_TAG = "level";
	MUSIC_TAG = "music";
	PLANET_TAG = "planet";
	WAVES_TAG = "waves";
	WAVE_TAG = "wave";
	ENEMY_TAG = "enemy";
	INDEX_ATTRIBUTE = "index";
	LAST_ATTRIBUTE = "last";
	MOVE_ATTRIBUTE = "move";
	SPLIT_ATTRIBUTE = "split";
	NUMBER_ATTRIBUTE = "number";
	RADIO_ATTRIBUTE = "radio";
	SPEED_ATTRIBUTE = "speed";
	ANGLE_ATTRIBUTE = "angle";
	HEIGHT_ATTRIBUTE = "height";

}

XMLManageLevel::~XMLManageLevel (){

	/*delete INIT_TAG;
	delete LEVEL_TAG;
	delete MUSIC_TAG;
	delete PLANET_TAG;
	delete WAVES_TAG;
	delete WAVE_TAG;
	delete ENEMY_TAG;
	delete INDEX_ATTRIBUTE;
	delete LAST_ATTRIBUTE;
	delete MOVE_ATTRIBUTE;
	delete SPLIT_ATTRIBUTE;
	delete NUMBER_ATTRIBUTE;
	delete RADIO_ATTRIBUTE;
	delete SPEED_ATTRIBUTE;
	delete ANGLE_ATTRIBUTE;
	delete HEIGHT_ATTRIBUTE;*/
}

void XMLManageLevel::loadXML(Configuration* configuration) {

	// Init parser with level.xml
	initParser(configuration->getLevelConfig()->_config_level_path);
	// Convert string tags in int
	XMLCh* initTag = XMLString::transcode(INIT_TAG);
	XMLCh* levelTag = XMLString::transcode(LEVEL_TAG);
	XMLCh* musicTag = XMLString::transcode(MUSIC_TAG);
	XMLCh* planetTag = XMLString::transcode(PLANET_TAG);
	XMLCh* wavesTag = XMLString::transcode(WAVES_TAG);
	XMLCh* waveTag = XMLString::transcode(WAVE_TAG);
	XMLCh* enemyTag = XMLString::transcode(ENEMY_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* lastAttribute = XMLString::transcode(LAST_ATTRIBUTE);
	XMLCh* moveAttribute = XMLString::transcode(MOVE_ATTRIBUTE);
	XMLCh* splitAttribute = XMLString::transcode(SPLIT_ATTRIBUTE);
	XMLCh* numberAttribute = XMLString::transcode(NUMBER_ATTRIBUTE);
	XMLCh* radioAttribute = XMLString::transcode(RADIO_ATTRIBUTE);
	XMLCh* speedAttribute = XMLString::transcode(SPEED_ATTRIBUTE);
	XMLCh* angleAttribute = XMLString::transcode(ANGLE_ATTRIBUTE);
	XMLCh* heightAttribute = XMLString::transcode(HEIGHT_ATTRIBUTE);

	// Proccess root node <levels>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* levelNode = _elementRoot->getChildNodes()->item(i);
		// If <level>??
		if (levelNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(levelNode->getNodeName(), levelTag)) {
			// Get level index
			int levelIndex = atoi(XMLManager::getAttributeValue(levelNode,indexAttribute));
			int last = atoi(XMLManager::getAttributeValue(levelNode,lastAttribute));
			float radio = atof(XMLManager::getAttributeValue(levelNode,radioAttribute));
			Configuration::Level_t::levels_t level;
			level._last = last;
			level._radio = radio;
			// for each children <level>
			for (XMLSize_t i = 0; i < levelNode->getChildNodes()->getLength(); ++i ) {
				// children
				DOMNode* childrenLevelNode = levelNode->getChildNodes()->item(i);
				// Init music and planet
				char* music = NULL;
				char* planet = NULL;
				// If <music>??
				if (childrenLevelNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(childrenLevelNode->getNodeName(), musicTag)) {
					music = XMLManager::getNodeValue(childrenLevelNode);
					level._music = music;
				// If <planet>??
				} else if(childrenLevelNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(childrenLevelNode->getNodeName(), planetTag)){
					planet = XMLManager::getNodeValue(childrenLevelNode);
					level._planet = planet;
				// If <waves>??
				}else if(childrenLevelNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(childrenLevelNode->getNodeName(), wavesTag)){
					// for each children <waves>
					for (XMLSize_t i = 0; i < childrenLevelNode->getChildNodes()->getLength(); ++i ) {
						// Get children nodes <waves>
						DOMNode* waveNode = childrenLevelNode->getChildNodes()->item(i);
						// If <wave>
						if (waveNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(waveNode->getNodeName(), waveTag)) {
							// Get index
							int waveIndex = atoi(XMLManager::getAttributeValue(waveNode,indexAttribute));
							int split = atoi(XMLManager::getAttributeValue(waveNode,splitAttribute));
							float speed = atof(XMLManager::getAttributeValue(waveNode,speedAttribute));
							char* move = XMLManager::getAttributeValue(waveNode,moveAttribute);
							float angle = atof(XMLManager::getAttributeValue(waveNode,angleAttribute));
							float height = atof(XMLManager::getAttributeValue(waveNode,heightAttribute));
							Configuration::Level_t::levels_t::waves_t wave;
							wave._move = move;
							wave._split = split;
							wave._speed = speed;
							wave._angle = angle;
							wave._height = height;
							// Wave children
							for (XMLSize_t i = 0; i < waveNode->getChildNodes()->getLength(); ++i ) {
								int number = 0;
								char* type = NULL;

								// If <enemy>
								DOMNode* enemyNode = waveNode->getChildNodes()->item(i);
								if (enemyNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(enemyNode->getNodeName(), enemyTag)) {
									number = atoi(XMLManager::getAttributeValue(enemyNode,numberAttribute));
									type = XMLManager::getNodeValue(enemyNode);
									Configuration::Level_t::levels_t::waves_t::enemies_t enemy;
									enemy._enemy = type;
									enemy._number = number;
									(wave._enemies)[type] = enemy;
								}
							}
						(level._waves)[waveIndex] = wave;
						}
					}
				}
			}
		configuration->addLevel(levelIndex,level);
		} else if(levelNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(levelNode->getNodeName(), initTag)){
			configuration->setConfigLevelInit(atoi(XMLManager::getNodeValue(levelNode)));
		}
	}

	// Free resources
	XMLString::release(&initTag);
	XMLString::release(&levelTag);
	XMLString::release(&musicTag);
	XMLString::release(&planetTag);
	XMLString::release(&wavesTag);
	XMLString::release(&waveTag);
	XMLString::release(&enemyTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&lastAttribute);
	XMLString::release(&moveAttribute);
	XMLString::release(&splitAttribute);
	XMLString::release(&numberAttribute);
	XMLString::release(&radioAttribute);
	XMLString::release(&speedAttribute);
	XMLString::release(&angleAttribute);
	XMLString::release(&heightAttribute);
}


void XMLManageLevel::saveXML(Configuration* configuration){

}
