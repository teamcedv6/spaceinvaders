#include "XMLManageShoot.h"

XMLManageShoot::XMLManageShoot ():XMLManager(){
	SHOOT_TAG = "shoot";
	INDEX_ATTRIBUTE = "index";
	NAME_TAG = "name";
	MESH_TAG = "mesh";
	SIZE_TAG = "size";
	MOVE_TAG = "move";
	IMPACT_FX_TAG = "impact_fx";
	DESTROY_FX_TAG = "destroy_fx";
	SPEED_TAG = "speed";
}

XMLManageShoot::~XMLManageShoot (){
	/*delete SHOOT_TAG;
	delete INDEX_ATTRIBUTE;
	delete NAME_TAG;
	delete MESH_TAG;
	delete SIZE_TAG;
	delete MOVE_TAG;
	delete IMPACT_FX_TAG;
	delete DESTROY_FX_TAG;
	delete SPEED_TAG;*/
}
void XMLManageShoot::loadXML(Configuration* configuration) {

}
void XMLManageShoot::loadXML(Configuration* configuration,SoundManager* soundManager) {
	// Init parser with shoot.xml
	initParser(configuration->getShootConfig()->_config_shoot_path);
	// Convert string tags in int
	XMLCh* shootTag = XMLString::transcode(SHOOT_TAG);
	XMLCh* indexAttribute = XMLString::transcode(INDEX_ATTRIBUTE);
	XMLCh* nameTag = XMLString::transcode(NAME_TAG);
	XMLCh* meshTag = XMLString::transcode(MESH_TAG);
	XMLCh* sizeTag = XMLString::transcode(SIZE_TAG);
	XMLCh* moveTag = XMLString::transcode(MOVE_TAG);
	XMLCh* impactFxTag = XMLString::transcode(IMPACT_FX_TAG);
	XMLCh* destroyFxTag = XMLString::transcode(DESTROY_FX_TAG);
	XMLCh* speedTag = XMLString::transcode(SPEED_TAG);
	// Proccess root node <shoot>
	for (XMLSize_t i = 0; i < _elementRoot->getChildNodes()->getLength(); ++i ) {
		// Get children
		DOMNode* shootNode = _elementRoot->getChildNodes()->item(i);
		// If <shoot>??
		if (shootNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootNode->getNodeName(), shootTag)) {
			// Prepare shoot
			string index = XMLManager::getAttributeValue(shootNode,indexAttribute);
			char* name = NULL;
			char* mesh = NULL;
			float size = 0;
			char* move = NULL;
			char* impact_fx = NULL;
			char* destroy_fx = NULL;
			float speed = 0;
			// Proccess root node <shoot>
			for (XMLSize_t i = 0; i < shootNode->getChildNodes()->getLength(); ++i ) {
				DOMNode* shootChildrenNode = shootNode->getChildNodes()->item(i);
				// Name
				if (shootChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootChildrenNode->getNodeName(), nameTag)) {
					name = XMLManager::getNodeValue(shootChildrenNode);
				// Life
				}else if(shootChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootChildrenNode->getNodeName(), meshTag)){
					mesh = XMLManager::getNodeValue(shootChildrenNode);
				// Mesh
				}else if(shootChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootChildrenNode->getNodeName(), sizeTag)){
					size = atof(XMLManager::getNodeValue(shootChildrenNode));
				// Fx
				}else if(shootChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootChildrenNode->getNodeName(), moveTag)){
					move = XMLManager::getNodeValue(shootChildrenNode);
				//Shoot
				}else if(shootChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootChildrenNode->getNodeName(), impactFxTag)){
					impact_fx = XMLManager::getNodeValue(shootChildrenNode);
				// Shield
				}else if(shootChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootChildrenNode->getNodeName(), destroyFxTag)){
					destroy_fx = XMLManager::getNodeValue(shootChildrenNode);
				// Speed
				}else if(shootChildrenNode->getNodeType() == DOMNode::ELEMENT_NODE && XMLString::equals(shootChildrenNode->getNodeName(), speedTag)){
					speed = atof(XMLManager::getNodeValue(shootChildrenNode));
				}
			}

			Configuration::Shoot_t::shoots shoot;
			shoot._name = name;
			shoot._mesh = mesh;
			shoot._size = size;
			shoot._move = move;
			shoot._impact_fx = impact_fx;
			shoot._destroy_fx = destroy_fx;
			shoot._speed = speed;

			//soundManager->setShoot(shoot._name,shoot._impact_fx);

			configuration->addShoot(index,shoot);
		}
	}
	// Free resources
	XMLString::release(&shootTag);
	XMLString::release(&indexAttribute);
	XMLString::release(&nameTag);
	XMLString::release(&meshTag);
	XMLString::release(&sizeTag);
	XMLString::release(&moveTag);
	XMLString::release(&impactFxTag);
	XMLString::release(&destroyFxTag);
	XMLString::release(&speedTag);
}


void XMLManageShoot::saveXML(Configuration* configuration){

}
