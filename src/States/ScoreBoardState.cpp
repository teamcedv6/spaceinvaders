/*
 * ScoreBoardState.cpp
 *
 *  Created on: 10 dic. 2016
 *      Author: joe
 */
#include "ScoreBoardState.h"

template<> ScoreBoardState* Ogre::Singleton<ScoreBoardState>::msSingleton = 0;

void ScoreBoardState::enter()
{
	GameManager * gameManager = GameManager::getSingletonPtr();
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");

	_menu= new MenuUI();

	int type = _type;
	switch ( type ) {
		// De MainState a LevelState
		case READ:
			_menu->createMenu(3);
			_menu->ShowScores();
		  break;
		// De LevelState a PlayState
		case WRITE:
			_menu->createMenu(4);
		  break;

		default:
		  std::cout << "selección erronea\n";
		  break;
	  }

	_exitGame = false;
}

void ScoreBoardState::exit()
{
	_menu->destroyMenu();
	delete _menu;
}

void ScoreBoardState::pause()
{
}

void ScoreBoardState::resume()
{
	_menu= new MenuUI();
		_menu->createMenu(3);
}

void ScoreBoardState::keyPressed(const OIS::KeyEvent &e)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
	  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
}

void ScoreBoardState::keyReleased(const OIS::KeyEvent &e)
{
	if (e.key == OIS::KC_ESCAPE)	{
			_exitGame=true;
	}
	CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(e.key));
}

void ScoreBoardState::mouseMoved(const OIS::MouseEvent &e)
{
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx, rely);
}

void ScoreBoardState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void ScoreBoardState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

bool ScoreBoardState::frameStarted (const Ogre::FrameEvent& e)
{

	_timeSinceLastFrame=e.timeSinceLastFrame;
	  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_timeSinceLastFrame);
	  return true;

	return true;
}

bool ScoreBoardState::frameEnded (const Ogre::FrameEvent & e)
{
	if (_exitGame)
		return false;

	return true;
}

ScoreBoardState& ScoreBoardState::getSingleton()
{
	assert(msSingleton);
	return *msSingleton;
}

ScoreBoardState* ScoreBoardState::getSingletonPtr()
{
	return msSingleton;
}

CEGUI::MouseButton ScoreBoardState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void ScoreBoardState::setType(ScoreType type) {
	_type = type;
}


