#include "ConfigurationState.h"

template<> ConfigurationState* Ogre::Singleton<ConfigurationState>::msSingleton = 0;

void
ConfigurationState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");

  _exitGame = false;
}

void
ConfigurationState::exit ()
{
}

void
ConfigurationState::pause ()
{
}

void
ConfigurationState::resume ()
{
}

bool
ConfigurationState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
ConfigurationState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
ConfigurationState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P) {
    popState();
  }
}

void
ConfigurationState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
ConfigurationState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
ConfigurationState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
ConfigurationState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

ConfigurationState*
ConfigurationState::getSingletonPtr ()
{
return msSingleton;
}

ConfigurationState&
ConfigurationState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
