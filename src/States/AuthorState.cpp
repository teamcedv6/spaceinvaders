/*
 * AuthorState.cpp
 *
 *  Created on: 10 dic. 2016
 *      Author: joe
 */
#include "AuthorState.h"

template<> AuthorState* Ogre::Singleton<AuthorState>::msSingleton = 0;

void AuthorState::enter()
{
	_root=Ogre::Root::getSingletonPtr();

	// Recuperamos SceneManager y Cámara de GameManager
	_sceneMgr =_root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	
	_menu= new MenuUI();
	_menu->createMenu(5);

	//auto music = gameManager->getConfiguration()->getUiConfig();
	//gameManager->getSoundManager()->selectResource("music",music->_music,"play");

	_exitGame=false;
}

void AuthorState::exit()
{
	_menu->destroyMenu();
	delete _menu;
}

void AuthorState::pause()
{
}

void AuthorState::resume()
{
}

void AuthorState::keyPressed(const OIS::KeyEvent &e)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
	CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);

}

void AuthorState::keyReleased(const OIS::KeyEvent &e)
{
	if (e.key == OIS::KC_ESCAPE)	{
		_exitGame=true;
	}
	CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(e.key));

}

void AuthorState::mouseMoved(const OIS::MouseEvent &e)
{
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx, rely);

}

void AuthorState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void AuthorState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

bool AuthorState::frameStarted (const Ogre::FrameEvent& evt)
{
	_timeSinceLastFrame=evt.timeSinceLastFrame;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_timeSinceLastFrame);
	return true;
}

bool AuthorState::frameEnded (const Ogre::FrameEvent & e)
{
	if (_exitGame)
		return false;

	return true;
}

AuthorState& AuthorState::getSingleton()
{
	assert(msSingleton);
	return *msSingleton;
}

AuthorState* AuthorState::getSingletonPtr()
{
	return msSingleton;
}
CEGUI::MouseButton AuthorState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}




