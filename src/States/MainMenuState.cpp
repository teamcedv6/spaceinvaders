#include <States/LoadState.h>
#include "MainMenuState.h"

template<> MainMenuState* Ogre::Singleton<MainMenuState>::msSingleton = 0;

void test(const boost::system::error_code& e){
	//GameManager::getSingletonPtr()->getSoundSystem()->selectResource("music","music","stop");
}

void
MainMenuState::enter ()
{
	GameManager * gameManager = GameManager::getSingletonPtr();
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");

	_menu= new MenuUI();
	_menu->createMenu(2);

	auto music = gameManager->getConfiguration()->getUiConfig();
	gameManager->getSoundManager()->selectResource("music",music->_music,"play");

	_exitGame = false;
}


void
MainMenuState::exit()
{
	_menu->destroyMenu();
	delete _menu;
}

void
MainMenuState::pause ()
{

}

void
MainMenuState::resume ()
{
	_menu= new MenuUI();
	_menu->createMenu(1);
}

bool
MainMenuState::frameStarted
(const Ogre::FrameEvent& evt) 
{
  _timeSinceLastFrame=evt.timeSinceLastFrame;
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_timeSinceLastFrame);
  return true;
}

bool
MainMenuState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
MainMenuState::keyPressed
(const OIS::KeyEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
}

void
MainMenuState::keyReleased
(const OIS::KeyEvent &e )
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(e.key));
}

void
MainMenuState::mouseMoved
(const OIS::MouseEvent &e)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx, rely);
}

void
MainMenuState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
MainMenuState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

void
MainMenuState::endGame() {
	_exitGame = true;
}

MainMenuState*
MainMenuState::getSingletonPtr ()
{
return msSingleton;
}

MainMenuState&
MainMenuState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
CEGUI::MouseButton MainMenuState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

