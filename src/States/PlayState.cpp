#include "PlayState.h"

#include "ScoreBoardState.h"
#include "LoadState.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  GameManager * gameManager = GameManager::getSingletonPtr();

  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");

  _hud= new HUD();
  _hud->createHUD(_playerShip->getLifes(),_playerShip->getShield());
  gameManager->getScoreSystem()->setHUD(_hud);

  _collisionSystem = gameManager->getCollisionSystem();
  _collisionSystem->setRadius(_level->getRadius());

  _animations = gameManager->getAnimationSystem();

  _configuration = gameManager->getConfiguration();
  // Inicializar nave jugador para este nivel
  _playerShip->createNode();
  _playerShip->setRadius(_level->getRadius());
  _playerShip->calculateAngularVelocity();
  _playerShip->setInitialPosition(0.0, 0.0);
  _playerShip->showNode();

  // Crear gestor de ataques de enemigos
  _attacker = new EnemyAttacker();
  _attacker->setPlayer(_playerShip);

  _level->addAttacker(_attacker);
  _level->startLevel();

  gameManager->getSoundManager()->selectResource("music",_level->getMusic(),"play");

  // Posicionador de cámara
  _cameraUpdater = new CameraUpdater(_camera, _playerShip, _level->getRadius() + 15.0, 9.0);

  _shootNumber = 0;

  _exitGame = false;
  _levelFinished = 0;
  _hud->setLevelLabel("");
  _hud->setLevelLabelAlpha(0);
  _blink = 1;
  _labelAlpha = 0.0;
}

void
PlayState::exit ()
{
	// Limpiar todo lo que se recrea al entrar en el nivel
	delete _attacker;
	_hud->destroyHUD();
	delete _hud;
	// Vaciar sistema de colisiones
	_collisionSystem->clear();
	delete _level;

	_animations->clear();

	if(_playerShip->getLifes() > 0) {
		_playerShip->destroyNode();
	}
}

void
PlayState::pause()
{
	_hud->destroyHUD();
}

void
PlayState::resume()
{
	/*_hud->dynamicHUD();*/
}

void
PlayState::score()
{
}


bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{

  ActionResult levelResult;

  _timeSinceLastFrame=evt.timeSinceLastFrame;
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_timeSinceLastFrame);

  if(_levelFinished > 0) {
	    _labelAlpha += _blink * 0.5 * _timeSinceLastFrame;

  		if(_labelAlpha > 1) {
  			_labelAlpha = 1;
  			_blink = -1;
  		}
  		else if(_labelAlpha < 0) {
  			_labelAlpha = 0;
  			_blink = 1;
  		}

  		_hud->setLevelLabelAlpha(_labelAlpha);
  		return true;
  	}

  // modificación del HUD
  //_hud->dynamicHUD();

  Ogre::Real deltaTime = evt.timeSinceLastFrame;
  
  _playerShip-> update(deltaTime);
  _cameraUpdater->update(deltaTime);

  levelResult = _level->update(deltaTime);

  if(levelResult == RESULT_GAME_LOST) {
    _hud->setLevelLabel("Has perdido : ( Pulsa Enter...");
	_levelFinished = 3;

  	return true;
   }
  if(levelResult == RESULT_GAME_WIN) {
  	  std::cout << "YOU WIN\n";
  	if(_level->getLast()){
		  std::cout << "YOU WIN\n";
		  _hud->setLevelLabel("Juego   finalizado.   ¡Enhorabuena!");
		  _levelFinished = 2;
	  }else {
		  _hud->setLevelLabel("Nivel   completado.    Pulsa    Enter.");
		  _levelFinished = 1;
	  }

  	  return true;
   }

  _animations->update(deltaTime);
  _collisionSystem->update();

  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
	if(e.key == OIS::KC_LEFT){
		_playerShip->move(Player::DIRECTION_LEFT);
	}
	else if (e.key == OIS::KC_RIGHT){
		_playerShip->move(Player::DIRECTION_RIGHT);
	}
	else if (e.key == OIS::KC_SPACE){
		_playerShip->shoot();
	}

}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
	if(e.key == OIS::KC_ESCAPE) {
		_exitGame = true;
	}else if(e.key == OIS::KC_LEFT || e.key == OIS::KC_RIGHT){
		_playerShip->stop();
	}
	else if(e.key == OIS::KC_RETURN && _levelFinished > 0) {
		if(_levelFinished == 1) {
			_configuration->getLevelConfig()->_currentLevel++;
			LoadState::getSingletonPtr()->setType(LoadState::LOAD);
			GameManager::getSingletonPtr()->changeState(LoadState::getSingletonPtr());
		}
		else if(_levelFinished == 2) {
			_configuration->getLevelConfig()->_currentLevel = _configuration->getLevelConfig()->_init_level;
			ScoreBoardState::getSingletonPtr()->setType(ScoreBoardState::WRITE);
			GameManager::getSingletonPtr()->changeState(ScoreBoardState::getSingletonPtr());
		}
		else if(_levelFinished == 3) {
			_configuration->getLevelConfig()->_currentLevel = _configuration->getLevelConfig()->_init_level;
			ScoreBoardState::getSingletonPtr()->setType(ScoreBoardState::WRITE);
			GameManager::getSingletonPtr()->changeState(ScoreBoardState::getSingletonPtr());
		}

	}
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;

}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
}


void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}


PlayState*
PlayState::getSingletonPtr ()
{
	return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void PlayState::setLevel(Level* level) {
	_level = level;

}

void PlayState::initPlayer(Player* player) {
	_playerShip = player;
}

void PlayState::process(Event &event) {
	if(event.getType() == Event::TYPE_SHOOT) {
		Event::ArrayAndString & params = event.getParametersArrayAndString();
		int direction = params.n;

		const Ogre::Vector3 & shooterPosition = _sceneMgr->getSceneNode(*params.parent)->getPosition();

		std::stringstream ss;

		ss << "s_shoot_s_p_";
		if(_shootNumber < 100) ss << "0";
		if(_shootNumber < 10) ss << "0";
		ss << _shootNumber;

		Shoot shoot(ss.str(),direction, Shoot::SHOOT_NORMAL, 5.0);

		_shootNumber++;
		_shootNumber%= 1000;
		GameManager::getSingletonPtr()->addListeners(&shoot);
		shoot.createNode();
		shoot.setInitialPosition(shooterPosition.x, shooterPosition.y, shooterPosition.z);
		shoot.showNode();

		_level->addShoot(shoot);
	}
	else if(event.getType() == Event::TYPE_COLLISION) {
		Event::DoubleString & params = event.getParametersDoubleString();

		if(params.name2->at(0) == 'p') {
			_level->receiveImpact(*params.name1);
			if(_playerShip->receiveImpact() == RESULT_GAME_LOST){
				// Player has dead, game over
				std::cout << "YOU LOSE\n";
			  _hud->setLevelLabel("Has   perdido   :(    Pulsa    Enter   ...");
			  _levelFinished = 3;
			}
		}
		else {
			ActionResult impactResult1 = _level->receiveImpact(*params.name1);
			ActionResult impactResult2 = _level->receiveImpact(*params.name2);
			//if(impactResult1 == RESULT_GAME_WIN || impactResult2 == RESULT_GAME_WIN ) {

			//}
		}

	}
	else if(event.getType() == Event::TYPE_HIT) {
		std::string & name = event.getParametersString();

		if(name.at(0) == 'p') {
			std::cout << "- life \n";
			_hud->substractLife();
			_hud->removeLifeGUI();

		}
	}
}
