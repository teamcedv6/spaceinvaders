#include <States/LoadState.h>
#include "PlayState.h"

template<> LoadState* Ogre::Singleton<LoadState>::msSingleton = 0;

void
LoadState::enter ()
{

	GameManager * gameManager = GameManager::getSingletonPtr();

	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_viewport = _root->getAutoCreatedWindow()->getViewport(0);
	// Nuevo background colour.
	//_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 0.0));

	_camera->setPosition(0, 0, 20);
	_camera->lookAt(0.000001, 0.0, 0.0);

	_menu= new LoadUI();
	_menu->createMenu();

	/**
	*
	* Inicio hilos
	*/

	_notEnd = true;

	_waitThread = new ThreadSystem(2,IceUtil::Time::seconds(6));
	IceUtil::ThreadControl tc_waitThread = _waitThread->start();
	tc_waitThread.detach();

	int type = _type;
	IceUtil::ThreadControl tc_loadConfigThread;
	IceUtil::ThreadControl tc_createLevelThread;
	switch ( type ) {
	    // De MainState a LevelState
	  	case INIT:
	  		_loadConfigThread = new ThreadSystem(0);
			tc_loadConfigThread = _loadConfigThread->start();
			tc_loadConfigThread.detach();
	      break;
	    // De LevelState a PlayState
	    case LOAD:
	    	_menu->setLevelNumber();
	    	_createLevelThread = new ThreadSystem(1);
			tc_createLevelThread = _createLevelThread->start();
			tc_createLevelThread.detach();
	      break;

	    default:
	      std::cout << "selección erronea\n";
	      break;
	  }				/* -----  end switch  ----- */

  	/**
  	* Fin prueba hilos
  	*/

  	// Load Planet
  	if(!_sceneMgr->hasSceneNode("LoadPlanet")){
		_loadPlanet = _sceneMgr->createSceneNode("LoadPlanet");
		_sceneMgr->getRootSceneNode()->addChild(_loadPlanet);
		_loadPlanet->setPosition(0,0,0);

		Entity *planetEntity = _sceneMgr->createEntity("PlanetEntity", "LoadPlanet.mesh");
  	}

  	_loadPlanet->attachObject(_sceneMgr->getEntity("PlanetEntity"));

  _exitGame = false;
}

void
LoadState::exit ()
{
	_loadPlanet->detachAllObjects();
	_menu->destroyMenu();

	delete _menu;
}

void
LoadState::pause ()
{
}

void
LoadState::resume ()
{
}

bool
LoadState::frameStarted
(const Ogre::FrameEvent& evt)
{
	Ogre::Real r = 0;

	// Control de tiempo por CEGUI
	_timeSinceLastFrame=evt.timeSinceLastFrame;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_timeSinceLastFrame);

	r=60;
	_loadPlanet->yaw(Ogre::Degree(r * _timeSinceLastFrame));

	_elapsedTime += _timeSinceLastFrame;

	if(_elapsedTime > 0.5){
		_elapsedTime = 0.0;
		_menu->updateProgressBar();
	}

	if(!_loadConfigThread->isAlive()){
	  //cout << "vivo" << endl;
	  if(!_waitThread->isAlive() && _notEnd){
		 cout << "entro solamente una vez" << endl;
		 switch ( _type ) {
			// De MainState a LevelState
			case INIT:
				GameManager::getSingletonPtr()->changeState(MainMenuState::getSingletonPtr ());
			  break;
			// De LevelState a PlayState
			case LOAD:
				GameManager::getSingletonPtr()->changeState(PlayState::getSingletonPtr ());
		 	   break;

			default:
			  std::cout << "selección erronea\n";
			  break;
		  }
	  }
	}


	return true;
}

bool
LoadState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
LoadState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P) {
    popState();
  }
  if(e.key == OIS::KC_ESCAPE){
	  _exitGame = true;
  }
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
}

void
LoadState::keyReleased
(const OIS::KeyEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(e.key));
}

void
LoadState::mouseMoved
(const OIS::MouseEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void
LoadState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
LoadState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

LoadState*
LoadState::getSingletonPtr ()
{
return msSingleton;
}

LoadState&
LoadState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

CEGUI::MouseButton LoadState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void
LoadState::setType(LoadType type) {
	_type = type;
}

