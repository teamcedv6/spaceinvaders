/*
 * EnemyAttacker.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#include "EnemyAttacker.h"

EnemyAttacker::EnemyAttacker() : _player(NULL), _currentDelay(0) {
	_delay = 0.5;
}

EnemyAttacker::EnemyAttacker(const EnemyAttacker & enemyAttacker) {
	_player = enemyAttacker._player;
	_waves = enemyAttacker._waves;
	_delay = enemyAttacker._delay;
	_currentDelay = enemyAttacker._currentDelay;
}

EnemyAttacker& EnemyAttacker::operator = (const EnemyAttacker &enemyAttacker){
	_player = enemyAttacker._player;
	_waves = enemyAttacker._waves;
	return *this;
}

EnemyAttacker::~EnemyAttacker() {}

void EnemyAttacker::setPlayer(Player * player) {
	_player = player;
}

void EnemyAttacker::addWave(Wave * wave) {
	_waves.push_back(wave);
}

void EnemyAttacker::removeWave(Wave * wave) {
	std::vector<Wave*>::iterator it = _waves.begin();
	bool found = false;

	while(!found && it != _waves.end()) {
		if((*it) == wave) {
			_waves.erase(it);
			found = true;
		}
		it++;
	}
}

void EnemyAttacker::tryAttack(Ogre::Real deltaTime) {
	assert(_player != NULL);

	if(_currentDelay <= 0) {
		bool hasAttacked = false;
		std::vector<Wave *>::iterator it = _waves.begin();

		while(!hasAttacked && it != _waves.end()){
			std::vector<Enemy *> potencialAttackers = (*it)->getPotencialAttackers();
			std::vector<Enemy *>::iterator enemyIt = potencialAttackers.begin();

			while(!hasAttacked && enemyIt != potencialAttackers.end()) {
				if(std::abs((*enemyIt)->getPosition() - _player->getPosition()) < 0.05){
					(*enemyIt)->shoot();
					hasAttacked = true;
					_currentDelay = _delay;
				}
				enemyIt++;
			}
			it++;
		}
	}
	else {
		_currentDelay -= deltaTime;
	}
}



