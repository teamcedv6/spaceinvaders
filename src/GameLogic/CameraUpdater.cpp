/*
 * CameraUpdater.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "CameraUpdater.h"


CameraUpdater::CameraUpdater():_inertia(false), _camera(NULL), _player(NULL),
		_distance(20), _pc(0.0), _wc(0.0), _h(0){

}

CameraUpdater::CameraUpdater(Ogre::Camera * camera, Player * player, int distance, float height):
		_inertia(false), _camera(camera), _player(player), _distance(distance),
		_pc(0.0), _wc(player->_maxW), _h(height){

}

CameraUpdater::CameraUpdater(const CameraUpdater & cameraUpdater) {
	_player = cameraUpdater._player;
	_distance = cameraUpdater._distance;
	_inertia = cameraUpdater._inertia;
	_camera = cameraUpdater._camera;
	_pc = cameraUpdater._pc;
	_wc = cameraUpdater._wc;
	_h = cameraUpdater._h;
}

CameraUpdater::~CameraUpdater(){}

CameraUpdater & CameraUpdater::operator =(const CameraUpdater & cameraUpdater){
	_player = cameraUpdater._player;
	_distance = cameraUpdater._distance;
	_inertia = cameraUpdater._inertia;
	_camera = cameraUpdater._camera;
	_pc = cameraUpdater._pc;
	_wc = cameraUpdater._wc;
	_h = cameraUpdater._h;

	return *this;
}

void CameraUpdater::update(Ogre::Real deltaTime) {
	// Player position
	double pp = _player->_p;

	if(!_inertia && (pp - _pc > 0.2 || _pc - pp > 0.2)) {
		_inertia = true;
	}

	if(_inertia) {
		//std::cout << pp << " - " << _pc << "\n";

		if(pp - _pc > 0.001 || _pc - pp > 0.001 ) {
			if(pp > _pc) {
				if(2*M_PI - pp + _pc > pp - _pc) _pc += _wc * deltaTime;
				else _pc += -_wc * deltaTime;
			}
			else {
				if(_pc - pp > 2*M_PI - _pc + pp) _pc += _wc * deltaTime;
				else _pc += -_wc * deltaTime;
			}
			if(_pc > 2 * M_PI) _pc -= (2 * M_PI);
			if(_pc < 0) _pc += (2 * M_PI);
		}
		else {
			_inertia = false;
		}
	}


	_camera->setPosition(_distance * cos(_pc), _h, _distance * sin(_pc));
	_camera->lookAt(0.0001, _h, 0);
}


