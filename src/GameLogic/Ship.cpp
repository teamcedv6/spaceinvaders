/*
 * Ship.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#include "Ship.h"

Ship::Ship(): Observable(), _name("noname"), _lifes(1), _shoot(Shoot::SHOOT_NORMAL), _node(NULL), _p(0), _r(0), _h(0) {}

Ship::Ship(std::string name, int lifes, Shoot::ShootType shoot) :
		Observable(4, Event::TYPE_CREATE |
				Event::TYPE_HIT | Event::TYPE_DESTROY |
				Event::TYPE_DEAD | Event::TYPE_MOVE |
				Event::TYPE_INGAME | Event::TYPE_SHOOT),
				 _name(name), _lifes(lifes), _shoot(shoot), _node(NULL), _p(0), _r(0), _h(0) {}

Ship::Ship(const Ship & ship) : Observable(ship) {
	_lifes = ship._lifes;
	_node = ship._node;
	_shoot = ship._shoot;
	_name = ship._name;
	_p = ship._p;
	_r = ship._r;
	_h = ship._h;
}

Ship::~Ship() {}

Ship& Ship::operator = (const Ship &ship){
	Observable::operator =(ship);

	_lifes = ship._lifes;
	_node = ship._node;
	_shoot = ship._shoot;
	_name = ship._name;
	_p = ship._p;
	_r = ship._r;
	_h = ship._h;
	return *this;

}

void Ship::createNode() {
	_event.setType(Event::TYPE_CREATE);
	_event.setParameters(&_node, &_name);

	this->send(_event);
}

void Ship::showNode() {
	std::string * names [] = {&_name};

	_event.setType(Event::TYPE_INGAME);
	_event.setParameters(NULL, names, 1);

	this->send(_event);
}

void Ship::destroyNode() {
	_event.setType(Event::TYPE_DESTROY);
	_event.setParameters(&_name);

	this->send(_event);
}

void Ship::setInitialPosition(float x, float y, float z) {
	assert(_node != NULL);
	_node->setPosition(x,y,z);
}

void Ship::setInitialPosition(float position, float height) {
	assert(_node != NULL);
	_p = position;
	_h = height;
	_node->setPosition(_r * cos(_p), _h, _r * sin(_p));
	_node->yaw(-Ogre::Radian(position + M_PI/2), Ogre::Node::TS_LOCAL);
}

void Ship::setRadius(int radius) {
	_r = radius;
}

std::string & Ship::getName() {
	return _name;
}

double Ship::getPosition(){
	return _p;
}

const int Ship::getLifes() {
	return _lifes;
}

ActionResult Ship::receiveImpact() {
	_lifes--;

	_event.setType(Event::TYPE_HIT);
	_event.setParameters(&_name);

	this->send(_event);

	if(_lifes == 0) {
		_event.setType(Event::TYPE_DEAD);
		_event.setParameters(&_name);

		this->send(_event);

		_event.setType(Event::TYPE_DESTROY);
		_event.setParameters(&_name);

		this->send(_event);
		return RESULT_DEAD;
	}
	else {
		return RESULT_CONTINUE;
	}
}


void Ship::createShoot(Direction direction) {
	std::string ** noname;
	int directionInt = 0;

	_event.setType(Event::TYPE_SHOOT);
	if(direction == DIRECTION_UP) directionInt = 1;
	else if(direction == DIRECTION_DOWN) directionInt = -1;

	_event.setParameters(&_name, noname, directionInt);

	this->send(_event);
}


