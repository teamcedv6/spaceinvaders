/*
 * Level.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#include "Level.h"

Level::Level():
_name("noname"), _r(0), _music("nomusic"), _last(0), _waveGenerator(NULL), _attacker(NULL) {}

Level::Level(std::string name, int radius, std::string music, Planet & planet, int last) :
 _name(name), _r(radius), _music(music), _last(last), _planet(planet), _waveGenerator(NULL), _attacker(NULL) {

}

Level::Level(const Level & level) {
	_name = level._name;
	_music = level._music;
	_planet = level._planet;	
	_r = level._r;
	_waveGenerator = level._waveGenerator;
	_last = level._last;
	_attacker = NULL;
}

Level::~Level() {
	delete _waveGenerator;

	for(std::vector<Wave*>::iterator it = _waves.begin();
			it != _waves.end(); it++) {
		delete *it;
	}

	for(std::vector<Shoot>::iterator it = _shoots.begin();
			it != _shoots.end(); it++) {
		it->destroyNode();
	}

	_planet.destroyNode();

	_shoots.clear();
}

Level& Level::operator = (const Level &level){
	_name = level._name;
	_music = level._music;
	_planet = level._planet;
	_r = level._r;
	_waveGenerator = level._waveGenerator;
	_last = level._last;
	return *this;
}

std::string & Level::getName() {
	return _name;
}

int Level::getRadius() {
	return _r;
}

std::string & Level::getMusic() {
	return _music;
}

ActionResult Level::update(Ogre::Real deltaTime) {
	ActionResult updateResult;

	/* Update planet */
	_planet.update(deltaTime);


	/* Update waves */
	std::vector<Wave*>::iterator waveIter = _waves.begin();
	  while(waveIter != _waves.end()) {
		  updateResult = (*waveIter)->update(deltaTime);
		  if(updateResult == RESULT_DESTROY) {
			  Wave * wave = (*waveIter);
			  waveIter = _waves.erase(waveIter);
			  _attacker->removeWave(wave);
			  _waveGenerator->deleteCurrentWave(wave);
			  delete wave;
		  }
		  else if(updateResult == RESULT_GAME_LOST) {
			  // Player has lost..
			  return RESULT_GAME_LOST;
		  }
		  else {
			  waveIter++;
		  }
	 }

		/* Are there any new wave? */
		Wave * newWave = _waveGenerator->askForWave();
		if(newWave != NULL) {
			newWave->createNode();
			newWave->calculateEnemiesInitialPosition();
			_attacker->addWave(newWave);
			_waves.push_back(newWave);
			newWave->showNode();
		}


	  /* Enemies shoots */
	  _attacker->tryAttack(deltaTime);

	 /* Update shoots */
	  std::vector<Shoot>::iterator shootIter =_shoots.begin();

	while(shootIter != _shoots.end()) {
	  if(shootIter->update(deltaTime)) {
		  shootIter++;
	  }
	  else {
		 shootIter= _shoots.erase(shootIter);
	  }
	}

	if(_waves.size() == 0) return RESULT_GAME_WIN;
	else return RESULT_CONTINUE;
}

ActionResult Level::receiveImpact(std::string & name) {
	if(name.at(0 )== 's') {
		bool found = false;

		std::vector<Shoot>::iterator it = _shoots.begin();
		while(!found && it != _shoots.end()){
			if(it->getName() == name) {
				found = true;
				it->destroyNode();
				_shoots.erase(it);
			}
			it++;
		}
	}
	else if(name.at(0) == 'e') {

		int enemyRow = std::atoi(name.substr(13,14).c_str());
		int enemyColumn = std::atoi(name.substr(16,17).c_str());


		bool found = false;
		std::vector<Wave*>::iterator waveIter = _waves.begin();

		std::stringstream ss;
		ss << "wave_" << name.substr(10,2);
		std::string name = ss.str();

		while(!found && waveIter != _waves.end()){
			if((*waveIter)->getName() == name){
				found = true;
				ActionResult impactResult = (*waveIter)->receiveImpact(enemyRow, enemyColumn);
				if(impactResult == RESULT_DEAD) {
					Wave * wave = (*waveIter);
					_attacker->removeWave(wave);
					_waves.erase(waveIter);
					_waveGenerator->deleteCurrentWave(wave);
					delete wave;
				}
			}
			waveIter++;
		}
	}

	return RESULT_CONTINUE;
}

void Level::startLevel() {
	assert(_waveGenerator != NULL);
	assert(_r != 0);
	/* Show planet */
	/* Show walls */
	/* Get first wave if necessary */
	Wave * w = _waveGenerator->askForWave();
	/* Show wave */
	if(w != NULL) {
		w->createNode();
		//w->setInitialPosition(0, 30);
		w->calculateEnemiesInitialPosition();
		_attacker->addWave(w);
		_waves.push_back(w);
		w->showNode();
	}

	_planet.createNode();
	_planet.setInitialPosition(0,0,0);
	_planet.showNode();
}
int Level::getLast() {
	return _last;
}

void Level::addShoot(Shoot & shoot) {
	_shoots.push_back(shoot);
}

void Level::addWall(Wall wall){

}

void Level::addWaveGenerator(WaveGenerator * generator) {
	_waveGenerator = generator;
}

void Level::addAttacker(EnemyAttacker * enemyAttacker) {
	_attacker = enemyAttacker;
}
