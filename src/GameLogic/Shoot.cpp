/*
 * CameraUpdater.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "Shoot.h"

Shoot::Shoot() : Observable(4, Event::TYPE_CREATE |
		Event::TYPE_DESTROY | Event::TYPE_MOVE |
		Event::TYPE_INGAME), _direction(1), _type(SHOOT_NORMAL),
		_name("shoot"), _node(NULL), _height(5.0) {
	_v = 2.0;
}

Shoot::Shoot(std::string name,int direction, ShootType type, float height) :
		Observable(4, Event::TYPE_CREATE | Event::TYPE_DESTROY |
		Event::TYPE_MOVE | 	Event::TYPE_INGAME),
		_name(name), _direction(direction), _type(type), _node(NULL), _height(height) {
	_v = 7.0;
}

Shoot::Shoot(const Shoot & shoot) : Observable(shoot) {
	_name = shoot._name;
	_v = shoot._v;
	_direction = shoot._direction;
	_type = shoot._type;
	_node = shoot._node;
	_height = shoot._height;
}

Shoot& Shoot::operator = (const Shoot &shoot) {
	Observable::operator =(shoot);
	_name = shoot._name;
	_v = shoot._v;
	_direction = shoot._direction;
	_type = shoot._type;
	_node = shoot._node;
	_height = shoot._height;
	return *this;
}
Shoot::~Shoot(){

}

std::string & Shoot::getName() {
	return _name;
}

void Shoot::createNode() {
	_event.setType(Event::TYPE_CREATE);
	_event.setParameters(&_node, &_name);

	this->send(_event);
}

void Shoot::showNode() {
	std::string * names [] = {&_name};

	_event.setType(Event::TYPE_INGAME);
	_event.setParameters(NULL, names, 1);

	this->send(_event);
}

void Shoot::destroyNode() {
	_event.setType(Event::TYPE_DESTROY);
	_event.setParameters(&_name);

	this->send(_event);
}

bool Shoot::update(Ogre::Real deltaTime) {
	_node->translate(0, _direction * _v * deltaTime, 0, Ogre::Node::TS_WORLD);

	float y = _node->getPosition().y;

	_event.setType(Event::TYPE_MOVE);
	_event.setParameters(&_node, &_name);

	this->send(_event);

	if(y > _maxHeight || y < _minHeight) {
		this->destroyNode();
		return false;
	}
	else {
		return true;
	}

}

void Shoot::setInitialPosition(float x, float y, float z) {
	assert(_node != NULL);	;
	_node->setPosition(x,y + (_height/2 * _direction),z);
}
