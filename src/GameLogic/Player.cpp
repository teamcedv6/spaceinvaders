/*
 * Player.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "Player.h"

Player::Player() : Ship("player25",3, Shoot::SHOOT_NORMAL),
_shield(0), _maxW(5.0), _w(0), _maxV(0), _shootDelay(1.0), _currentDelay(0)  {}

Player::Player(std::string name, int lifes, int shield, Ogre::Real shootDelay) :
				Ship(name,lifes, Shoot::SHOOT_NORMAL),
				_shield(shield), _maxW(0), _maxV(0), _w(0), _shootDelay(shootDelay),
				_currentDelay(0) {}

Player::Player(const Player & player) : Ship(player) {
	_shield = player._shield;
	_maxW = player._maxW;
	_w = player._w;
	_p = player._p;
	_shootDelay = player._shootDelay;
	_maxV = player._maxV;
	_currentDelay = 0;

}

Player::~Player(){

}

Player& Player::operator = (const Player &player) {
	Ship::operator =(player);
	_shield = player._shield;
	_maxW = player._maxW;
	_w = player._w;
	_p = player._p;
	_shootDelay = player._shootDelay;
	_maxV = player._maxV;
	_currentDelay = 0;

	return *this;
}

void Player::setMaxVelocity(double maxV) {
	_maxV = maxV;
}

void Player::calculateAngularVelocity() {
	_maxW = _maxV/_r;
}

int Player::getShield() {
	return _shield;
}

void Player::shoot() {
	if(_currentDelay == 0) {
		this->createShoot(DIRECTION_UP);
		_currentDelay = _shootDelay;
	}
}

ActionResult Player::receiveImpact() {
	if(_shield - 1 >= 0) {
		_shield--;
		return RESULT_CONTINUE;
	}
	else {
		if(Ship::receiveImpact() == RESULT_DEAD){
			return RESULT_GAME_LOST;
		}
		else {
			return RESULT_CONTINUE;
		}
	}
}

ActionResult Player::update(Ogre::Real deltaTime) {
	_p += _w * deltaTime;
	if(_p > 2 * M_PI) _p -= (2 * M_PI);
	if(_p < 0) _p += (2 * M_PI);

	assert(_node != NULL);

	if(_currentDelay > 0) {
		_currentDelay -= deltaTime;
		if(_currentDelay < 0) _currentDelay = 0;
	}

	/* Position = p0 + w * t
	 * Incremento de p = Ap = w * At
	 * Position en el instante n = p(n) = p(n-1) + Ap
	 */
	_node->setPosition(_r * cos(_p), _h, _r * sin(_p));
	_node->yaw(-Ogre::Radian(_w * deltaTime), Ogre::Node::TS_LOCAL);

	if(std::abs(_w) > 0){
		_event.setType(Event::TYPE_MOVE);
		_event.setParameters(&_node, &_name);

		this->send(_event);
	}


	/*int fps = 1.0/deltaTime;
		std::cout << fps << "\n";*/

	return RESULT_CONTINUE;
}

void Player::move(Direction direction) {
	_w = _maxW * direction;
}

void Player::stop() {
	_w = 0.0;
}



