/*
 * Enemy.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "Enemy.h"

Enemy::Enemy() : Ship("Enemy25",1, Shoot::SHOOT_NORMAL), _hasMoved(false), _row(0), _column(0){_minHeight = 1;}

Enemy::Enemy(std::string name, int lifes, Shoot::ShootType shoot, int row, int column) :
				Ship(name,lifes, shoot) , _hasMoved(false),
				_row(row), _column(column)
				{_minHeight = 1;}

Enemy::Enemy(const Enemy & enemy) : Ship(enemy){
	_hasMoved = false;
	_minHeight = 1;
	_row = enemy._row;
	_column = enemy._column;
}

Enemy::~Enemy(){

}

Enemy& Enemy::operator = (const Enemy &enemy) {
	Ship::operator =(enemy);
	_minHeight = 1;
	_row = enemy._row;
	_column = enemy._column;
	return *this;
}

void Enemy::shoot() {
	this->createShoot(DIRECTION_DOWN);
}

void Enemy::setPosition(float p) {
	_p = p;
}

void Enemy::move(float position, float height) {
	_dp = position - _p;
	_p = position;
	_h = height;
	_hasMoved = true;

	if(_p > 2 * M_PI) _p -= 2*M_PI;
	if(_p < 0) _p += 2*M_PI;
}

ActionResult Enemy::update(Ogre::Real deltaTime) {
	//std::cout << _name << " - "<< _p << "\n";

	if(_hasMoved){
		_node->setPosition(_r * cos(_p), _h, _r * sin(_p));
		_node->yaw(-Ogre::Radian(_dp), Ogre::Node::TS_LOCAL);
		_event.setType(Event::TYPE_MOVE);
		_event.setParameters(&_node, &_name);
		_hasMoved = false;

		this->send(_event);

		if(_h <= _minHeight) {
			return RESULT_GAME_LOST;
		}
	}

	return RESULT_CONTINUE;

}

const int Enemy::getRow() const{
	return _row;
}

const int Enemy::getColumn() const{
	return _column;
}

void Enemy::setUniqueParameters(std::string name, int row, int column) {
	_name = name;
	_row = row;
	_column = column;
}




