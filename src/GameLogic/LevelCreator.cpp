#include "LevelCreator.h"

LevelCreator::LevelCreator(){

}

LevelCreator::~LevelCreator(){

}

void LevelCreator::createLevel(Configuration* configuration) {

	GameManager * gm = GameManager::getSingletonPtr();

	// Get actual level
   	auto configLevel = (configuration->getLevelConfig()->_levels)[configuration->getLevelConfig()->_currentLevel];

	auto _collisionSystem = gm->getCollisionSystem();
   	_collisionSystem->setRadius(configLevel._radio);

	// Get config
	auto configMusic = (configuration->getMusicConfig()->_tracks)[configLevel._music];
	auto configPlanet = (configuration->getPlanetConfig()->_planets)[configLevel._planet];
	auto configLast = configLevel._last;
	auto configRadio = configLevel._radio;

	std::stringstream ss;
	ss << "t_" << configPlanet._name << "_n";
	std::cout << configPlanet._name;
	std::cout << ss.str() << "\n";

	Planet* planet = new Planet(ss.str(),configPlanet._mesh,configPlanet._speed,configPlanet._size,configPlanet._radio);
	gm->addListeners(planet);
//	planet->createNode();

	WaveGenerator * generator = new WaveGenerator();
	Spawner * spawner = GameManager::getSingletonPtr()->getSpawner();

	int waveNumber = 0;

	for (auto kv1 : configLevel._waves) {
		// Get actual wave
		auto configWave = kv1.second;
		auto split = configWave._split;
		auto speed = configWave._speed;
		auto move = configWave._move;

		std::vector<Enemy,allocator<Enemy> >* enemies = new std::vector<Enemy,allocator<Enemy>>[split];
		int column = 0;
		int row = 0;

		for (auto kv2 : configWave._enemies) {
			// Get actual enemy
			auto configEnemy = kv2.second;
			//auto dataConfigEnemy = (configuration->getEnemyConfig()->_enemies)[configEnemy._enemy];
			//auto dataShoot = (configuration->getShootConfig()->_shoots)[dataConfigEnemy._shoot];
			auto number = configEnemy._number;
			int j = 0;

			while(j < number) {
				// Clonar
				std::stringstream ss2;
				ss2 << "e_" << configEnemy._enemy << "_r_";
				if(waveNumber < 10) ss2 << "0";
				ss2 << waveNumber << "_";
				if(row < 10) ss2 << "0";
				ss2 << row << "_";
				if(column < 10) ss2 << "0";
				ss2 << column;

				Enemy clonedEnemy = spawner->cloneEnemy(configEnemy._enemy);
				clonedEnemy.setUniqueParameters(ss2.str(),row,column);
				gm->addListeners(&clonedEnemy);
				(enemies+column)->push_back(clonedEnemy);

				j++;
				column++;
				if(column == split){
					column = 0;
					row++;
				}
			}
		}

		std::stringstream ss3;
		ss3 << "wave_";
		if(waveNumber < 10) ss3 << "0";
		ss3 << waveNumber;

		waveNumber++;

		Wave::Movement movement = Wave::ZIGZAG;
		if(configWave._move == "classic") {
			movement = Wave::ZIGZAG;
		}
		else if(configWave._move == "circle") {
			movement = Wave::LINE;
		}

		auto wave = new Wave(ss3.str(),enemies,split,movement);
		wave->setRadius((int)configRadio);
		wave->setMaxVelocity(configWave._speed);
		Ogre::Radian alpha(Ogre::Degree(configWave._angle));

		wave->setInitialPosition(alpha.valueRadians(),configWave._height);

		gm->addListeners(wave);
		generator->addWave(wave);
		std::cout << "End level create\n";
	}

	std::string name = "Level 1";
	Level* level = new Level(name,(int)configRadio,get<0>(configMusic),*planet, configLast);

	level->addWaveGenerator(generator);

	PlayState::getSingletonPtr()->setLevel(level);
}
