/*
 * Planet.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#include "Planet.h"

Planet::Planet() : Observable(4, Event::TYPE_CREATE |
		Event::TYPE_DESTROY | Event::TYPE_MOVE |
		Event::TYPE_INGAME),
		_node(NULL), _name(NULL), _mesh(NULL), _speed(0), _size(0), _radio(0){

}

Planet::Planet(std::string name, std::string mesh, float speed, float size, float radio) :
		Observable(4, Event::TYPE_CREATE | Event::TYPE_DESTROY |
		Event::TYPE_MOVE | 	Event::TYPE_INGAME),
		_node(NULL), _name(name), _mesh(mesh), _speed(speed), _size(size), _radio(radio) {

}

Planet::Planet(const Planet & planet) : Observable(planet) {
	_node = planet._node;
	_name = planet._name;
	_mesh = planet._mesh;
	_speed = planet._speed;
	_size = planet._size;
	_radio = planet._radio;
}

Planet& Planet::operator = (const Planet &planet) {
	Observable::operator =(planet);
	_node = planet._node;
	_name = planet._name;
	_mesh = planet._mesh;
	_speed = planet._speed;
	_size = planet._size;
	_radio = planet._radio;
	return *this;
}



Planet::~Planet(){

}

std::string & Planet::getName() {
	return _name;
}

void Planet::createNode() {
	_event.setType(Event::TYPE_CREATE);
	_event.setParameters(&_node, &_name);

	this->send(_event);
}

void Planet::showNode() {
	std::string * names [] = {&_name};

	_event.setType(Event::TYPE_INGAME);
	_event.setParameters(NULL, names, 1);

	this->send(_event);
}

void Planet::destroyNode() {
	_event.setType(Event::TYPE_DESTROY);
	_event.setParameters(&_name);

	this->send(_event);
}

ActionResult Planet::update(Ogre::Real deltaTime) {
	Ogre::Real r = 0; r=10;
	_node->yaw(Ogre::Degree(r * deltaTime));
	return RESULT_CONTINUE;
}

void Planet::setInitialPosition(float x, float y, float z) {
	assert(_node != NULL);	;
	_node->setPosition(x,20,z);
}




