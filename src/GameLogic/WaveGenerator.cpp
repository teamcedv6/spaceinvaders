/*
 * WaveGenerator.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#include "WaveGenerator.h"

WaveGenerator::WaveGenerator() {}

WaveGenerator::WaveGenerator(const WaveGenerator & WaveGenerator) {

}

WaveGenerator::~WaveGenerator() {}

WaveGenerator& WaveGenerator::operator = (const WaveGenerator &WaveGenerator){

	return *this;
}

void WaveGenerator::addWave(Wave * wave) {
	_futureWaves.insert(_futureWaves.begin(), wave);
}

Wave * WaveGenerator::askForWave() {
	if(_futureWaves.size() > 0) {
		if(_currentWaves.size() == 0){
			Wave * newWave = _futureWaves.at(_futureWaves.size() -1 );
			_futureWaves.pop_back();
			_currentWaves.push_back(newWave);
			return newWave;
		}
		else {
			int enemyNumber = 0;

			for(std::vector<Wave*>::iterator it = _currentWaves.begin();
					it != _currentWaves.end(); it++){
				enemyNumber += (*it)->getEnemyNumber();
			}
			if(enemyNumber < 5){
				Wave * newWave = _futureWaves.at(_futureWaves.size() -1 );
				_futureWaves.pop_back();
				_currentWaves.push_back(newWave);
				return newWave;
			}
		}
	}



	return NULL;
}

void WaveGenerator::deleteCurrentWave(Wave * wave){
	std::vector<Wave*>::iterator it = _currentWaves.begin();
	bool found = false;

	while(!found && it != _currentWaves.end()) {
		if((*it)->getName() == wave->getName()) {
			found = true;
			_currentWaves.erase(it);
		}
		it++;
	}
}



