/*
 * Spawner.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#include "Spawner.h"

Spawner::Spawner() {

}

Spawner::Spawner(const Spawner & spawner) {

}

Spawner& Spawner::operator = (const Spawner &spawner){

	return *this;
}

Spawner::~Spawner() {}

Enemy Spawner::cloneEnemy(std::string name){
	if(_enemyModels.count(name) == 1){
		return _enemyModels[name];
	}
	else {
		return Enemy();
	}
}

void Spawner::addEnemyModel(Configuration::Enemy_t::enemies_t & enemyData){

		Shoot::ShootType shoot;

		/*if(enemyData->_shoot == "") {

		}*/

		Enemy newEnemy (enemyData._name, (int)enemyData._life,Shoot::SHOOT_NORMAL,0,0);
		_enemyModels[enemyData._name] = newEnemy;
}




