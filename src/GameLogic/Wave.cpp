/*
 * Wave.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */

#include "Wave.h"

Wave::Wave(): Observable(), _name("noname"), _enemiesColumns(NULL), _columnsNumber(0), _enemiesNumber(0), _movement(ZIGZAG),
	_widthSeparation(3), _heightSeparation(1.5), _widthDistance(60), _heightDistance(10),_r(0), _h(0),
	_v(0), _w(0), _p0(0), _p(0), _movementDirection(Ship::DIRECTION_RIGHT),
	_currentHeightDistance(0),_currentWidthDistance(0) {}

Wave::Wave(std::string name, std::vector<Enemy> * enemies, int columnsNumber, Movement movement) :
		Observable(4, Event::TYPE_CREATE | Event::TYPE_INGAME |
				Event::TYPE_SHOOT),
				_name(name), _enemiesColumns(enemies), _columnsNumber(columnsNumber),
				_movement(movement), _r(0), _h(0), _v(0), _w(0), _p0(0),
				_p(0), _movementDirection(Ship::DIRECTION_RIGHT),
				_currentHeightDistance(0), _currentWidthDistance(0) {
	_enemiesNumber = 0;
	_widthSeparation = 0.04;
	_heightSeparation = 1.5;
	_widthDistance = 0.2;
	_heightDistance = 2;


	for(int i = 0; i < columnsNumber; i++) {
		_enemiesNumber += (enemies+i)->size();
	}
}

Wave::Wave(const Wave & Wave) {
//	_waveNode = NULL;
	_name = Wave._name;
	//_enemiesColumns =
}

Wave::~Wave() {
	delete [] _enemiesColumns;
}

Wave& Wave::operator = (const Wave &Wave){
	_name = Wave._name;
	return *this;
}

void Wave::createNode() {
	int columnLenght = 0;

	for(int i = 0; i < _columnsNumber; i++) {
		columnLenght = (_enemiesColumns + i)->size();
		for(int j = 0; j < columnLenght; j++){
			(_enemiesColumns + i)->at(j).createNode();
		}
	}
}

void Wave::showNode() {
	std::string * names [_enemiesNumber];

	int columnLenght = 0;

	for(int i = 0; i < _columnsNumber; i++) {
		columnLenght = (_enemiesColumns + i)->size();
		for(int j = 0; j < columnLenght; j++){
			names[i * columnLenght + j] = &(_enemiesColumns + i)->at(j).getName();
		}
	}

	_event.setType(Event::TYPE_INGAME);
	_event.setParameters(NULL, names, _enemiesNumber);

	this->send(_event);
}

void Wave::destroyNode() {
	/*_event.setType(Event::TYPE_DESTROY);
	_event.setParameters(&_name);

	this->send(_event);*/
}

void Wave::setInitialPosition(float x, float y, float z) {
	//assert(_waveNode != NULL);
	//_waveNode->setPosition(x,y,z);
	// Set enemies position
	// L = r * ɸ

}

void Wave::setInitialPosition(float position, float height) {
	_p0 = position;
	_p = position;
	_h = height;
}

void Wave::calculateEnemiesInitialPosition() {
	double childP = 0.0;
	double leftPos = (_columnsNumber - 1)  * _widthSeparation * 2;

	if(_columnsNumber % 2 == 1) {
		leftPos += _widthSeparation;
	}
	for(int i = 0; i < _columnsNumber; i++) {
		for(int j = 0; j < (_enemiesColumns + i)->size(); j++){

			childP = _p - leftPos + (_widthSeparation * 2 * i);
			if(childP > 2 * M_PI) childP -= 2 * M_PI;
			if(childP < 0) childP += 2 * M_PI;
			(_enemiesColumns + i)->at(j).setInitialPosition(childP, _h - _heightSeparation * j);
		}

	}

}

void Wave::setMaxVelocity(double maxV) {
	_v = maxV;
	_w = maxV/_r;
}

void Wave::setRadius(int radius) {
	_r = radius;
	int columnLenght;

	for(int i = 0; i < _columnsNumber; i++) {
		columnLenght = (_enemiesColumns + i)->size();
		for(int j = 0; j < columnLenght; j++){
			(_enemiesColumns + i)->at(j).setRadius(radius);
		}
	}
}

std::string & Wave::getName() {
	return _name;
}

std::vector<Enemy *> Wave::getPotencialAttackers() {
	std::vector<Enemy *> attackers;
	attackers.reserve(_columnsNumber);

	for(int i = 0; i < _columnsNumber; i++) {
		if((_enemiesColumns + i)->size() > 0) {
			attackers.push_back(&*(_enemiesColumns + i)->rbegin());
		}
	}

	return attackers;
}

int Wave::getEnemyNumber() {
	return _enemiesNumber;
}

ActionResult Wave::receiveImpact(int row, int column) {
	std::vector<Enemy>::iterator it = (_enemiesColumns + column)->begin();
	bool found = false;
	ActionResult impactResult = RESULT_CONTINUE;

	while(!found && it != (_enemiesColumns + column)->end()) {
		std::cout << "row: " << it->getRow() << " -- column: "  << it->getColumn() << "\n";
		if(it->getRow() == row && it->getColumn() == column){
			found = true;
			std::cout << "found! \n";
		}
		else it++;
	}

	if(found) {
		impactResult = it->receiveImpact();
	}

	if(impactResult == RESULT_DEAD) {
		// Delete object related with destroyed enemy
		(_enemiesColumns + column)->erase(it);

		_enemiesNumber--;
	}

	if(_enemiesNumber == 0) {
		return RESULT_DEAD;
	}
	else return RESULT_CONTINUE;
}

ActionResult Wave::update(Ogre::Real deltaTime) {
	/* Update enemies */
	ActionResult updateResult;

	if(_movement == LINE){
		// While hasn't complete a lap
		if(_p < 2 * M_PI) {
			_p += _w * deltaTime;

			return positionEnemies(deltaTime);

		}
		else {
			//Lap completed, erase enemy
			for(int i = 0; i < _columnsNumber; i++) {
				for(int j = 0; j < (_enemiesColumns+i)->size(); j++) {
					(_enemiesColumns + i)->at(j).destroyNode();
				}
			}

			return RESULT_DESTROY;
		}
	}
	else if(_movement == ZIGZAG) {
		//std::cout << "dist: " << _currentWidthDistance << " -- reach: " << _widthDistance << " !  " << "\n" ;
		if(_movementDirection == Ship::DIRECTION_RIGHT) {
			_p -= _w * deltaTime;
			_currentWidthDistance -= _w * deltaTime;
			//std::cout << "what\n";

			if(_currentWidthDistance <= -_widthDistance) {
				_currentHeightDistance = 0;
				_movementDirection = Ship::DIRECTION_DOWN;
			}
		}
		else{
			if (_movementDirection == Ship::DIRECTION_LEFT) {
				_p += _w * deltaTime;
				_currentWidthDistance += _w * deltaTime;
				//std::cout << "what3\n";
				if(_currentWidthDistance >= _widthDistance) {
					_currentHeightDistance = 0;
					_movementDirection = Ship::DIRECTION_DOWN;
				}
			}
			else {
				if (_movementDirection == Ship::DIRECTION_DOWN) {
					_h -= _v * deltaTime;
					_currentHeightDistance += _v * deltaTime;
					//std::cout << "what2\n";
					if(_currentHeightDistance >= _heightDistance) {
						std::cout << "what4\n";
						if(_currentWidthDistance < 0) {
							_currentWidthDistance = -2* _widthDistance;
							_movementDirection = Ship::DIRECTION_LEFT;
						}
						else {
							_currentWidthDistance = 2* _widthDistance;
							_movementDirection = Ship::DIRECTION_RIGHT;
						}
					}
				}
			}
		}

		return positionEnemies(deltaTime);
		//std::cout<< (_enemiesColumns)->size() << " -- " << (_enemiesColumns+1)->size();
	}
	return RESULT_CONTINUE;
}

ActionResult Wave::positionEnemies(Ogre::Real deltaTime) {
	std::vector<Enemy>::iterator it;
	ActionResult result = RESULT_CONTINUE;

	double leftPos = (_columnsNumber - 1)  * _widthSeparation * 2;
	if(_columnsNumber % 2 == 1) {
		leftPos += _widthSeparation;
	}
	for(int i = 0; i < _columnsNumber; i++) {
		for(int j = 0; j < (_enemiesColumns + i)->size(); j++){
			it = (_enemiesColumns + i)->begin() + j;
			it->move(_p - leftPos + (_widthSeparation * 2 * it -> getColumn()),
					_h - _heightSeparation * it->getRow());
			result = it->update(deltaTime);

			if(result == RESULT_GAME_LOST) {
				return result;
			}

		}

	}

	return RESULT_CONTINUE;
}



