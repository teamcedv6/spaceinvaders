/*
 * UserInterface.cpp
 *
 *  Created on: 22 feb. 2017
 *      Author: joe
 */

#include "UserInterface.h"
#include <iostream>

UserInterface::UserInterface()	{
	std::cout << "Constructor UserInterface ... " << std::endl;
}

void UserInterface::createInterface() {
	//CEGUI::Scheme::setDefaultResourceGroup("Schemes");
	//CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
	//CEGUI::Font::setDefaultResourceGroup("Fonts");
	//CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
	//CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

	CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("SpellByte.scheme");
	//CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("SpaceInvaders");
	//CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");
}

void UserInterface::destroyInterface() {
	CEGUI::SchemeManager::getSingleton().destroyAll();
	std::cout << "destruyendo Interface ..." << std::endl;
}

UserInterface::~UserInterface()	{
	//CEGUI::OgreRenderer::destroySystem(); este para cuando se salga del juego
	std::cout << "Destructor UserInterface ..." << std::endl;
}
