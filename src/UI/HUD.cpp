/*
 * HUD.cpp
 *
 *  Created on: 28 feb. 2017
 *      Author: joe
 */

#include <HUD.h>
#include <iostream>

HUD::HUD() : UserInterface() {
  _lifes=3;
  _shield=0;
  _score=0;
  sheet=NULL; lay=NULL; pbar=NULL;
  pscore=NULL;
  _lifebar=0.8;
  std::cout << "Constructor HUD ..." << std::endl;
}
HUD::~HUD() {
  std::cout << "Destructor HUD ..." << std::endl;
  plife.clear();
  pshield.clear();
  UserInterface::destroyInterface();
}

void HUD::setLifes(int n) {
  _lifes=n;
}

void HUD::setShield(int n) {
  _shield=n;
}

void HUD::substractLife() {
  _lifes--;
}

void HUD::substractShield() {
  _shield--;
}

void HUD::addShield() {
  _shield++;
}

void HUD::addLife() {
  _lifes++;
}

void HUD::addScore(int score) {
  _score+=score;
  ss.str(std::string());
  	  ss << _score;
  	  str=ss.str();
  	pscore->setText(str );
}

int HUD::getLifes() const {
  return _lifes;
}

int HUD::getShield() const {
  return _shield;
}

int HUD::getScore() const {
  return _score;
}

void HUD::changeLbar(float f) {
  _lifebar+=f;
	if ( _lifebar >= 1.0F )
	_lifebar=1.0F;
  else if ( _lifebar <= 0.0F )
	_lifebar=0.0F;
}

void HUD::intostring() {
  ss.str(std::string());
  ss << _score;
  str=ss.str();
}

void HUD::createHUD(int life, int shield) {
  // Interface General
  UserInterface::createInterface();

  // Interface particular del HUD
  CEGUI::SchemeManager::getSingleton().createFromFile("Ship.scheme");
  CEGUI::SchemeManager::getSingleton().createFromFile("Shield.scheme");
  CEGUI::SchemeManager::getSingleton().createFromFile("Generic.scheme");
  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("SpaceInvaders");

  // Ocultar el cursor
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();

  // Crea hoja principal
  sheet=CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "HUD/Sheet");

  // Carga el layout
  lay=CEGUI::WindowManager::getSingleton().loadLayoutFromFile("HudSpiBasic.layout");

  // Obtiene el progress bar de hijo
  pbar=lay->getChild("ProgressBar");
  pbar->hide();

  // Obtiene la vida hijo del layout
  plife.push_back(lay->getChild("Ship"));
  sheet->addChild(plife[0]);

  for(int i=0; i<1; i++) {
	  // Clonamos la ventana a la que apunta el elemento anterior
	  // auto last = i-1;
	  plife.push_back(plife[i]->clone());
	  // Formamos el nombre
	  ss.str(std::string()); ss << "Ship" << i+1; str=ss.str();
	  // Asignamos el nombre
	  plife[i+1]->setName(str);
	  // Obtenemos la posición en x
	  CEGUI::UDim Uaux=plife[i+1]->getXPosition();
	  // Aumentamos la x
	  Uaux.d_offset+=55;
	  // Establecemos una nueva posición en X
	  plife[i+1]->setXPosition(Uaux);
	  // Añadimos el hijo
	  sheet->addChild(plife[i+1]);
  }

  pshield.push_back(lay->getChild("Shield"));
  // pshield[0]->disable();
  pshield[0]->hide();
  sheet->addChild(pshield[0]);

  // Obtiene el texto del score
  pscore=lay->getChild("OneupPoints");

  // Formamos el texto a mostrar
  intostring();

  // Asociamos el texto a la label donde se mostrará
  pscore->setText(str);

  // Añadimos el layout
  sheet->addChild(lay);

  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
}

void HUD::addLifeGUI() {
	if ( _lifes == 2 ) {
			//plife[0]->activate();
			plife[0]->show();
	}
	else if (_lifes >=3) {
		int i=_lifes-3;
		plife.push_back(plife[i]->clone());
		ss.str(std::string()); ss << "Ship" << i+1; str=ss.str();
		plife[i+1]->setName(str);

		if (0 == (plife.size()%5)) { // ojo aquí el size ya es uno más que al principio
			CEGUI::UDim Uaux1=plife[i+1]->getYPosition();
			Uaux1.d_offset-=55;
			plife[i+1]->setYPosition(Uaux1);
			CEGUI::UDim Uaux2=plife[0]->getXPosition();
			plife[i+1]->setXPosition(Uaux2);
		}
		else {
			// Obtenemos la posición en x
			CEGUI::UDim Uaux3=plife[i+1]->getXPosition();
			// Aumentamos la x
			Uaux3.d_offset+=55;
			// Establecemos una nueva posición en X
			plife[i+1]->setXPosition(Uaux3);
		}
		// Añadimos el hijo
		sheet->addChild(plife[i+1]);
		}
}

void HUD::removeLifeGUI() {
	int j=_lifes-1;
	std::cout << "j ==== " << j << "\n";
	if ( j==0 ) { // si el índice es igual a 0
		plife[0]->disable();
		plife[0]->hide();
	}
	else if(j > 0){
		sheet->removeChild(plife[j]);
		plife.pop_back();
	}
}

void HUD::addShieldGUI() {
	if ( _shield == 1 ) {
		 pshield[0]->activate();
		pshield[0]->show();
	}
	else if ( _shield >= 2 ) {
		int r=_shield-2; // tamaño antes de añadir
		pshield.push_back(pshield[r]->clone());
		ss.str(std::string()); ss << "Shield" << r+1; str=ss.str();
		pshield[r+1]->setName(str);
		if (0 == (_shield%5)) {
			CEGUI::UDim Uaux1=pshield[r+1]->getYPosition();
			Uaux1.d_offset-=50;
			pshield[r+1]->setYPosition(Uaux1);
			CEGUI::UDim Uaux2=pshield[0]->getXPosition();
			pshield[r+1]->setXPosition(Uaux2);
		}
		else {
			CEGUI::UDim Uaux3=pshield[r]->getXPosition();
			Uaux3.d_offset-=50;
			pshield[r+1]->setXPosition(Uaux3);
		}
		sheet->addChild(pshield[r]);
	}
}

void HUD::removeShieldGUI() {
	if (_shield == 0) {
			pshield[0]->disable();
			pshield[0]->hide();
	}
	else {
			sheet->removeChild(pshield[_shield]);
			pshield.pop_back();
	}
}

/*

void HUD::dynamicHUD () {
  dynamic_cast<CEGUI::ProgressBar*>(pbar)->setProgress(_lifebar);

  if ( _lifes > (plife.size()+1) ) {
	  std::cout << "_lifes= " << _lifes << "plife= " << plife.size() << std::endl;
	  addLifeGUI();
  }
  else if ( _lifes == (plife.size()) ) {
	  std::cout << "_lifes= " << _lifes << "plife= " << plife.size() << std::endl;
	  removeLifeGUI();
  }

  if (_shield >(pshield.size())) {
	  std::cout << "_escudos = " << _shield << " pshield= " << pshield.size() << std::endl;
	  addShieldGUI();
  }
  else if (_shield < (pshield.size()) ) {
	  std::cout << "_escudos " << _shield << "pshield= " << pshield.size() << std::endl;
	  removeShieldGUI();
  }

  // Actualización score
  pscore->setText(str);
}
*/
void HUD::destroyHUD() {
  std::cout << "en HUD::destroyHUD" << std::endl;
  // CEGUI::WindowManager::getSingleton().destroyAllWindows();
  sheet->removeChild(lay);
  CEGUI::WindowManager::getSingleton().destroyWindow(pbar);
  for (int i=0; i<_lifes; i++)
	  CEGUI::WindowManager::getSingleton().destroyWindow(plife[i]);
  for (int i=0; i<=_shield; i++)
	  CEGUI::WindowManager::getSingleton().destroyWindow(pshield[i]);
  CEGUI::WindowManager::getSingleton().destroyWindow(pscore);
  CEGUI::WindowManager::getSingleton().destroyWindow(lay);
  CEGUI::WindowManager::getSingleton().destroyWindow(sheet);
}

void HUD::setLevelLabel(std::string text) {
	CEGUI::Window * label = lay->getChild("EndLevelLabel");
	label->setText(text);
}

void HUD::setLevelLabelAlpha(float alpha) {
	CEGUI::Window * label = lay->getChild("EndLevelLabel");
	label->setAlpha(alpha);
}
