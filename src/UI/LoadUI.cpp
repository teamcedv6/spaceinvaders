#include "LoadUI.h"

//using namespace CEGUI;

using std::string;

LoadUI::LoadUI() : UserInterface() {
	 sheet=NULL; lay=NULL; progressBarWindow=NULL; progressBar=NULL;
	 std::cout << "Contructor LoadUI ... " << std::endl;
}

LoadUI::~LoadUI() {
  std::cout << "Destructor LoadUI ..." << std::endl;
  UserInterface::destroyInterface();
}

void LoadUI::destroyMenu() {
  CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void LoadUI::createMenu() {

  // Interface General
  UserInterface::createInterface();
  // Ocultar el cursor
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();

  string s1, s2;

  s1="LoadScreen.layout";
  s2="progressBar";

  // Carga del Layout
  lay=CEGUI::WindowManager::getSingleton().loadLayoutFromFile(s1);

  // Dar funcionalidad a un boton del Layout, tomar el nombre
  progressBarWindow = lay->getChild("loadWindow")->getChild(s2);//Element("loadWindow");//Child(s2);

 //ProgressBar
  progressBar = static_cast<CEGUI::ProgressBar*>(progressBarWindow);
  progressBar->setProgress(0.00f); // Initial progress of 25%
  progressBar->setStepSize(0.05f); // Calling step() will increase the progress by 10%

  // Hoja a dibujar
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(lay);
}

void LoadUI::updateProgressBar(){
	//progressBar->adjustProgress(deltaTime); // Adjust the progress by a delta value rather than setting a new value through setProgress
	float valueProgressBar = progressBar->getProgress();
	if(valueProgressBar == 1.0f) {
		progressBar->setProgress(0.00f);
	}
	progressBar->step(); // Advance the progress by the size specified in setStepSize()
}

void LoadUI::setLevelNumber(){
	CEGUI::Window* label = lay->getChild("levelLabel");
	int level =	GameManager::getSingletonPtr()->getConfiguration()->getLevelConfig()->_currentLevel;

	std::stringstream ss;
	ss << "Nivel " << level + 1;

	label->setText(String(ss.str()));
}

