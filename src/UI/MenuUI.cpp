/*
 * MenuUI.cpp
 *
 *  Created on: 22 feb. 2017
 *      Author: joe
 */

#include <States/LoadState.h>
#include "MenuUI.h"
#include <string>
#include <iostream>
#include "GameManager.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "ConfigurationState.h"
#include "AuthorState.h"
#include "ScoreBoardState.h"
#include "ScoreSystem.h"
#include "SoundManager.h"
#include "XMLManageScore.h"

//using namespace CEGUI;

using std::string;

MenuUI::MenuUI() : UserInterface() {
	 sheet=NULL; lay=NULL; button=NULL; splash=NULL; mask=0;
	 std::cout << "Contructor MenuUI ... " << std::endl;
}

void MenuUI::createMenu(const int& n) {
  // Interface General
  UserInterface::createInterface();
  // Muestra el cursor
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();


  // cambiamos valor de máscara
  mask=n;
  // Sheet general
  sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","MenuIntro/Sheet");
  string s1;
  std::vector<string> buttons;


  switch ( n ) {
    // De MainState a LevelState
  	case 1:
      s1="levelSP.layout";
      buttons.push_back("LevelButton");
      break;
    // De LevelState a PlayState
    case 2:
      s1="intro.layout";
      buttons.push_back("PlayButton");
      buttons.push_back("AuthorButton");
      buttons.push_back("ScoresButton");
      buttons.push_back("ExitButton");
      break;
    case 3:
		s1="ScoreUI.layout";
		buttons.push_back("scoresExitButton");
		break;
    case 4:
    	s1="InputUI.layout";
    	buttons.push_back("addScore");
    	break;
    case 5:
    	s1="Authors.layout";
    	buttons.push_back("ExitButton");
    	break;
    default:
      std::cout << "selección erronea\n";
      break;
  }				/* -----  end switch  ----- */

  // Carga del Layout
  lay=CEGUI::WindowManager::getSingleton().loadLayoutFromFile(s1);

  //splash=lay->getChild("SplashScreen");

  // Dar funcionalidad a un boton del Layout, tomar el nombre
  for(std::vector<string>::iterator it = buttons.begin();
		  it != buttons.end();it++){
	  button = lay->getChild(*it);
	  button->subscribeEvent(CEGUI::PushButton::EventClicked,
   			     CEGUI::Event::Subscriber(&MenuUI::cscallback,
  						      this));
  }

  //Attaching buttons
  sheet->addChild(lay);
  //sheet->addChild(splash);
  // Hoja a dibujar
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);

}

void MenuUI::destroyMenu() {
  sheet->removeChild(lay);
  //CEGUI::WindowManager::getSingleton().destroyAllWindows();
  CEGUI::WindowManager::getSingleton().destroyWindow(button);
  CEGUI::WindowManager::getSingleton().destroyWindow(lay);
  //CEGUI::WindowManager::getSingleton().destroyWindow(splash);
  CEGUI::WindowManager::getSingleton().destroyWindow(sheet);
}

bool MenuUI::cscallback(const CEGUI::EventArgs &e) {
  //std::cout << "estoy pulsando el bottón" << std::endl;
	 const CEGUI::MouseEventArgs& we =
	    static_cast<const CEGUI::MouseEventArgs&>(e);
	 CEGUI::String senderID = we.window->getName();

	 CEGUI::String name;
	 Configuration* config = GameManager::getSingletonPtr()->getConfiguration();
	 SoundManager* sound = GameManager::getSingletonPtr()->getSoundManager();
	 ScoreSystem* score;
	 XMLManageScore* xmlManageScore;


	 sound->selectResource("fx",config->getUiConfig()->_fx,"play");

  switch ( mask ) {
    case 1:

      break;

    case 2:
		if(senderID == "PlayButton") {
			LoadState::getSingletonPtr()->setType(LoadState::LOAD);
			GameManager::getSingletonPtr()->changeState(LoadState::getSingletonPtr());
		}
		else if(senderID == "AuthorButton") {
			GameManager::getSingletonPtr()->changeState(AuthorState::getSingletonPtr());
		}
		else if(senderID == "ScoresButton") {
			ScoreBoardState::getSingletonPtr()->setType(ScoreBoardState::READ);
			GameManager::getSingletonPtr()->changeState(ScoreBoardState::getSingletonPtr());
		}
		else if(senderID == "ExitButton") {
			MainMenuState::getSingletonPtr()->endGame();
		}
		break;
    case 3:
    	GameManager::getSingletonPtr()->changeState(MainMenuState::getSingletonPtr());

      break;
    case 4:

    	name = lay->getChild("Editbox")->getText();
    	score = GameManager::getSingletonPtr()->getScoreSystem();

    	xmlManageScore = new XMLManageScore();
    	xmlManageScore->saveXML(config,score,name.c_str());
    	delete xmlManageScore;

    	GameManager::getSingletonPtr()->changeState(MainMenuState::getSingletonPtr());
    	//ScoreBoardState::getSingletonPtr()->setType(ScoreBoardState::READ);
		//GameManager::getSingletonPtr()->changeState(ScoreBoardState::getSingletonPtr());
    	break;

    case 5:
    	if(senderID == "ExitButton") {
    	    GameManager::getSingletonPtr()->changeState(MainMenuState::getSingletonPtr());
    	}
      break;

    default:
      std::cout << "selección erronea\n";
      break;
  }				/* -----  end switch  ----- */

  return true;
}

MenuUI::~MenuUI() {
  std::cout << "Destructor MenuUI ..." << std::endl;
  UserInterface::destroyInterface();
}

void MenuUI::ShowScores(){

	auto lb = lay->getChild("scoresListbox");
	CEGUI::Listbox * listbox = static_cast<CEGUI::Listbox*>(lb);

	CEGUI::ListboxTextItem* itemListbox = new CEGUI::ListboxTextItem("Value A", 2);

	//listbox->clearAllSelections();
	//listbox->addItem(itemListbox);


	//CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem("640 x 480",2);

	//listbox->addItem(item);
	CEGUI::Window* root = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::Window* recordFrame;
	CEGUI::Window* name;
	CEGUI::Window* score;
	ScoreSystem * scoreSystem = GameManager::getSingletonPtr()->getScoreSystem();
	auto configuration = GameManager::getSingletonPtr()->getConfiguration();

	XMLManageScore* xmlManageScore = new XMLManageScore();
	xmlManageScore->loadXML(configuration,scoreSystem);
	delete xmlManageScore;


	std::map<int, string> scoreMap = scoreSystem->getXmlScores();

	std::map<int, string>::iterator it = scoreMap.end();
	it--;

	for(int i = 0; i < 10; i++) {

		ostringstream os1;
		os1 << i;
		recordFrame = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Frame"+os1.str());
		name = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Name"+os1.str());
		score = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score"+os1.str());

		recordFrame ->setSize(CEGUI::USize(CEGUI::UDim(0,400),CEGUI::UDim(0.0,20)));
		recordFrame ->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0, 400),CEGUI::UDim(0, 180 + (i * 35))));

		name ->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(1.0,0)));
		name ->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0,0),CEGUI::UDim(0,0)));
		name -> setText("[colour='FF00FF00'][font='SpaceInvaders']" + it->second);

		ostringstream os2;
		os2 << "" << it->first;
		score ->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(1.0,0)));
		score ->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.7,0),CEGUI::UDim(0,0)));
		score -> setText("[colour='FFFFFFFF'][font='SpaceInvaders']" + os2.str());

		recordFrame->addChild(name);
		recordFrame->addChild(score);

		root -> addChild(recordFrame);

		it--;
	}
}





