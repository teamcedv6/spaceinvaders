#include "Configuration.h"

Configuration::Configuration(){

	_optionsConfig = new Options_t;
	_uiConfig = new Ui_t;
	_enemyConfig = new Enemy_t;
	_fxConfig = new Fx_t;
	_levelConfig = new Level_t;
	//_movementConfig = new Movement_t;
	_musicConfig = new Music_t;
	_planetConfig = new Planet_t;
	_playerConfig = new Player_t;
	_shieldConfig = new Shield_t;
	_shootConfig = new Shoot_t;
	_scoreConfig = new Score_t;

}
Configuration::~Configuration (){

	delete _optionsConfig;
	delete _uiConfig;
	delete _enemyConfig;
	delete _fxConfig;
	delete _levelConfig;
	//delete _movementConfig;
	delete _musicConfig;
	delete _planetConfig;
	delete _playerConfig;
	delete _shieldConfig;
	delete _shootConfig;
	delete _scoreConfig;

}
Configuration::Configuration(const Configuration & configuration) {

	_optionsConfig = configuration._optionsConfig;
	_uiConfig = configuration._uiConfig;
	_enemyConfig = configuration._enemyConfig;
	_fxConfig = configuration._fxConfig;
	_levelConfig = configuration._levelConfig;
	//_movementConfig = configuration._movementConfig;
	_musicConfig = configuration._musicConfig;
	_planetConfig = configuration._planetConfig;
	_playerConfig = configuration._playerConfig;
	_shieldConfig = configuration._shieldConfig;
	_shootConfig = configuration._shootConfig;
	_scoreConfig = configuration._scoreConfig;

}


Configuration& Configuration::operator = (const Configuration &configuration){

	_optionsConfig = configuration._optionsConfig;
	_uiConfig = configuration._uiConfig;
	_enemyConfig = configuration._enemyConfig;
	_fxConfig = configuration._fxConfig;
	_levelConfig = configuration._levelConfig;
	//_movementConfig = configuration._movementConfig;
	_musicConfig = configuration._musicConfig;
	_planetConfig = configuration._planetConfig;
	_playerConfig = configuration._playerConfig;
	_shieldConfig = configuration._shieldConfig;
	_shootConfig = configuration._shootConfig;
	_scoreConfig = configuration._scoreConfig;

	return *this;

}

// Setters Option
void Configuration::setConfigOptionMusic(char* option){
	_optionsConfig->_music = option;
}

void Configuration::setConfigOptionFx(char* option){
	_optionsConfig->_fx = option;
}

void Configuration::setConfigOptionLoadMusic(string music){
	_optionsConfig->_loadMusic = music;
}

void Configuration::setConfigOptionLoadFx(string fx){
	_optionsConfig->_loadFx = fx;
}

// Setters Ui
void Configuration::setConfigUiMusic(string option){
	_uiConfig->_music = option;
}

void Configuration::setConfigUiFx(string option){
	_uiConfig->_fx = option;
}

// Setters Enemy
void Configuration::setConfigEnemyPath(char* path){
	_enemyConfig->_config_enemy_path = path;
}

void Configuration::setConfigEnemyType(char* path){
	_enemyConfig->_resource_type = path;
}

void Configuration::addEnemy(string index,Enemy_t::enemies_t enemy){
	(_enemyConfig->_enemies)[index] = enemy;
}


// Setters Fx
void Configuration::setConfigFxPath(char* path){
	_fxConfig->_config_fx_path = path;
}

void Configuration::setConfigFxType(char* path) {
	_fxConfig->_resource_type = path;
}

void Configuration::addFx(string index,char* fx,float volume){
	(_fxConfig->_fxs)[index] = make_tuple(index,volume,fx);
}

// Setters Level
void Configuration::setConfigLevelPath(char* path){
	_levelConfig->_config_level_path = path;
}

void Configuration::setConfigLevelType(char* path){
	_levelConfig->_resource_type = path;
}

void Configuration::setConfigLevelInit(int init){
	_levelConfig->_init_level = init;
	_levelConfig->_currentLevel = init;
}
void Configuration::addLevel(int index,Level_t::levels_t level){
	(_levelConfig->_levels)[index] = level;
}

/*// Setters Movement
void Configuration::setConfigMovementPath(char* path){
	_movementConfig->_config_movement_path = path;
}

void Configuration::setConfigMovementType(char* path){
	_movementConfig->_resource_type = path;
}

void Configuration::addMovement(string index,Movement_t::movements_t movement){
	(_movementConfig->_movements)[index] = movement;
}*/

// Setters music
void Configuration::setConfigMusicPath(char* path){
	_musicConfig->_config_music_path = path;
}

void Configuration::setConfigMusicType(char* path) {
	_musicConfig->_resource_type = path;
}

void Configuration::addMusic(string index,char* track,float volume){
	(_musicConfig->_tracks)[index] = make_tuple(index,volume,track);
}

// Setters Planet
void Configuration::setConfigPlanetPath(char* path){
	_planetConfig->_config_planet_path = path;
}

void Configuration::setConfigPlanetType(char* path){
	_planetConfig->_resource_type = path;
}

void Configuration::addPlanet(string index,Planet_t::planets_t planet){
	(_planetConfig->_planets)[index] = planet;
}

// Setters Player
void Configuration::setConfigPlayerPath(char* path){
	_playerConfig->_config_player_path = path;
}

void Configuration::setConfigPlayerType(char* path){
	_playerConfig->_resource_type = path;
}
void Configuration::addPlayer(string index,Player_t::players_t player){
	(_playerConfig->_players)[index] = player;
}

// Setters Shield
void Configuration::setConfigShieldPath(char* path){
	_shieldConfig->_config_shield_path = path;
}

void Configuration::setConfigShieldType(char* path){
	_shieldConfig->_resource_type = path;
}

void Configuration::addShield(string index,Shield_t::shields_t shield){
	(_shieldConfig->_shields)[index]=shield;
}

// Setters Shoot
void Configuration::setConfigShootPath(char* path){
	_shootConfig->_config_shoot_path = path;
}

void Configuration::setConfigShootType(char* path){
	_shootConfig->_resource_type = path;
}

void Configuration::addShoot(string index,Shoot_t::shoots_t shoot){
	_shootConfig->_shoots[index] = shoot;
}

// Setters Shoot
void Configuration::setConfigScorePath(char* path){
	_scoreConfig->_config_score_path = path;
}

void Configuration::setConfigScoreType(char* path){
	_scoreConfig->_resource_type = path;
}

// Getters
Configuration::Options_t* Configuration::getOptionConfig() {
	return _optionsConfig;
}

Configuration::Ui_t* Configuration::getUiConfig() {
	return _uiConfig;
}

Configuration::Enemy_t* Configuration::getEnemyConfig() {
	return _enemyConfig;
}

Configuration::Fx_t* Configuration::getFxConfig(){
	return _fxConfig;
}

Configuration::Level_t* Configuration::getLevelConfig() {
	return _levelConfig;
}

/*Configuration::Movement_t* Configuration::getMovementConfig() {
	return _movementConfig;
}*/

Configuration::Music_t* Configuration::getMusicConfig(){
	return _musicConfig;
}

Configuration::Planet_t* Configuration::getPlanetConfig() {
	return _planetConfig;
}

Configuration::Player_t* Configuration::getPlayerConfig() {
	return _playerConfig;
}

Configuration::Shield_t* Configuration::getShieldConfig() {
	return _shieldConfig;
}

Configuration::Shoot_t* Configuration::getShootConfig() {
	return _shootConfig;
}

Configuration::Score_t* Configuration::getScoreConfig() {
	return _scoreConfig;
}
