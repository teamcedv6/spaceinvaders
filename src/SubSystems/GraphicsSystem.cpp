/*
 * GraphicsSystem.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "GraphicsSystem.h"

GraphicsSystem::GraphicsSystem() : Observer(), _sceneManager(NULL) {}

GraphicsSystem::GraphicsSystem(Ogre::SceneManager * sceneManager):
	Observer(Event::TYPE_CREATE | Event::TYPE_INGAME | Event::TYPE_DESTROY),
	_sceneManager(sceneManager) {}


GraphicsSystem::GraphicsSystem(const GraphicsSystem & GraphicsSystem) {
	_sceneManager = NULL;
}

GraphicsSystem::~GraphicsSystem(){

}

GraphicsSystem& GraphicsSystem::operator = (const GraphicsSystem &GraphicsSystem) {
		return *this;
}

void GraphicsSystem::createNode(Ogre::SceneNode ** nodePtr, std::string * nodeName,std::string * mesh) {
	*nodePtr = _sceneManager->createSceneNode(*nodeName);
	std::stringstream ss;
	std::string meshName = nodeName->substr(2,5);
	ss << meshName << ".mesh";

	Ogre::Entity *entPtr;

	std::cout << meshName << " mesh \n";
	entPtr = _sceneManager->createEntity(*nodeName, ss.str());


	(*nodePtr)->attachObject(entPtr);
	(*nodePtr)->setPosition(0, 0, 0);
}

void GraphicsSystem::attachNode(std::string * parentName, std::string * nodeName){
	Ogre::SceneNode * parent;
	Ogre::SceneNode * node = _sceneManager->getSceneNode(*nodeName);

	if(parentName != NULL)
		parent = _sceneManager->getSceneNode(*parentName);
	else
		parent = _sceneManager->getRootSceneNode();


	parent->addChild(node);
}

void GraphicsSystem::dettachNode(const std::string & nodeName){

}

void GraphicsSystem::destroyNode(const std::string & nodeName){
	_sceneManager->destroySceneNode(nodeName);
	_sceneManager->destroyEntity(nodeName);
}

void GraphicsSystem::process(Event & event){
	if(event.getType() == Event::TYPE_CREATE){
		Event::NodeAndString params = event.getParametersNodeAndString();
		createNode(params.node, params.name, params.name);
	}
	else if(event.getType() == Event::TYPE_INGAME){
		Event::ArrayAndString params = event.getParametersArrayAndString();
		int i = 0;
		while(i < params.n){
			attachNode(params.parent, *(params.names + i));
			i++;
		}
	}
	else if(event.getType() == Event::TYPE_DESTROY){
		const std::string & name = event.getParametersString();
		std::cout << "destroy " << name << "\n";
		dettachNode(name);
		destroyNode(name);
	}
}
