/*
 * ScoreSystem.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "ScoreSystem.h"


ScoreSystem::ScoreSystem():
	Observer(Event::TYPE_DEAD), _playerName("noname"), _hud(NULL), _currentScore(0) {}


ScoreSystem::ScoreSystem(const ScoreSystem & scoreSystem) : Observer(scoreSystem){
	_playerName = scoreSystem._playerName;
	_hud = scoreSystem._hud;
	_currentScore = scoreSystem._currentScore;
}

ScoreSystem::~ScoreSystem(){

}


ScoreSystem& ScoreSystem::operator = (const ScoreSystem & scoreSystem) {
	Observer::operator =(scoreSystem);
	_playerName = scoreSystem._playerName;
	_hud = scoreSystem._hud;
	_currentScore = scoreSystem._currentScore;
	return *this;
}

void ScoreSystem::process(Event & event){

	if(event.getType() == Event::TYPE_DEAD){
		auto name= event.getParametersString();
		std::string str2 = name.substr (2,5);
		if(name.at(0) == 'e') {
			addScore(str2 );
		}
	}
}

void ScoreSystem::addEnemyScore(std::string name, int score) {
	_enemiesScores[name] = score;
}

void ScoreSystem::setPlayerName(std::string playerName){
	_playerName = playerName;
}

void ScoreSystem::setHUD(HUD * hud){
	_hud = hud;
}

void ScoreSystem::addScore(int score) {
	_currentScore += score;
	_hud->addScore(score);
}

void ScoreSystem::addScore(std::string name) {
	int score = _enemiesScores[name];
	_currentScore += score;
	_hud->addScore(score);
}

void ScoreSystem::saveScore() {
	// Save score in xml
}

void ScoreSystem::clear() {
	_playerName = "noname";
	_enemiesScores.clear();
	_hud = NULL;
	_currentScore = 0;
}

void ScoreSystem::setXmlScores(map<int,string> xmlScores) {
	_xmlScores.clear();
	_xmlScores = xmlScores;
}

map<int,string> ScoreSystem::getXmlScores() {
	return _xmlScores;
}

int ScoreSystem::getCurrentScore() {
	return _currentScore;
}




