/*
 * CollisionSystem.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "CollisionSystem.h"

CollisionSystem::CollisionSystem() : Observer(), Observable(1, Event::TYPE_COLLISION), _sceneManager(NULL), _L(0), _r(0) {}

CollisionSystem::CollisionSystem(Ogre::SceneManager * sceneManager):
	Observer(Event::TYPE_INGAME | Event::TYPE_MOVE | Event::TYPE_DESTROY),
	Observable(1, Event::TYPE_COLLISION),
	_sceneManager(sceneManager), _L(0), _r(0){}


CollisionSystem::CollisionSystem(const CollisionSystem & CollisionSystem) {
	_sceneManager = NULL;
	_L = 0;
	_r = 0;
}

CollisionSystem::~CollisionSystem(){

}

void CollisionSystem::setRadius(int radius) {
	_r = radius;
	_L = 2 * M_PI * _r;
}

CollisionSystem& CollisionSystem::operator = (const CollisionSystem &CollisionSystem) {
		return *this;
}

/**
 * In the example illustrated to the right (or above in some mobile versions) ,
 * a particle on object P is at a fixed distance r from the origin, O, rotating
 *  counterclockwise. It becomes important to then represent the position of
 *  particle P in terms of its polar coordinates (r, θ).
 *   In this particular example, the value of θ is changing, while the value of
 *   the radius remains the same. (In rectangular coordinates (x, y) both x and y
 *   vary with time). As the particle moves along the circle, it travels an arc length s,
 *   which becomes related to the angular position through the relationship:
 *  s = r θ
 */

void CollisionSystem::registerCollisionReceiver(std::string * name) {
	assert(_r != 0);

	Ogre::SceneNode * node = _sceneManager->getSceneNode(*name);
	BoundingRectangle br;
	initBoundingParameters(name, br);
	updateBoundingParameters(br, node);

	_collidable.push_back(br);
}

void CollisionSystem::registerCollisionProvider(std::string * name){
	assert(_r != 0);

	Ogre::SceneNode * node = _sceneManager->getSceneNode(*name);
	BoundingRectangle br;
	initBoundingParameters(name, br);
	updateBoundingParameters(br, node);

	_colliders.push_back(br);

}

void CollisionSystem::initBoundingParameters(std::string * name, BoundingRectangle & br) {
	std::pair<float, float> boundings = _sizes[name->substr(2,5)];

	std::cout << "name: " << name->substr(2,5) << "\n";
 	std::cout << "w : " << boundings.first << " -- l: "<< boundings.second << " \n";

	br.name = *name;
	br.x = 0;
	br.xMax = 0;
	br.xMin = 0;
	br.y = 0;
	br.yMax = 0;
	br.yMin = 0;
	br.width  = boundings.first;
	br.height = boundings.second;


}

void CollisionSystem::updateBoundingParameters(BoundingRectangle & br, Ogre::SceneNode * node) {
	const Ogre::Vector3 & pos = node->getPosition();
	double div = std::abs(asin(pos.z/_r));
	double angle = 0.0;

	if(div != 0) angle = acos(pos.x/_r) * (asin(pos.z/_r)/std::abs(asin(pos.z/_r)));
	else angle = acos(pos.x/_r);

	if(angle > 2 * M_PI) angle -= 2 * M_PI;
	if(angle < 0) angle += 2 * M_PI;

	br.x = angle * _r;
	br.xMax = br.x + br.height;
	br.xMin = br.x - br.width;
	br.y = pos.y;
	br.yMax = br.y + br.height;
	br.yMin = br.y - br.width;

	//std::cout << "bounding at " << angle << " -- div = " << div << "-- name = " << *br.name  << "\n"; //"-- x =" << br.x << " -- y = " << br.y << "\n";
}


void CollisionSystem::process(Event & event){

	if(event.getType() == Event::TYPE_INGAME){
		Event::ArrayAndString & params = event.getParametersArrayAndString();
		std::string * name;

		for(int i = 0; i < params.n; i++){
			name = *(params.names + i);

			if(name->at(8) == 's') {
				registerCollisionProvider(name);
			}
			else if(name->at(8) == 'r') {
				registerCollisionReceiver(name);
			}
		}
	}
	else if(event.getType() == Event::TYPE_MOVE){
		bool found = false;
		BoundingRectangle * br = NULL;
		std::vector<BoundingRectangle>::iterator it = _colliders.begin();
		Event::NodeAndString & params = event.getParametersNodeAndString();

		while(!found && it != _colliders.end()) {
			if((*it).name == *(params.name)) {
				found = true;
				br = &(*it);
			}
			it++;
		}

		it = _collidable.begin();
		while (!found && it != _collidable.end()) {
			if((*it).name == *(params.name)) {
				found = true;
				br = &(*it);
			}
			it++;
		}

		if(br != NULL) {
			Ogre::SceneNode * node = *params.node;
			updateBoundingParameters(*br, node);
		}
	}
	else if(event.getType() == Event::TYPE_DESTROY){
		bool found = false;



		std::vector<BoundingRectangle>::iterator it = _colliders.begin();
		std::string & name = event.getParametersString();

		while(!found && it != _colliders.end()) {
			if((*it).name == name) {
				found = true;
				_colliders.erase(it);
			}
			it++;
		}

		it = _collidable.begin();
		while (!found && it != _collidable.end()) {
			if((*it).name == name) {
				found = true;
				_collidable.erase(it);
			}
			it++;
		}

	}

}

void CollisionSystem::addCollisionShape(std::string name, float width, float height) {
	_sizes[name] = std::pair<float, float>(width/2.0, height/2.0);
}

void CollisionSystem::update() {
	std::vector<BoundingRectangle>::reverse_iterator it = _colliders.rbegin();

	bool collision = false;
	while(it != _colliders.rend()) {
		std::vector<BoundingRectangle>::reverse_iterator it2 = _collidable.rbegin();
		while(!collision && it2 != _collidable.rend()) {

			collision = false;
			if((*it).xMax > _L) {
				collision |= collide((*it).xMin - _L, (*it).xMax - _L,  (*it).yMin,  (*it).yMin,
						             (*it2).xMin,     (*it2).xMax,     (*it2).yMin, (*it2).yMax);
			}

			//std::cout << (*it).name << ": " << (*it).xMin << " " << (*it).xMax << " " << (*it).yMin << " " << (*it).yMax << " " <<
				//(*it2).name << ": " << (*it2).xMin << " " << (*it2).xMax << " " << (*it2).yMin << " " << (*it2).yMax << "\n";

			collision |=  collide( (*it).xMin,  (*it).xMax,  (*it).yMin,  (*it).yMin,
					              (*it2).xMin, (*it2).xMax, (*it2).yMin, (*it2).yMax);

			if((*it).xMin < 0) {
				collision |=  collide((*it).xMin + _L, (*it).xMax + _L,  (*it).yMin,  (*it).yMin,
					                  (*it2).xMin,     (*it2).xMax,     (*it2).yMin, (*it2).yMax) ;
			}

			if(collision) {
				std::cout << "COLLISION -> " << (*it).name << " -- " << (*it2).name << "********\n";
				_event.setType(Event::TYPE_COLLISION);
				_event.setParameters(&(*it).name, &(*it2).name);

				send(_event);
			}
			it2++;
		}
		it++;
	}
}

bool CollisionSystem::collide(float x1Min, float x1Max, float y1Min, float y1Max,
							  float x2Min, float x2Max, float y2Min, float y2Max) {
	if(x1Max > x2Min &&
	   y1Max > y2Min &&
	   x2Max > x1Min &&
	   y2Max > y1Min){
		return true;
	}
	else{
		return false;
	}

}

void CollisionSystem::clear() {
	_collidable.clear();
	_colliders.clear();
}
