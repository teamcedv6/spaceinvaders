/*
 * LogSystem.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "LogSystem.h"

LogSystem::LogSystem() : Observer(), _logLevel(0) {}

LogSystem::LogSystem(int logLevel):
	Observer(Event::TYPE_ADDPOINTS | Event::TYPE_COLLISION |
			Event::TYPE_CREATE | Event::TYPE_INGAME | Event::TYPE_DESTROY),
	_logLevel(_logLevel){}


LogSystem::LogSystem(const LogSystem & logSystem) {
	_logLevel = NULL;
}

LogSystem::~LogSystem(){

}

LogSystem& LogSystem::operator = (const LogSystem &logSystem) {
	_logLevel = logSystem._logLevel;
	return *this;
}

void LogSystem::writeLog(const std::string & str) {
	std::cout << str << "\n";
}

void LogSystem::process(Event & event){
	if(_logLevel > 0) {
		//writeLog(event.getName());
	}
	if(_logLevel >= 4){
		if(event.getType() == Event::TYPE_CREATE){
			Event::NodeAndString & params = event.getParametersNodeAndString();
			writeLog(*params.name);
		}
		else if(event.getType() == Event::TYPE_COLLISION){
			Event::DoubleString & params = event.getParametersDoubleString();
			writeLog("Collision: " + *(params.name1) + " -- " + *(params.name2));
		}
	}
}
