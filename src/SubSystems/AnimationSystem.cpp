/*
 * AnimationSystem.cpp
 *
 *  Created on: Feb 5, 2017
 *      Author: root
 */
#include "AnimationSystem.h"

AnimationSystem::AnimationSystem() : Observer(), _sceneManager(NULL) {}

AnimationSystem::AnimationSystem(Ogre::SceneManager * sceneManager):
	Observer(Event::TYPE_INGAME | Event::TYPE_DESTROY | Event::TYPE_DEAD),
	_sceneManager(sceneManager) {}


AnimationSystem::AnimationSystem(const AnimationSystem & animationSystem) {
	_sceneManager = NULL;
}

AnimationSystem::~AnimationSystem(){

}


AnimationSystem& AnimationSystem::operator = (const AnimationSystem & animationSystem) {
		return *this;
}

void AnimationSystem::process(Event & event){

	if(event.getType() == Event::TYPE_INGAME){
/*
		Event::ArrayAndString & params = event.getParametersArrayAndString();
		std::string * name;
		const std::string & animationName = event.getName();
		Ogre::AnimationState * animationState;

		for(int i = 0; i < params.n; i++){
			name = *(params.names + i);

			Ogre::Entity * entity = _sceneManager->getEntity(*name);
			if(entity->hasAnimationState(animationName)) {
				animationState = entity->getAnimationState(animationName);
				animationState->setEnabled(true);
				animationState->setLoop(true);
				animationState->setTimePosition(0.0);
				_animations.push_back(animationState);
			}
		}*/
	}
	else if(event.getType() == Event::TYPE_DESTROY){
		/*bool found = false;
		std::string & name = event.getParametersString();

		Ogre::Entity * ent = _sceneManager->getEntity(name);
		Ogre::AnimationStateSet * allAnimations = ent->getAllAnimationStates();
		Ogre::AnimationStateIterator animationIterator = allAnimations->getAnimationStateIterator();
		std::vector<Ogre::AnimationState*>::iterator it;
		Ogre::AnimationState * animationState;

		while(animationIterator.hasMoreElements()) {
			animationState = animationIterator.getNext();
			 it = _animations.begin();

			found = false;

			while(!found && it != _animations.end()) {
				if((*it) == animationState) {
					found = true;
					_animations.erase(it);
				}
				it++;
			}

		}*/

	}
	else if(event.getType() == Event::TYPE_DEAD) {
		std::string & name = event.getParametersString();
		Ogre::SceneNode * node = _sceneManager->getSceneNode(name);
		Ogre::AnimationState * animationState;

		std::stringstream ss;
		ss << "a_anima_";
		if(_deadNumber < 100) ss << "0";
		if(_deadNumber < 10)  ss << "0";
		ss << _deadNumber;
		_deadNumber = (_deadNumber + 1) % 1000;

		Ogre::SceneNode * deadNode = _sceneManager->createSceneNode(ss.str());
		Ogre::Entity * deadEnt = _sceneManager->createEntity(ss.str(), "dead.mesh");

		deadNode->attachObject(deadEnt);
		deadNode->setPosition(node->getPosition());
		deadNode->setOrientation(node->getOrientation());

		_sceneManager->getRootSceneNode()->addChild(deadNode);

		animationState = deadEnt->getAnimationState("explosion");
		animationState->setTimePosition(0.0);
		animationState->setEnabled(true);
		animationState->setLoop(false);

		_deadNodes.push_back(deadNode);
		_deadAnimations.push_back(animationState);
	}
}

void AnimationSystem::update(Ogre::Real deltaTime) {


	std::vector<Ogre::AnimationState*>::iterator it = _animations.begin();
	while(it != _animations.end()) {
		(*it)->addTime(deltaTime);
		if((*it)->hasEnded()) {
			_animations.erase(it);
		}
		else {
			it++;
		}
	}

	int i = 0;
	it = _deadAnimations.begin();
	while(it != _deadAnimations.end()) {
		(*it)->addTime(deltaTime);
		if((*it)->hasEnded()) {
			_deadAnimations.erase(it);
			Ogre::SceneNode * node = _deadNodes.at(i);
			_deadNodes.erase(_deadNodes.begin() + i);
			const std::string name = node->getName();

			_sceneManager->getRootSceneNode()->removeAndDestroyChild(name);
			_sceneManager->destroyEntity(name);
		}
		else {
			i++;
			it++;
		}
	}
}

void AnimationSystem::clear() {
	_animations.clear();
	_deadAnimations.clear();

	std::vector<Ogre::SceneNode *>::iterator it2 = _deadNodes.begin();

	while(it2 != _deadNodes.end()) {
		Ogre::SceneNode * node = *it2;
		it2 = _deadNodes.erase(it2);
		const std::string name = node->getName();
		_sceneManager->getRootSceneNode()->removeAndDestroyChild(name);
		_sceneManager->destroyEntity(name);
	}

}



