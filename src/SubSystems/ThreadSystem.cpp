#include <ThreadSystem.h>

using namespace xercesc;

ThreadSystem::ThreadSystem
(int type,const IceUtil::Time& delay):
  _type(type),_delay(delay)
{

}

ThreadSystem::ThreadSystem
(int type):
  _type(type)
{

}

void
ThreadSystem::run () {
	_gameManager = GameManager::getSingletonPtr();
	switch(_type){
		case CONFIG:
			cout << "### Load basic configuraton ###" << endl;
			basicConfigLoad();
			cout << "### Basic configuration loaded ###" << endl;
		break;
		case LOAD:
			cout << "### level building ###" << endl;
			loadLevel();
			cout << "### level build ###" << endl;
		break;
		case DELAY:
			cout << "### Waiting Thread ###" << endl;
			IceUtil::ThreadControl::sleep(_delay);
			cout << "### Waiting End ###" << endl;
		break;
		case TEST:
			cout << "### Load basic configuraton && level building ###" << endl;
			basicConfigLoad();
			loadLevel();
			cout << "### Waiting End && level build ###" << endl;
		break;
	}
}

void
ThreadSystem::basicConfigLoad () {

	// config.xml
	XMLManageConfig* xmlManageConfig = new XMLManageConfig();
	xmlManageConfig->loadXML(_gameManager->getConfiguration());
	delete xmlManageConfig;

	// music.xml & fx.xml
	XMLManageSound* xmlManageSound = new XMLManageSound();
	xmlManageSound->loadXML(_gameManager->getConfiguration());
	delete xmlManageSound;

	// Init sound
	_gameManager->getSoundManager()->start(_gameManager->getConfiguration());

	auto music = _gameManager->getConfiguration()->getOptionConfig();

	_gameManager->getSoundManager()->selectResource("music",music->_loadMusic,"play");

	// enemy.xml
	XMLManageEnemy* xmlManageEnemy = new XMLManageEnemy();
	xmlManageEnemy->loadXML(_gameManager->getConfiguration(),_gameManager->getSoundManager());
	delete xmlManageEnemy;

	// planer.xml
	XMLManagePlayer* xmlManagePlayer = new XMLManagePlayer();
	xmlManagePlayer->loadXML(_gameManager->getConfiguration(),_gameManager->getSoundManager());
	delete xmlManagePlayer;

	// shoot.xml
	XMLManageShoot* xmlManageShoot = new XMLManageShoot();
	xmlManageShoot->loadXML(_gameManager->getConfiguration(),_gameManager->getSoundManager());
	delete xmlManageShoot;


	// level.xml
	XMLManageLevel* xmlManageLevel = new XMLManageLevel();
	xmlManageLevel->loadXML(_gameManager->getConfiguration());
	delete xmlManageLevel;


	/*// move.xml
	XMLManageMovement xmlManageMovement;
	xmlManageMovement.loadXML(gameManager->getConfiguration());*/


	// planet.xml
	XMLManagePlanet* xmlManagePlanet = new XMLManagePlanet();
	xmlManagePlanet->loadXML(_gameManager->getConfiguration());
	delete xmlManagePlanet;


	// shiel.xml
	XMLManageShield* xmlManageShield = new XMLManageShield();
	xmlManageShield->loadXML(_gameManager->getConfiguration());
	delete xmlManageShield;


	// Crear modelos de enemigo
	Spawner * spawner = _gameManager->getSpawner();
	Configuration::Enemy_t * enemiesConfig = _gameManager->getConfiguration()->getEnemyConfig();
	CollisionSystem * collisions = _gameManager->getCollisionSystem();
	ScoreSystem * scores = _gameManager->getScoreSystem();

	// score.xml
	/*XMLManageScore* xmlManageScore = new XMLManageScore();
	xmlManageScore->loadXML(_gameManager->getConfiguration(),scores);
	delete xmlManageScore;*/

	for(std::map<string,Configuration::Enemy_t::enemies_t>::iterator it =
		enemiesConfig->_enemies.begin();
		it != enemiesConfig->_enemies.end(); it++){
		//Configuration::Enemy_t::enemies_t e : enemiesConfig->_enemies) {
		Configuration::Enemy_t::enemies_t e = it->second;
		spawner->addEnemyModel(e);
		collisions->addCollisionShape(e._name, e._length, e._height);
		scores->addEnemyScore(e._name, e._points);
	}


	Configuration::Player_t * playerConfig = _gameManager->getConfiguration()->getPlayerConfig();
	auto player = playerConfig->_players["gonza"];
	std::stringstream pn;
	pn << "p_" << player._name << "_r";
	auto life = player._life;
	// Sería con objeto escudo auto shield = player._shield;
	auto shield = 0;
	auto shootDealy = 0.5;

	auto playerShip = new Player(pn.str(),(int)life, shield, shootDealy);
	playerShip->setMaxVelocity(player._speed);

	collisions->addCollisionShape("shoot", 0.086, 0.695);
	collisions->addCollisionShape(player._name,1.251, 0.771);

	// Agregar playstate a listeners
	_gameManager->add(PlayState::getSingletonPtr());
	_gameManager->addListeners(playerShip);
	_gameManager->addListeners(collisions);

	PlayState::getSingletonPtr()->initPlayer(playerShip);

}

void
ThreadSystem::loadLevel() {
	// TODO Buscar otra solución para esto

	auto music = _gameManager->getConfiguration()->getOptionConfig();

	_gameManager->getSoundManager()->selectResource("music",music->_loadMusic,"play");


	LevelCreator levelCreator;
	levelCreator.createLevel(_gameManager->getConfiguration());

}

