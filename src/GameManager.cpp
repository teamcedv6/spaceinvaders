#include <Ogre.h>

#include "GameManager.h"
#include "GameState.h"

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;

GameManager::GameManager ()
{
  _root = 0;
}

GameManager::~GameManager ()
{
  while (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  if (_root)
    delete _root;
}

void
GameManager::start
(GameState* state)
{
  // Creación del objeto Ogre::Root.
  _root = new Ogre::Root();
  _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");

  loadResources();

  if (!configure())
    return;    

  _inputMgr = new InputManager;
  _inputMgr-> initialise(_renderWindow);

  // Registro como key y mouse listener...
  _inputMgr->addKeyListener(this, "GameManager");
  _inputMgr->addMouseListener(this, "GameManager");

  _animations = new AnimationSystem(_sceneManager);
  _graphics = new GraphicsSystem(_sceneManager);
  _logs = new LogSystem(5);
  _collisions = new CollisionSystem(_sceneManager);
  _scores = new ScoreSystem();

  // El GameManager es un FrameListener.
  _root->addFrameListener(this);

  // Inicializamos CEGUI
  initializeCEGUI();

  _configuration = new Configuration;
  _soundManager = new SoundManager;
  _spawner = new Spawner;

	/**
	* CameraManager
	*/

  _sceneManager->setSkyBox(true,"SkyBox",900,true);
  // Para destruirlo _sceneMgr->setSkyBox(false,"SkyBox");



	Ogre::Camera *_camera = _sceneManager->createCamera("IntroCamera");
	_camera->setAspectRatio(1.77);
	Ogre::Viewport * _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
	_viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
	//InputManager::getSingletonPtr()->getMouse()->getMouseState().X.

	_camera->setPosition(0, 0, 20);
	_camera->lookAt(0.000001, 0.0, 0.0);
	_camera->setFOVy(Ogre::Radian(Ogre::Degree(70.0)));
	_camera->setNearClipDistance(0.1);
	_camera->setFarClipDistance(300.0);

	/* Creamos un punto de luz
	SceneNode* lightNode = _sceneManager->createSceneNode("lightNode");
	_sceneManager->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
	Light* lightEntity = _sceneManager->createLight("lightEntity");
	lightEntity->setType(Light::LT_SPOTLIGHT);
	lightEntity->setPosition(11.27606, 98.2641, 64.05135);
	lightEntity->setDirection(Vector3(0.000001, 34.0, 0.0));
	lightEntity->setSpotlightInnerAngle(Degree(5.0));
	lightEntity->setSpotlightOuterAngle(Degree(55.0));
	lightEntity->setSpotlightFalloff(0.0f);
	lightNode->attachObject(lightEntity);
	_sceneManager->getRootSceneNode()->addChild(lightNode);*/

	Light* light = _sceneManager->createLight("Light1");
	light->setType(Light::LT_DIRECTIONAL);
	light->setDirection(Vector3(-1,0,0));


	Light* light2 = _sceneManager->createLight("Light2");
	light2->setType(Light::LT_POINT);
	light2->setPosition(37.66144, 71.91479, 2.49251);
	light2->setSpecularColour(0.9, 0.9, 0.9);
	light2->setDiffuseColour(0.9, 0.9, 0.9);

	Light* light3 = _sceneManager->createLight("Light3");
	light3->setType(Light::LT_POINT);
	light3->setPosition(-37.66144, 21, 2.49251);
	light3->setSpecularColour(0.3, 0.3, 0.3);
	light3->setDiffuseColour(0.3, 0.3, 0.3);

	// Creamos una luz de ambiente
	_sceneManager->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
	_sceneManager->setAmbientLight(Ogre::ColourValue(0.1, 0.1, 0.1));
  /**
   * End CameraManager
   */
	//_sceneManager->setSkyBox(true, "Examples/SpaceSkyBox");
  // Transición al estado inicial.
  changeState(state);

  // Bucle de rendering.
  _root->startRendering();
}

void
GameManager::changeState
(GameState* state)
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    // exit() sobre el último estado.
    _states.top()->exit();
    // Elimina el último estado.
    _states.pop();
  }

  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::pushState
(GameState* state)
{
  // Pausa del estado actual.
  if (!_states.empty())
    _states.top()->pause();
  
  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::popState ()
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  // Vuelta al estado anterior.
  if (!_states.empty())
    _states.top()->resume();
}

void
GameManager::loadResources ()
{
  Ogre::ConfigFile cf;
  cf.load("resources.cfg");
  
  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);	
    }
  }
}

bool
GameManager::configure ()
{
  if (!_root->restoreConfig()) {
    if (!_root->showConfigDialog()) {
      return false;
    }
  }
  _renderWindow = _root->initialise(true, "Space Invaders - UCLM CEDV 6");
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

  return true;
}

GameManager*
GameManager::getSingletonPtr ()
{
  return msSingleton;
}

GameManager&
GameManager::getSingleton ()
{  
  assert(msSingleton);
  return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.
bool
GameManager::frameStarted
(const Ogre::FrameEvent& evt)
{
  _inputMgr->capture();
  return _states.top()->frameStarted(evt);
}

bool
GameManager::frameEnded
(const Ogre::FrameEvent& evt)
{
  return _states.top()->frameEnded(evt);
}

void
GameManager::addListeners(Observable * observable)
{
	if(observable->getType() & _animations->getFilter()) {
			observable->add(_animations);
	}
	if(observable->getType() & _graphics->getFilter()){
		observable->add(_graphics);
	}
	if(observable->getType() & _logs->getFilter()) {
		observable->add(_logs);
	}
	if(observable->getType() & _collisions->getFilter()) {
		observable->add(_collisions);
	}

	if(observable->getType() & _soundManager->getFilter()) {
		observable->add(_soundManager);
	}

	if(observable->getType() & _scores->getFilter()) {
			observable->add(_scores);
	}

	for(std::vector<Observer*>::iterator it = _observers.begin(); it != _observers.end(); it++){
		observable->add(*it);
	}
}

void
GameManager::add(Observer * observer) {
	_observers.push_back(observer);
}

CollisionSystem *
GameManager::getCollisionSystem() {
	return _collisions;
}

ScoreSystem *
GameManager::getScoreSystem() {
	return _scores;
}

AnimationSystem *
GameManager::getAnimationSystem() {
	return _animations;
}

Spawner *
GameManager::getSpawner() {
	return _spawner;
}

bool
GameManager::keyPressed 
(const OIS::KeyEvent &e)
{
  _states.top()->keyPressed(e);
  return true;
}

bool
GameManager::keyReleased
(const OIS::KeyEvent &e)
{
  _states.top()->keyReleased(e);
  return true;
}

bool
GameManager::mouseMoved 
(const OIS::MouseEvent &e)
{
  _states.top()->mouseMoved(e);
  return true;
}

bool
GameManager::mousePressed 
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mousePressed(e, id);
  return true;
}

bool
GameManager::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mouseReleased(e, id);
  return true;
}

Configuration* GameManager::getConfiguration(){
	return _configuration;
}

SoundManager* GameManager::getSoundManager(){
	return _soundManager;
}

void GameManager::initializeCEGUI() {
  CEGUI::OgreRenderer::bootstrapSystem();

  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  // CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
  // CEGUI::SchemeManager::getSingleton().createFromFile("HUDDemo.scheme");
  // CEGUI::SchemeManager::getSingleton().createFromFile("SpellByte.scheme");
  // CEGUI::SchemeManager::getSingleton().createFromFile("Generic.scheme");
  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  // CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
  // CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("GreatVibes-22");
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

  // Let's make the OS and the CEGUI cursor be in the same place
  CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);
}
