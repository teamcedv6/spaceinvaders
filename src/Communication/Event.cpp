/*
 * Event.cpp
 *
 *  Created on: Feb 1, 2017
 *      Author: root
 */

#include "Event.h"
#include "Observable.h"

Event::Event(){
	_type = TYPE_MOVE;
	_argType = 1;
	_parameters.onlyInteger = 0;
	_name = "move";
}

Event::Event(const Event & event) {
	this -> _type = event._type;
	this -> _parameters = event._parameters;
	this -> _argType = event._argType;
}

Event::~Event() { }

Event& Event::operator = (const Event &t) {
	this -> _type = t._type;
	this -> _parameters = t._parameters;
	this -> _argType = t._argType;
	return *this;
}

const EventType_t Event::getType() const {
	return _type;
}

const std::string & Event::getName() const {
	return _name;
}

void Event::setType(EventType_t type) {
	_type = type;

	switch(_type){
	case TYPE_ADDPOINTS:
		_name = "addpoints";
		break;
	case TYPE_CREATE:
		_name = "create";
		break;
	case TYPE_DESTROY:
		_name = "destroy";
		break;
	case TYPE_HIT:
		_name = "hit";
		break;
	case TYPE_DEAD:
		_name = "dead";
		break;
	case TYPE_INGAME:
		_name = "ingame";
		break;
	case TYPE_ITEMCHANGE:
		_name = "itemchange";
		break;
	case TYPE_ITEMPUSH:
		_name = "itempush";
		break;
	case TYPE_MOVE:
		_name = "move";
		break;
	case TYPE_SHOOT:
		_name = "shoot";
		break;
	}
}


void Event::setParameters(int param1) {
	_argType = 0;
	_parameters.onlyInteger = param1;
}

void Event::setParameters(std::string * string) {
	_argType = 1;
	_parameters.onlyString = string;
}

void Event::setParameters(std::string * parent, std::string * names[], int n){
	_argType = 2;
	_parameters.arrayAndString.parent = parent;
	_parameters.arrayAndString.names = names;
	_parameters.arrayAndString.n = n;
}

void Event::setParameters(Ogre::SceneNode** node, std::string * name) {
	_argType = 3;
	_parameters.nodeAndString.node = node;
	_parameters.nodeAndString.name = name;
}

void Event::setParameters(std::string * name1, std::string * name2) {
	_argType = 4;
	_parameters.doubleString.name1 = name1;
	_parameters.doubleString.name2 = name2;
}

int Event::getParametersInt(){
	assert(_argType == 0);
	return _parameters.onlyInteger;
}

std::string& Event::getParametersString() {
	assert(_argType == 1);
	return *_parameters.onlyString;
}

Event::ArrayAndString& Event::getParametersArrayAndString(){
	assert(_argType == 2);
	return _parameters.arrayAndString;
}

Event::NodeAndString& Event::getParametersNodeAndString(){
	assert(_argType == 3);
	return _parameters.nodeAndString;
}

Event::DoubleString& Event::getParametersDoubleString() {
	assert(_argType == 4);
	return _parameters.doubleString;
}
