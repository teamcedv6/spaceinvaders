/*
 * Observer.cpp
 *
 *  Created on: Feb 1, 2017
 *      Author: root
 */

#include "Observer.h"

Observer::Observer() {
	_filter = Event::TYPE_MOVE;
}

Observer::Observer(EventType_t filter) : _filter(filter) {}

Observer::Observer(const Observer& observer) {
	_filter = observer._filter;
}

Observer::~Observer() {}

Observer& Observer::operator = (const Observer &observer) {
	_filter = observer._filter;

	return *this;
}

void Observer::receive(Event &event){
	if((event.getType() & _filter) > 0){
		process(event);
	}
}

const EventType_t	 Observer::getFilter() const{
	return _filter;
}

