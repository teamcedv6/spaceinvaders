/*
 * Observable.cpp
 *
 *  Created on: Feb 1, 2017
 *      Author: root
 */

#include "Observable.h"

Observable::Observable() {
	_observers = std::vector<Observer*>();
	_observers.reserve(4);
	_type = Event::TYPE_MOVE;
}

Observable::Observable(int expectedObservers, EventType_t type) {
	_observers = std::vector<Observer*>();
	_observers.reserve(expectedObservers);
	_type = type;
}

Observable::Observable(const Observable & observable) {
	_observers = observable._observers;
	_event = observable._event;
	_type = observable._type;
}

Observable::~Observable() {

}

Observable& Observable::operator = (const Observable &observable){
	_observers = observable._observers;
	_event = observable._event;
	_type = observable._type;
	return *this;
}

void Observable::send(Event &event) {
	std::vector<Observer*>::iterator it = _observers.begin();

	while(it != _observers.end()){
		(*it)->receive(event);
		it++;
	}
}

EventType_t Observable::getType() {
	return _type;
}

void Observable::add(Observer * observer) {
	_observers.push_back(observer);
}

void Observable::remove(Observer * observer) {
	std::vector<Observer*>::iterator it = _observers.begin();
	bool deleted = false;

	while(!deleted && it != _observers.end()) {
		if(*it == observer){
			_observers.erase(it);
			deleted = true;
		}
		it++;
	}
}

Event& Observable::getEvent(){
	return _event;
}
