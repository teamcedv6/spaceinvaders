#include "TestState.h"

template<> TestState* Ogre::Singleton<TestState>::msSingleton = 0;

#include <iostream>

void
TestState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");

  	nodeShip = _sceneMgr->createSceneNode("IntroScoreboard");
  	Entity *entShip = _sceneMgr->createEntity("IntroScoreboard", "Cube.mesh");
  	nodeShip->attachObject(entShip);

  	SceneNode *nodeShip2 = _sceneMgr->createSceneNode("IntroScoreboard2");
  	Entity *entShip2 = _sceneMgr->createEntity("IntroScoreboard2", "Cube.mesh");
  	nodeShip2->attachObject(entShip2);
  	nodeShip2->setPosition(0, 0, 0);

  	nodeShip->setPosition(0, 0, 10);
  	nodeShip->yaw(Radian(Degree(90)));
  	_sceneMgr-> getRootSceneNode() -> addChild(nodeShip);
  	_sceneMgr-> getRootSceneNode() -> addChild(nodeShip2);

  	_camera->setPosition(0, 0, 15);

  	/**
	*
	* Inicio hilos
	*/

	_notEnd = true;

	//_loadConfigThread = new ThreadSystem(0);
	_createLevelThread = new ThreadSystem(3);
	_waitThread = new ThreadSystem(2,IceUtil::Time::seconds(2));

	//IceUtil::ThreadControl tc1 = _loadConfigThread->start();
	IceUtil::ThreadControl tc2 = _createLevelThread->start();
	IceUtil::ThreadControl tc3 = _waitThread->start();

	//tc1.detach();
	tc2.detach();
	tc3.detach();

	/**
	* Fin prueba hilos
	*/



  	r = 10.0;
  	pR = 0.0;
  	wR = 0.0;
  	cR = 0.0;


  	_exitGame = false;
}

void
TestState::exit ()
{
}

void
TestState::pause()
{
}

void
TestState::resume()
{

}

void
TestState::score()
{
}


bool
TestState::frameStarted
(const Ogre::FrameEvent& evt)
{
	/**
	 * Thread test
	*/

	if(!_createLevelThread->isAlive()){
	  //cout << "vivo" << endl;
	  if(!_waitThread->isAlive() && _notEnd){
		 //cout << "entro solamente una vez" << endl;
		 //_notEnd = false;
		 //_menu->createInterface(2);
	  }
	}

/*
	 * End Thread test
	 */

	Real dt = evt.timeSinceLastFrame;
	//pR += wR * dt;
	Vector3 v3(0, 0, 0);
	pR += wR * dt;


	double a = wR * wR * r;
	double v = wR * r * 0.1;
	double vx = a * cos(pR);
	double vy = a * sin(pR);
	nodeShip->setPosition(r * cos(pR), 0, r * sin(pR));
	//nodeShip->lookAt(v3, Node::TS_WORLD);//(0, 0, 0);
	nodeShip->yaw(-Radian(wR * dt), Node::TS_LOCAL);
	//nodeShip->translate(vx * dt, 0, vy * dt);

	if(!inertia && (pR - cR > 0.2 || cR- pR > 0.2)) {
		inertia = true;
	}
	if(inertia) {
		std::cout << pR - cR << "\n";
		if(pR - cR > 0.001 || cR - pR > 0.001) {
			if(cR > pR) {
				cR += -1.5 * dt;
			}
			else {
				cR += 1.5 * dt;
			}
		}
		else {
			inertia = false;
		}
	}


	_camera->setPosition((r + 5) * cos(cR), 0, (r +5) * sin (cR));
	_camera->lookAt(0.0001, 0, 0);

  return true;
}

bool
TestState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
TestState::keyPressed
(const OIS::KeyEvent &e)
{
	switch (e.key) {
	case OIS::KC_LEFT:
		wR = -1.5;
		break;
	case OIS::KC_RIGHT:
		wR = 1.5;
		break;

	}
}

void
TestState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
	#ifdef _TESTONLY
	  _exitGame = true;
	#else
	  this->changeState(PlayState::getSingletonPtr());
	#endif
  }
  else if (e.key == OIS::KC_LEFT || OIS::KC_RIGHT) {
	 wR = 0.0;
  }
}

void
TestState::mouseMoved
(const OIS::MouseEvent &e)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;

}

void
TestState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
}


void
TestState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}


TestState*
TestState::getSingletonPtr ()
{
return msSingleton;
}

TestState&
TestState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
